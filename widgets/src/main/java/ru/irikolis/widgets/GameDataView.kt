package ru.irikolis.widgets

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Parcel
import android.os.Parcelable
import android.text.format.DateFormat
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat

/**
 * @author Irina Kolovorotnaya (irikolis)
 */

enum class DataState(@param:DrawableRes val iconId: Int, @param:StringRes val infoId: Int) {
    NO_DATA(R.drawable.ic_data_error_24dp, R.string.no_data),
    OLD_DATA(R.drawable.ic_data_warring_24dp, R.string.last_update),
    SUCCESS(R.drawable.ic_data_ok_24dp, R.string.last_update),
    LOADING_ERROR(R.drawable.ic_data_error_24dp, R.string.loading_error);
}

@Suppress("unused", "MemberVisibilityCanBePrivate")
class GameDataView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0,
        defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyleAttr, defStyleRes) {
    var imgData: ImageView
        private set
    var txtDataName: TextView
        private set
    var txtLastUpdateTime: TextView
        private set
    var btnUpdate: Button
        private set

    var picture: Drawable
        get() = imgData.drawable
        set(value) { imgData.setImageDrawable(value) }

    var name: CharSequence
        get() = txtDataName.text
        set(value) { txtDataName.text = value }

    var maxDays: Int = 1
        private set

    var loading: Boolean = false
        private set

    var lastUpdateTime: Long = 0L
        private set

    override fun setOnClickListener(l: OnClickListener?) = btnUpdate.setOnClickListener(l)

    init {
        LayoutInflater.from(context).inflate(R.layout.view_game_data, this, true)
        imgData = findViewById(R.id.image_data)
        txtDataName = findViewById(R.id.text_view_data_name)
        txtLastUpdateTime = findViewById(R.id.text_view_last_update_time)
        btnUpdate = findViewById(R.id.button_update)

        attrs?.let {
            val a = context.obtainStyledAttributes(
                    attrs,
                    R.styleable.GameDataView,
                    defStyleAttr,
                    defStyleRes
            )

            picture = a.getDrawable(R.styleable.GameDataView_gd_picture)
                    ?: ColorDrawable()

            name = a.getText(R.styleable.GameDataView_gd_name)

            maxDays = a.getInteger(R.styleable.GameDataView_gd_maxDays, 1)

            a.recycle()
        }

        setLoading(loading)
        setLastUpdateTime(lastUpdateTime)
    }

    internal class SavedState : BaseSavedState {
        var loading: Boolean = false
        var lastUpdateTime: Long = 0L

        constructor(state: Parcelable?) : super(state)

        constructor(source: Parcel) : super(source) {
            loading = (source.readInt() == 1)
            lastUpdateTime = source.readLong()
        }

        companion object {
            @JvmField
            val CREATOR = object : Parcelable.Creator<SavedState> {
                override fun createFromParcel(source: Parcel) = SavedState(source)
                override fun newArray(size: Int) = arrayOfNulls<SavedState>(size)
            }
        }

        override fun writeToParcel(dest: Parcel?, flags: Int) {
            dest?.writeInt(if (loading) 1 else 0)
            dest?.writeLong(lastUpdateTime)
        }

        override fun describeContents() = 0
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        (state as? SavedState)?.let {
            loading = it.loading
        }
        super.onRestoreInstanceState(state)
    }

    override fun onSaveInstanceState(): Parcelable {
        val state = SavedState(super.onSaveInstanceState())
        state.loading = this.loading
        return state
    }

    private fun getDiffDays(time: Long): Double {
        val diff = System.currentTimeMillis() - time
        return diff.toDouble() / (24 * 60 * 60 * 1000)
    }

    fun setLastUpdateTime(time: Long) {
        this.lastUpdateTime = time

        val state = when {
            (time == 0L) -> DataState.NO_DATA
            (getDiffDays(time) < maxDays) -> DataState.SUCCESS
            else -> DataState.OLD_DATA
        }

        var info: String = context.getString(state.infoId)
        if (state == DataState.OLD_DATA || state == DataState.SUCCESS)
            info += ":\r\n${DateFormat.format("dd-MM-yyyy HH:mm:ss", time)}"
        btnUpdate.text = context.getString(R.string.load)
        txtLastUpdateTime.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context, state.iconId), null, null, null)
        txtLastUpdateTime.text = info
    }

    fun setLoading(loading: Boolean) {
        this.loading = loading

        if (loading) {
            val info = "${context.getString(R.string.loading)} ..."
            txtLastUpdateTime.text = info
            txtLastUpdateTime.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
        }
        btnUpdate.isEnabled = !loading
    }

    fun setMaxDays(days: Int) {
        this.maxDays = days

        setLastUpdateTime(lastUpdateTime)
    }
}