package ru.irikolis.caravan.application;


import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import ru.irikolis.caravan.BuildConfig;
import ru.irikolis.caravan.di.component.AppComponent;
import ru.irikolis.caravan.di.component.AuthComponent;
import ru.irikolis.caravan.di.component.DaggerAppComponent;
import ru.irikolis.caravan.di.component.GameComponent;
import ru.irikolis.caravan.di.component.ConfigurationComponent;
import ru.irikolis.caravan.di.component.MainComponent;
import ru.irikolis.caravan.di.component.ServiceComponent;
import ru.irikolis.caravan.di.module.AuthModule;
import ru.irikolis.caravan.di.module.ConfigurationModule;
import ru.irikolis.caravan.di.module.GameModule;
import ru.irikolis.caravan.di.module.ServiceModule;
import ru.irikolis.caravan.di.module.MainModule;
import timber.log.Timber;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class CaravanApplication extends Application
        implements Application.ActivityLifecycleCallbacks {

    private static CaravanApplication caravanApplication;
    public static CaravanApplication getInstance() {
        return caravanApplication;
    }

    private AppComponent appComponent;
    private MainComponent mainComponent;
    private AuthComponent authComponent;
    private ConfigurationComponent configurationComponent;
    private GameComponent gameComponent;
    private ServiceComponent serviceComponent;

    private int activityCount = 0;

    public boolean isAppPaused() {
        return activityCount <= 0;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        caravanApplication = this;
        appComponent = DaggerAppComponent
                .builder()
                .application(this)
                .build();
        registerActivityLifecycleCallbacks(this);

        if (BuildConfig.DEBUG)
            Timber.plant(new Timber.DebugTree());
    }

    public MainComponent getMainComponent() {
        if (mainComponent == null) {
            mainComponent = appComponent.plusMainComponent(new MainModule());
        }
        return mainComponent;
    }

    public void clearMainComponent() {
        mainComponent = null;
    }

    public AuthComponent getAuthComponent() {
        if (authComponent == null) {
            authComponent = appComponent.plusAuthComponent(new AuthModule());
        }
        return authComponent;
    }

    public void clearAuthComponent() {
        authComponent = null;
    }

    public ConfigurationComponent getConfigurationComponent() {
        if (configurationComponent == null) {
            configurationComponent = appComponent.plusConfigurationComponent(new ConfigurationModule());
        }
        return configurationComponent;
    }

    public void clearConfigurationComponent() {
        configurationComponent = null;
    }

    public GameComponent getGameComponent() {
        if (gameComponent == null) {
            gameComponent = appComponent.plusGameComponent(new GameModule());
        }
        return gameComponent;
    }

    public void clearGameComponent() {
        gameComponent = null;
    }

    public ServiceComponent getServiceComponent() {
        if (serviceComponent == null) {
            serviceComponent = appComponent.plusGameServiceComponent(new ServiceModule());
        }
        return serviceComponent;
    }

    public void clearGameServiceComponent() {
        serviceComponent = null;
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {
    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        ++activityCount;
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {
        --activityCount;
    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
    }
}