package ru.irikolis.caravan.data.network;

import androidx.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import ru.irikolis.caravan.BuildConfig;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class LoggingInterceptor implements Interceptor {

    private final Interceptor interceptor;

    private LoggingInterceptor() {
        interceptor = new HttpLoggingInterceptor()
                .setLevel(BuildConfig.DEBUG ? Level.BODY : Level.NONE);
    }

    @Override
    @NonNull
    public Response intercept(@NonNull Chain chain) throws IOException {
        return interceptor.intercept(chain);
    }

    @NonNull
    public static LoggingInterceptor create() {
        return new LoggingInterceptor();
    }
}
