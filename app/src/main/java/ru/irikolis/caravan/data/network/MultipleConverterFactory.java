package ru.irikolis.caravan.data.network;

import android.graphics.BitmapFactory;

import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class MultipleConverterFactory extends Converter.Factory {
    private final Converter.Factory gson;

    public static MultipleConverterFactory create() {
        return new MultipleConverterFactory();
    }

    private MultipleConverterFactory() {
        gson = GsonConverterFactory.create();
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, @NotNull Annotation[] annotations, Retrofit retrofit) {
        for (Annotation annotation : annotations) {
            if (annotation.annotationType() == Json.class) {
                return gson.responseBodyConverter(type, annotations, retrofit);
            }
            if (annotation.annotationType() == Img.class) {
                return body -> BitmapFactory.decodeStream(body.byteStream());
            }
        }

        return gson.responseBodyConverter(type, annotations, retrofit);
    }

    @Override
    public Converter<?, RequestBody> requestBodyConverter(Type type, Annotation[] parameterAnnotations, Annotation[] methodAnnotations, Retrofit retrofit) {
        return gson.requestBodyConverter(type, parameterAnnotations, methodAnnotations, retrofit);
    }
}
