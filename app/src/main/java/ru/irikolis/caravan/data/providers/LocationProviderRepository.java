package ru.irikolis.caravan.data.providers;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import ru.irikolis.caravan.domain.models.Coord;
import ru.irikolis.caravan.domain.models.CoordState;
import ru.irikolis.caravan.domain.models.Result;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static androidx.core.content.ContextCompat.checkSelfPermission;
import static ru.irikolis.caravan.application.AppConstants.MIN_LOCATION_UPDATE_DISTANCE;
import static ru.irikolis.caravan.application.AppConstants.MIN_LOCATION_UPDATE_TIME;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class LocationProviderRepository {
    private Context context;
    private LocationManager locationManager;

    @Inject
    public LocationProviderRepository(Context context) {
        this.context = context;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    public Observable<Result<CoordState, Coord>> startLocationListener() {
        return Observable.create(emitter -> {
            final LocationListener locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Criteria criteria = new Criteria();
                    criteria.setAccuracy(Criteria.ACCURACY_FINE);
                    String bestProvider = locationManager.getBestProvider(criteria, true);

                    if (location.getProvider().equals(bestProvider))
                        emitter.onNext(Result.create(CoordState.SUCCESS, new Coord(location.getLatitude(), location.getLongitude())));
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                @Override
                public void onProviderEnabled(String provider) {
                }

                @Override
                public void onProviderDisabled(String provider) {
                }
            };

            // Check location permissions
            if (checkSelfPermission(context, ACCESS_FINE_LOCATION) != PERMISSION_GRANTED &&
                checkSelfPermission(context, ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {
                emitter.onNext(Result.create(CoordState.PERMISSION_DENIED, null));
                emitter.onComplete();
            }
            // Request location updates
            else {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_LOCATION_UPDATE_TIME, MIN_LOCATION_UPDATE_DISTANCE, locationListener);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_LOCATION_UPDATE_TIME, MIN_LOCATION_UPDATE_DISTANCE, locationListener);
                emitter.setCancellable(() -> locationManager.removeUpdates(locationListener));
            }
        });
    }

    public Single<Result<CoordState, Coord>> getLastLocation() {
        // Check location permissions
        if (checkSelfPermission(context, ACCESS_FINE_LOCATION) != PERMISSION_GRANTED &&
            checkSelfPermission(context, ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {
            return Single.just(Result.create(CoordState.PERMISSION_DENIED, null));
        }
        // Get last location
        else {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            return Single.fromCallable(() -> locationManager.getBestProvider(criteria, true))
                    .map(locationManager::getLastKnownLocation)
                    .map(lastLocation -> Result.create(CoordState.SUCCESS, new Coord(lastLocation.getLatitude(), lastLocation.getLongitude())))
                    .onErrorReturnItem(Result.create(CoordState.NO_DATA, null));
        }
    }

    public static boolean isReachTarget(Coord location, Coord target, float accuracy) {
        float[] result = new float[3];
        Location.distanceBetween(location.getLatitude(), location.getLongitude(),
                target.getLatitude(), target.getLongitude(), result);
        return result[0] <= Math.abs(accuracy);
    }
}
