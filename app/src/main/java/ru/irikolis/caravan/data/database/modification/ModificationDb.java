package ru.irikolis.caravan.data.database.modification;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Entity(tableName = "modifications")
public class ModificationDb {

    @PrimaryKey
    @ColumnInfo(name = "table_name")
    @NonNull private String tableName;

    @ColumnInfo(name = "trigger_action")
    private String triggerAction;

    @ColumnInfo(name = "last_modified_time", defaultValue = "CURRENT_TIMESTAMP")
    private String lastModifiedTime;

    public ModificationDb(@NotNull String tableName, String triggerAction, String lastModifiedTime) {
        this.tableName = tableName;
        this.triggerAction = triggerAction;
        this.lastModifiedTime = lastModifiedTime;
    }

    @NotNull public String getTableName() {
        return tableName;
    }

    public String getTriggerAction() {
        return triggerAction;
    }

    public String getLastModifiedTime() {
        return lastModifiedTime;
    }
}
