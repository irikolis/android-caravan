package ru.irikolis.caravan.data.mappers;

import ru.irikolis.caravan.data.network.game.GameResponse;
import ru.irikolis.caravan.domain.models.Game;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GameMapper {
    public static Game networkToEntity(GameResponse response) {
        return new Game(
                response.getId(),
                response.getName(),
                response.getDescription(),
                response.getIconUrl());
    }
}
