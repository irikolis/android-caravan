package ru.irikolis.caravan.data.database.gamemessage;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Entity(tableName = "audio_messages")
public class AudioMessageDb extends BaseMessageDb {

    @PrimaryKey
    @ColumnInfo(name = "sent_id")
    private int sentId;

    @ColumnInfo(name = "sent_time")
    private Long sentTime;

    @ColumnInfo(name = "text")
    private String text;

    @ColumnInfo(name = "audio_url")
    private String audioUrl;

    @ColumnInfo(name = "audio_start_time")
    private Integer audioStartTime;

    @ColumnInfo(name = "audio_final_time")
    private Integer audioFinalTime;

    @ColumnInfo(name = "audio_size")
    private Double audioSize;

    public AudioMessageDb(int sentId, Long sentTime, String text, String audioUrl, Integer audioStartTime, Integer audioFinalTime, Double audioSize, Long postingTime) {
        super(postingTime);
        this.sentId = sentId;
        this.sentTime = sentTime;
        this.text = text;
        this.audioUrl = audioUrl;
        this.audioStartTime = audioStartTime;
        this.audioFinalTime = audioFinalTime;
        this.audioSize = audioSize;
    }

    public int getSentId() {
        return sentId;
    }

    public Long getSentTime() {
        return sentTime;
    }

    public String getText() {
        return text;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public Integer getAudioStartTime() {
        return audioStartTime;
    }

    public Integer getAudioFinalTime() {
        return audioFinalTime;
    }

    public Double getAudioSize() {
        return audioSize;
    }
}
