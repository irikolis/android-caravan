package ru.irikolis.caravan.data.database.collected;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import ru.irikolis.caravan.data.database.artifact.ArtifactDb;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Entity(tableName = "collected_artifacts")
public class CollectedArtifactDb {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @Embedded
    private ArtifactDb artifact;

    public CollectedArtifactDb(ArtifactDb artifact) {
        this.artifact = artifact;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public ArtifactDb getArtifact() {
        return artifact;
    }
}
