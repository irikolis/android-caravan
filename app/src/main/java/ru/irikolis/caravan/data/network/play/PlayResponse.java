package ru.irikolis.caravan.data.network.play;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings("unused")
public class PlayResponse {
	@Expose
	@SerializedName("last_id")
	private int lastMessageId;

	public int getLastMessageId() {
		return lastMessageId;
	}
}
