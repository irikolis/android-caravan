package ru.irikolis.caravan.data.mappers;

import ru.irikolis.caravan.data.network.questbuild.QuestBuildResponse;
import ru.irikolis.caravan.data.network.questbuild.TargetNetwork;
import ru.irikolis.caravan.domain.models.GameData;
import ru.irikolis.caravan.domain.models.Target;
import ru.irikolis.caravan.domain.models.TargetMessage;
import ru.irikolis.caravan.domain.models.TargetState;
import ru.irikolis.caravan.domain.models.TargetType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GameDataMapper {
    public static GameData networkToEntity(QuestBuildResponse response) {
        List<Target> targets = new ArrayList<>();
        List<TargetMessage> messages = new ArrayList<>();

        boolean isFirstNecessaryPoint = true;
        for (TargetNetwork target : response.getTargets()) {
            TargetType type = TargetType.valueOf(target.getType());
            TargetState state;
            if (type == TargetType.NECESSARY) {
                state = (response.isOrdered() && !isFirstNecessaryPoint) ? TargetState.BLOCKED : TargetState.UNBLOCKED;
                isFirstNecessaryPoint = false;
            }
            else state = TargetState.UNBLOCKED;

            targets.add(new Target(target.getSeqId(), target.getGeoId(), type, state));
            messages.add(new TargetMessage(target.getSeqId(), target.getMessage(), null));
        }

        return new GameData(response.getGamerId(), response.isMapVisible(), response.isTargetVisible(), response.isOrdered(), targets, messages);
    }
}
