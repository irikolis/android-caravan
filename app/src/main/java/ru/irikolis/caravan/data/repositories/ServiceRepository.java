package ru.irikolis.caravan.data.repositories;

import android.app.ActivityManager;
import android.content.Context;

import ru.irikolis.caravan.data.service.RxBusEvent;
import ru.irikolis.caravan.domain.models.BaseMessage;
import ru.irikolis.caravan.domain.models.Coord;
import ru.irikolis.caravan.data.service.GameService;

import java.util.Objects;

import io.reactivex.Observable;
import ru.irikolis.caravan.data.service.RxBus;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ServiceRepository {
    private Context context;
    private RxBus rxBus;

    public ServiceRepository(Context context) {
        this.context = context;
        this.rxBus = RxBus.getInstance();
    }

    public boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : Objects.requireNonNull(manager).getRunningServices(Integer.MAX_VALUE)) {
            if (GameService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public Observable<Boolean> startRequestPermissionListener() {
        return rxBus.listen(RxBusEvent.RequestPermissionEvent.class)
                .map(event -> true);
    }

    public Observable<Coord> startLocationListener() {
        return rxBus.listen(RxBusEvent.LocationEvent.class)
                .map(RxBusEvent.LocationEvent::getCoord);
    }

    public Observable<BaseMessage> startCompletedTargetListener() {
        return rxBus.listen(RxBusEvent.CompletedTargetEvent.class)
                .map(RxBusEvent.CompletedTargetEvent::getTargetMessage);
    }

    public Observable<BaseMessage> startLastCompletedTargetListener() {
        return rxBus.listen(RxBusEvent.LastCompletedTargetEvent.class)
                .map(RxBusEvent.LastCompletedTargetEvent::getTargetMessage);
    }

    public Observable<BaseMessage> startTextMessageListener() {
        return rxBus.listen(RxBusEvent.TextMessageEvent.class)
                .map(RxBusEvent.TextMessageEvent::getTextMessage);
    }

    public Observable<BaseMessage> startAudioMessageListener() {
        return rxBus.listen(RxBusEvent.AudioMessageEvent.class)
                .map(RxBusEvent.AudioMessageEvent::getAudioMessage);
    }
}
