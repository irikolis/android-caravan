package ru.irikolis.caravan.data.network.messages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings("unused")
public class MessageResponse {
	@Expose
	@SerializedName("id")
	private int id;

	@Expose
	@SerializedName("text")
	private String text;

	@Expose
	@SerializedName("audio_url")
	private String audioUrl;

	@Expose
	@SerializedName("audio_size")
	private double audioSize;

	@Expose
	@SerializedName("time")
	private String time;

	public int getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public String getAudioUrl() {
		return audioUrl;
	}

	public double getAudioSize() {
		return audioSize;
	}

	public String getTime() {
		return time;
	}
}
