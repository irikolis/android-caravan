package ru.irikolis.caravan.data.database.modification;

import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import org.jetbrains.annotations.NotNull;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ModificationTriggerCallback extends RoomDatabase.Callback {
    private final String TABLE_NAME = "modifications";

    private final String MODIFIED_TABLE_NAME_COLUMN = "table_name";
    private final String ACTION_COLUMN = "trigger_action";
    private final String LAST_MODIFIED_COLUMN = "last_modified_time";

    private final String[] tables = { "geo_points", "artifacts", "game_maps" };


    @Override
    public void onCreate(@NotNull SupportSQLiteDatabase db) {
        super.onCreate(db);
    }

    @Override
    public void onOpen(@NotNull SupportSQLiteDatabase db) {
        super.onOpen(db);

        // Create modification table
        createTable(db);

        // Create triggers
        for (String table : tables)
            createTrigger(db, table);
    }

    private void createTable(@NotNull SupportSQLiteDatabase db) {
        String CREATE_TABLE_SQL =
                "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                        MODIFIED_TABLE_NAME_COLUMN + " TEXT PRIMARY KEY, " +
                        ACTION_COLUMN + " TEXT, " +
                        LAST_MODIFIED_COLUMN + " TEXT DEFAULT CURRENT_TIMESTAMP);";
        db.execSQL(CREATE_TABLE_SQL);
    }

    private void createTrigger(@NotNull SupportSQLiteDatabase db, String table) {
        String INSERT_INTO_TABLE_SQL =
                "INSERT OR IGNORE INTO " + TABLE_NAME +
                        "(" + MODIFIED_TABLE_NAME_COLUMN + ") " +
                        "VALUES ('" + table + "');";
        db.execSQL(INSERT_INTO_TABLE_SQL);

        String trigger = table + "_inserted";
        String CREATE_TRIGGER_TABLE_SQL =
                "CREATE TRIGGER IF NOT EXISTS " + trigger + " " +
                "AFTER INSERT ON " + table + " " +
                "BEGIN UPDATE " + TABLE_NAME + " " +
                    "SET " + ACTION_COLUMN + " = 'UPDATE', " +
                             LAST_MODIFIED_COLUMN + " = CURRENT_TIMESTAMP " +
                    "WHERE " + MODIFIED_TABLE_NAME_COLUMN + " = '" + table + "';" +
                "END;";
        db.execSQL(CREATE_TRIGGER_TABLE_SQL);
    }
}
