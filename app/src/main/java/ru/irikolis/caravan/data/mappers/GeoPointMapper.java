package ru.irikolis.caravan.data.mappers;

import ru.irikolis.caravan.data.database.geopoint.GeoPointDb;
import ru.irikolis.caravan.data.network.geopoint.GeoPointResponse;
import ru.irikolis.caravan.domain.models.GeoPoint;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GeoPointMapper {
    public static GeoPoint networkToEntity(GeoPointResponse response) {
        return new GeoPoint(response.getGeoId(), response.getLabel(), response.getLatitude(), response.getLongitude());
    }

    public static GeoPoint dbToEntity(GeoPointDb geoPointDb) {
        return new GeoPoint(geoPointDb.getGeoId(), geoPointDb.getTitle(), geoPointDb.getLatitude(), geoPointDb.getLongitude());
    }

    public static GeoPointDb entityToDb(GeoPoint geoPoint) {
        return new GeoPointDb(geoPoint.getGeoId(), geoPoint.getTitle(), geoPoint.getLatitude(), geoPoint.getLongitude());
    }
}
