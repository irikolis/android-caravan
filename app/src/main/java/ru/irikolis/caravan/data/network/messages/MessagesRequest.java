package ru.irikolis.caravan.data.network.messages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class MessagesRequest {
	@Expose
	@SerializedName("start_message_id")
	private int startId;

	@Expose
	@SerializedName("count_message")
	private int count;

	public MessagesRequest(int startId, int count) {
		this.startId = startId;
		this.count = count;
	}
}
