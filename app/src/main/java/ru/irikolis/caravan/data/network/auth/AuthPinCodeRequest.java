package ru.irikolis.caravan.data.network.auth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class AuthPinCodeRequest {
	@Expose
	@SerializedName("pincode")
	private String pinCode;

	public AuthPinCodeRequest(String pinCode) {
		this.pinCode = pinCode;
	}
}
