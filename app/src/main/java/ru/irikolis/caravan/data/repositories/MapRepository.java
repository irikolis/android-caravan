package ru.irikolis.caravan.data.repositories;

import android.content.Context;

import ru.irikolis.caravan.data.database.AppDatabase;
import ru.irikolis.caravan.data.mappers.GameMapMapper;
import ru.irikolis.caravan.data.network.ApiService;
import ru.irikolis.caravan.domain.models.GameMap;
import ru.irikolis.caravan.utils.BitmapUtils;
import ru.irikolis.caravan.utils.FileUtils;

import java.io.InputStream;
import java.util.List;

import io.reactivex.Single;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class MapRepository {
    private Context context;
    private ApiService apiService;
    private AppDatabase appDatabase;

    public MapRepository(Context context, ApiService apiService, AppDatabase appDatabase) {
        this.context = context;
        this.apiService = apiService;
        this.appDatabase = appDatabase;
    }

    /**
     * Get game map from api
     */
    public Single<List<GameMap>> getMapFromApi() {
        return apiService.getGameMap()
                .flattenAsObservable(items -> items)
                .map(GameMapMapper::networkToEntity)
                .toList();
    }

    /**
     * Reset game map in the database
     */
    public Single<List<Long>> resetMapsDb(List<GameMap> gameMaps) {
        return Single.fromCallable(() -> appDatabase.clearTable("game_maps"))
                .map(cleared -> gameMaps)
                .flattenAsObservable(items -> items)
                .map(GameMapMapper::entityToDb)
                .map(map -> appDatabase.getGameMapDao().insert(map))
                .toList();
    }

    /**
     * Get game map from the database
     */
    public Single<GameMap> getMapToDb() {
        return Single.fromCallable(() -> appDatabase.getGameMapDao().getById(1))
                .map(GameMapMapper::dbToEntity);
    }

    /**
     * Download map image from url
     */
    public Single<InputStream> downloadMapImageFromApi(String imageUrl) {
        return apiService.downloadMapImage(imageUrl)
                .map(BitmapUtils::getInputStreamFromBitmap);
    }

    /**
     * Save map image to file
     */
    public Single<Long> putMapToFile(String fileName, InputStream inputStream) {
        return Single.fromCallable(() -> FileUtils.copyInputStreamToFile(context, inputStream, fileName));
    }
}
