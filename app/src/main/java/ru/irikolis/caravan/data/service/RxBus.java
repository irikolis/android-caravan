package ru.irikolis.caravan.data.service;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class RxBus {
    private static volatile RxBus rxBus = null;
    private PublishSubject<Object> publisher = PublishSubject.create();

    private RxBus() {
    }

    public static RxBus getInstance() {
        if (rxBus == null) {
            synchronized (RxBus.class) {
                if (rxBus == null) {
                    rxBus = new RxBus();
                }
            }
        }
        return rxBus;
    }

    @SuppressWarnings("unchecked")
    public <T> Observable<T> listen(Class<T> clazz) {
        return publisher
                .filter(obj -> obj.getClass().equals(clazz))
                .map(obj -> (T) obj);
    }

    public void publish(Object event) {
        publisher.onNext(event);
    }
}
