package ru.irikolis.caravan.data.network;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Target(METHOD)
@Retention(RUNTIME)
@interface Audio {
}
