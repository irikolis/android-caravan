package ru.irikolis.caravan.data.database.gamemap;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Entity(tableName = "game_maps")
public class GameMapDb {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "image_url")
    private String imageUrl;

    @ColumnInfo(name = "south_east_latitude")
    private double southEastLatitude;

    @ColumnInfo(name = "north_west_latitude")
    private double northWestLatitude;

    @ColumnInfo(name = "north_west_longitude")
    private double northWestLongitude;

    @ColumnInfo(name = "south_east_longitude")
    private double southEastLongitude;

    @ColumnInfo(name = "accuracy")
    private int accuracy;

    public GameMapDb(String imageUrl, double southEastLatitude, double northWestLatitude, double northWestLongitude, double southEastLongitude, int accuracy) {
        this.imageUrl = imageUrl;
        this.southEastLatitude = southEastLatitude;
        this.northWestLatitude = northWestLatitude;
        this.northWestLongitude = northWestLongitude;
        this.southEastLongitude = southEastLongitude;
        this.accuracy = accuracy;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public double getSouthEastLatitude() {
        return southEastLatitude;
    }

    public double getNorthWestLatitude() {
        return northWestLatitude;
    }

    public double getNorthWestLongitude() {
        return northWestLongitude;
    }

    public double getSouthEastLongitude() {
        return southEastLongitude;
    }

    public int getAccuracy() {
        return accuracy;
    }
}
