package ru.irikolis.caravan.data.database.modification;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Dao
public interface ModificationDao {

    @Query("SELECT * FROM modifications")
    List<ModificationDb> getAll();

    @Query("SELECT * FROM modifications WHERE table_name = :tableName")
    ModificationDb getByTableName(String tableName);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ModificationDb modificationDb);

    @Update
    void update(ModificationDb modificationDb);

    @Delete
    void delete(ModificationDb modificationDb);
}
