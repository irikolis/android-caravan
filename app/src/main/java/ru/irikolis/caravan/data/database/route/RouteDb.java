package ru.irikolis.caravan.data.database.route;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Entity(tableName = "routes")
public class RouteDb {

    @PrimaryKey
    @ColumnInfo(name = "route_id")
    private int routeId;

    @ColumnInfo(name = "route_name")
    private String name;

    @ColumnInfo(name = "route_level")
    private int level;

    @ColumnInfo(name = "route_description")
    private String description;

    @ColumnInfo(name = "master_instruction")
    private String instruction;

    @ColumnInfo(name = "map_visible")
    private boolean mapVisible;

    @ColumnInfo(name = "route_visible")
    private boolean routeVisible;

    @ColumnInfo(name = "ordered")
    private boolean ordered;

    @ColumnInfo(name = "last_modified_time")
    private String lastModifiedTime;

    public RouteDb(int routeId, String name, int level, String description, String instruction,
                   boolean mapVisible, boolean routeVisible, boolean ordered, String lastModifiedTime) {
        this.routeId = routeId;
        this.name = name;
        this.level = level;
        this.description = description;
        this.instruction = instruction;
        this.mapVisible = mapVisible;
        this.routeVisible = routeVisible;
        this.ordered = ordered;
        this.lastModifiedTime = lastModifiedTime;
    }

    public int getRouteId() {
        return routeId;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public String getDescription() {
        return description;
    }

    public String getInstruction() {
        return instruction;
    }

    public boolean isMapVisible() {
        return mapVisible;
    }

    public boolean isRouteVisible() {
        return routeVisible;
    }

    public boolean isOrdered() {
        return ordered;
    }

    public String getLastModifiedTime() {
        return lastModifiedTime;
    }
}
