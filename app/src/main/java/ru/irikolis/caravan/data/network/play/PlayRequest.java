package ru.irikolis.caravan.data.network.play;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class PlayRequest {
	@Expose
	@SerializedName("user_id")
	private int gamerId;

	@Expose
	@SerializedName("latitude")
	private double latitude;

	@Expose
	@SerializedName("longitude")
	private double longitude;

	@Expose
	@SerializedName("last_points")
	private Integer[] lastPoints;

	public PlayRequest(int gamerId, double latitude, double longitude, Integer[] lastPoints) {
		this.gamerId = gamerId;
		this.latitude = latitude;
		this.longitude = longitude;
		this.lastPoints = lastPoints;
	}
}
