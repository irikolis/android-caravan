package ru.irikolis.caravan.data.database.target;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Entity(tableName = "targets")
public class TargetDb {

    @PrimaryKey
    @ColumnInfo(name = "seq_id")
    private int seqId;

    @ColumnInfo(name = "geo_id")
    private int geoId;

    @ColumnInfo(name = "type")
    private String type;

    @ColumnInfo(name = "state")
    private String state;

    public TargetDb(int seqId, int geoId, String type, String state) {
        this.seqId = seqId;
        this.geoId = geoId;
        this.type = type;
        this.state = state;
    }

    public int getSeqId() {
        return seqId;
    }

    public int getGeoId() {
        return geoId;
    }

    public String getType() {
        return type;
    }

    public String getState() {
        return state;
    }
}
