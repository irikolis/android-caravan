package ru.irikolis.caravan.data.repositories;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import ru.irikolis.caravan.data.mappers.GameMapper;
import ru.irikolis.caravan.data.network.ApiService;
import ru.irikolis.caravan.domain.models.Game;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GamesRepository {
    private ApiService apiService;

    @Inject
    public GamesRepository(ApiService apiService) {
        this.apiService = apiService;
    }

    public Single<List<Game>> getGamesFromApi() {
        return apiService.getGames()
                .flattenAsObservable(items -> items)
                .map(GameMapper::networkToEntity)
                .toList();
    }
}
