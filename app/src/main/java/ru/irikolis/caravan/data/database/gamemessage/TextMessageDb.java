package ru.irikolis.caravan.data.database.gamemessage;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Entity(tableName = "text_messages")
public class TextMessageDb extends BaseMessageDb {

    @PrimaryKey
    @ColumnInfo(name = "sent_id")
    private int sentId;

    @ColumnInfo(name = "sent_time")
    private Long sentTime;

    @ColumnInfo(name = "text")
    private String text;

    public TextMessageDb(int sentId, Long sentTime, String text, Long postingTime) {
        super(postingTime);
        this.sentId = sentId;
        this.sentTime = sentTime;
        this.text = text;
    }

    public int getSentId() {
        return sentId;
    }

    public Long getSentTime() {
        return sentTime;
    }

    public String getText() {
        return text;
    }
}
