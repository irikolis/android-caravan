package ru.irikolis.caravan.data.network.auth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings("unused")
public class AuthPinCodeResponse {
	@Expose
	@SerializedName("token")
	private String token;

	@Expose
	@SerializedName("user_id")
	private int userId;

	@Expose
	@SerializedName("email")
	private String email;

	@Expose
	@SerializedName("gamer_name")
	private String gamerName;

	@Expose
	@SerializedName("route_id")
	private int routeId;

	public String getToken() {
		return token;
	}

	public int getUserId() {
		return userId;
	}

	public String getEmail() {
		return email;
	}

	public String getGamerName() {
		return gamerName;
	}

	public int getRouteId() {
		return routeId;
	}
}
