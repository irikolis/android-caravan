package ru.irikolis.caravan.data.repositories;

import ru.irikolis.caravan.data.network.ApiService;
import ru.irikolis.caravan.data.network.auth.AuthLoginRequest;
import ru.irikolis.caravan.data.network.auth.AuthPinCodeRequest;
import ru.irikolis.caravan.data.network.auth.AuthLoginResponse;
import ru.irikolis.caravan.data.preference.AppPreference;

import io.reactivex.Single;
import ru.irikolis.caravan.utils.AppExceptionFactory;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class AuthRepository {
    private ApiService apiService;
    private AppPreference appPreference;

    public AuthRepository(ApiService apiService, AppPreference appPreference) {
        this.apiService = apiService;
        this.appPreference = appPreference;
    }

    public Single<Boolean> performAuthLoginFromApi(String username, String password) {
        return apiService.performAuthLogin(new AuthLoginRequest(username, password))
                .map(AuthLoginResponse::getToken)
                .doOnSuccess(token -> appPreference.putToken(token))
                .flatMap(token -> (token.isEmpty())
                        ? Single.error(AppExceptionFactory.createTokenException())
                        : Single.just(true)
                );
    }

    public Single<Boolean> performAuthPinCodeFromApi(String pinCode) {
        return apiService.performAuthPinCode(new AuthPinCodeRequest(pinCode))
                .doOnSuccess(response -> {
                    appPreference.putToken(response.getToken());
                    appPreference.putRouteId(response.getRouteId());
                    appPreference.putGamerName(response.getGamerName());
                })
                .flatMap(response -> (response.getToken().isEmpty())
                        ? Single.error(AppExceptionFactory.createTokenException())
                        : Single.just(true)
                );
    }
}
