package ru.irikolis.caravan.data.network.route;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings("unused")
public class RouteResponse {
	@Expose
	@SerializedName("route_id")
	private int routeId;

	@Expose
	@SerializedName("route_name")
	private String routeName;

	@Expose
	@SerializedName("route_level")
	private int routeLevel;

	@Expose
	@SerializedName("route_description")
	private String routeDescription;

	@Expose
	@SerializedName("master_instruction")
	private String masterInstruction;

	@Expose
	@SerializedName("map_visible")
	private boolean mapVisible;

	@Expose
	@SerializedName("route_visible")
	private boolean routeVisible;

	@Expose
	@SerializedName("ordered")
	private boolean ordered;

	@Expose
	@SerializedName("last_update")
	private String lastModifiedTime;

	public int getRouteId() {
		return routeId;
	}

	public String getRouteName() {
		return routeName;
	}

	public int getRouteLevel() {
		return routeLevel;
	}

	public String getRouteDescription() {
		return routeDescription;
	}

	public String getMasterInstruction() {
		return masterInstruction;
	}

	public boolean isMapVisible() {
		return mapVisible;
	}

	public boolean isRouteVisible() {
		return routeVisible;
	}

	public boolean isOrdered() {
		return ordered;
	}

	public String getLastModifiedTime() {
		return lastModifiedTime;
	}
}
