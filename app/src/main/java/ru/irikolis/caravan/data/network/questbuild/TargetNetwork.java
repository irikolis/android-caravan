package ru.irikolis.caravan.data.network.questbuild;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings("unused")
public class TargetNetwork {
    @Expose
    @SerializedName("seq_id")
    private int seqId;

    @Expose
    @SerializedName("geo_id")
    private int geoId;

    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("point_type")
    private String type;

    public int getSeqId() {
        return seqId;
    }

    public int getGeoId() {
        return geoId;
    }

    public String getMessage() {
        return message;
    }

    public String getType() {
        return type;
    }
}
