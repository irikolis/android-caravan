package ru.irikolis.caravan.data.network.geopoint;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings("unused")
public class GeoPointNetwork {
    @Expose
    @SerializedName("latitude")
    private double latitude;

    @Expose
    @SerializedName("longitude")
    private double longitude;

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
