package ru.irikolis.caravan.data.network.game;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings("unused")
public class GameResponse {
	@Expose
	@SerializedName("id")
	private int id;

	@Expose
	@SerializedName("name")
	private String name;

	@Expose
	@SerializedName("description")
	private String description;

	@Expose
	@SerializedName("icon")
	private String iconUrl;

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getIconUrl() {
		return iconUrl;
	}
}
