package ru.irikolis.caravan.data.network.questbuild;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class QuestBuildRequest {
	@Expose
	@SerializedName("gamer_name")
	private String gamerName;

	@Expose
	@SerializedName("route_id")
	private int routeId;

	public QuestBuildRequest(String gamerName, int routeId) {
		this.gamerName = gamerName;
		this.routeId = routeId;
	}
}
