package ru.irikolis.caravan.data.mappers;

import java.util.Date;

import ru.irikolis.caravan.data.database.gamemessage.AudioMessageDb;
import ru.irikolis.caravan.data.database.gamemessage.BaseMessageDb;
import ru.irikolis.caravan.data.database.gamemessage.TargetMessageDb;
import ru.irikolis.caravan.data.database.gamemessage.TextMessageDb;
import ru.irikolis.caravan.data.network.messages.MessageResponse;
import ru.irikolis.caravan.domain.models.AudioMessage;
import ru.irikolis.caravan.domain.models.BaseMessage;
import ru.irikolis.caravan.domain.models.TargetMessage;
import ru.irikolis.caravan.domain.models.TextMessage;
import ru.irikolis.caravan.utils.DateUtils;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class MessageMapper {
    public static BaseMessage networkToEntity(MessageResponse response) {
        // according to android documentation zone offset with X format is supporting in API level 24+
        int id = response.getId();
        long sentTime = DateUtils.parse("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ", response.getTime()).getTime();
        String text = response.getText();
        long postingTime = new Date().getTime();
        String url = response.getAudioUrl();
        if (!url.isEmpty())
            return new AudioMessage(id, sentTime, text, url, null, null, response.getAudioSize(), postingTime);
        return new TextMessage(id, sentTime, text, postingTime);
    }

    public static BaseMessage dbToEntity(BaseMessageDb db) {
        if (db instanceof TargetMessageDb) {
            TargetMessageDb messageDb = (TargetMessageDb) db;
            return new TargetMessage(
                    messageDb.getId(),
                    messageDb.getText(),
                    messageDb.getPostingTime()
            );
        }
        else if (db instanceof TextMessageDb) {
            TextMessageDb messageDb = (TextMessageDb) db;
            return new TextMessage(
                    messageDb.getSentId(),
                    messageDb.getSentTime(),
                    messageDb.getText(),
                    messageDb.getPostingTime()
            );
        }
        else if (db instanceof AudioMessageDb) {
            AudioMessageDb messageDb = (AudioMessageDb) db;
            return new AudioMessage(
                    messageDb.getSentId(),
                    messageDb.getSentTime(),
                    messageDb.getText(),
                    messageDb.getAudioUrl(),
                    messageDb.getAudioStartTime(),
                    messageDb.getAudioFinalTime(),
                    messageDb.getAudioSize(),
                    messageDb.getPostingTime()
            );
        }
        return null;
    }

    public static BaseMessageDb entityToDb(BaseMessage message) {
        if (message instanceof TargetMessage) {
            TargetMessage msg = (TargetMessage) message;
            return new TargetMessageDb(
                    msg.getId(),
                    msg.getText(),
                    msg.getPostingTime()
            );
        }
        else if (message instanceof TextMessage) {
            TextMessage msg = (TextMessage) message;
            return new TextMessageDb(
                    msg.getSentId(),
                    msg.getSentTime(),
                    msg.getText(),
                    msg.getPostingTime()
            );
        }
        else if (message instanceof AudioMessage) {
            AudioMessage msg = (AudioMessage) message;
            return new AudioMessageDb(
                    msg.getSentId(),
                    msg.getSentTime(),
                    msg.getText(),
                    msg.getAudioUrl(),
                    msg.getStartTime(),
                    msg.getFinalTime(),
                    msg.getAudioSize(),
                    msg.getPostingTime()
            );
        }
        return null;
    }
}
