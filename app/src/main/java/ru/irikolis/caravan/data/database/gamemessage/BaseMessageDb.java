package ru.irikolis.caravan.data.database.gamemessage;

import androidx.room.ColumnInfo;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class BaseMessageDb {
    @ColumnInfo(name = "posting_time")
    private Long postingTime;

    public BaseMessageDb(Long postingTime) {
        this.postingTime = postingTime;
    }

    public Long getPostingTime() {
        return postingTime;
    }
}
