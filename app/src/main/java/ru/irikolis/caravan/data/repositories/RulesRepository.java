package ru.irikolis.caravan.data.repositories;

import android.content.Context;

import ru.irikolis.caravan.utils.FileUtils;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;

import io.reactivex.Single;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class RulesRepository {
    private Context context;

    public RulesRepository(Context context) {
        this.context = context;
    }

    /**
     * Get game rules from url
     */
    public Single<InputStream> getGameRulesFromUrl(String fileUrl) {
        return Single.fromCallable(() -> new URL(fileUrl))
                .map(URL::openStream);
    }

    /**
     * Get game rules to file
     */
    public Single<Long> putGameRulesToFile(String fileName, InputStream inputStream) {
        return Single.fromCallable(() -> FileUtils.copyInputStreamToFile(context, inputStream, fileName));
    }

    public Single<Long> getLastModifyOfFile(String fileName) {
        return Single.just(FileUtils.getLastModified(context.getFilesDir() + "/" + fileName))
                .flatMap(millis -> (millis == 0L) ? Single.error(new FileNotFoundException()) : Single.just(millis));
    }
}
