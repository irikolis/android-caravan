package ru.irikolis.caravan.data.database.modification;

import androidx.room.TypeConverter;

import java.util.Date;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class TimeConverter {
    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
}
