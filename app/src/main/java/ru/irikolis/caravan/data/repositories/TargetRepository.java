package ru.irikolis.caravan.data.repositories;

import ru.irikolis.caravan.data.database.AppDatabase;
import ru.irikolis.caravan.data.mappers.GeoPointMapper;
import ru.irikolis.caravan.data.mappers.PointMapper;
import ru.irikolis.caravan.data.mappers.GameDataMapper;
import ru.irikolis.caravan.data.mappers.TargetMapper;
import ru.irikolis.caravan.data.network.ApiService;
import ru.irikolis.caravan.data.network.play.PlayRequest;
import ru.irikolis.caravan.data.network.play.PlayResponse;
import ru.irikolis.caravan.data.network.questbuild.QuestBuildRequest;
import ru.irikolis.caravan.domain.models.GeoPoint;
import ru.irikolis.caravan.domain.models.TargetCoord;
import ru.irikolis.caravan.domain.models.GameData;
import ru.irikolis.caravan.domain.models.Target;

import java.util.List;

import io.reactivex.Single;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class TargetRepository {
    private ApiService apiService;
    private AppDatabase appDatabase;

    public TargetRepository(ApiService apiService, AppDatabase appDatabase) {
        this.apiService = apiService;
        this.appDatabase = appDatabase;
    }

    /**
     * Perform quest build
     */
    public Single<GameData> performQuestBuildApi(String gamerName, int routeId) {
        return apiService.performQuestBuild(new QuestBuildRequest(gamerName, routeId))
                .map(GameDataMapper::networkToEntity);
    }

    /**
     * Dispatch gamer location and receive last message id
     */
    public Single<Integer> dispatchLocationApi(int gamerId, double latitude, double longitude, Integer[] lastPoints) {
        return apiService.dispatchLocation(new PlayRequest(gamerId, latitude, longitude, lastPoints))
                .map(PlayResponse::getLastMessageId);
    }

    /**
     * Get geo-points from api
     */
    public Single<List<GeoPoint>> getGeoPointsFromApi() {
        return apiService.getGeoPoints()
                .flattenAsObservable(items -> items)
                .map(GeoPointMapper::networkToEntity)
                .toList();
    }

    /**
     * Reset geo-points in the database
     */
    public Single<List<Long>> resetGeoPointsDb(List<GeoPoint> geoPoints) {
        return Single.fromCallable(() -> appDatabase.clearTable("geo_points"))
                .map(cleared -> geoPoints)
                .flattenAsObservable(items -> items)
                .map(GeoPointMapper::entityToDb)
                .map(geoPoint -> appDatabase.getGeoPointDao().insert(geoPoint))
                .toList();
    }

    /**
     * Reset targets in the database
     */
    public Single<List<Long>> resetTargetsDb(List<Target> targets) {
        return Single.fromCallable(() -> appDatabase.clearTable("targets"))
                .map(cleared -> targets)
                .flattenAsObservable(items -> items)
                .map(TargetMapper::entityToDb)
                .map(routePoint -> appDatabase.getTargetDao().insert(routePoint))
                .toList();
    }

    /**
     * Get targets from database
     */
    public Single<List<TargetCoord>> getAllTargetsFromDb() {
        return Single.fromCallable(() -> appDatabase.getPointDao().getAllTargets())
                .flattenAsObservable(items -> items)
                .map(PointMapper::dbToEntity)
                .toList();
    }

    public Single<List<TargetCoord>> getAllTargetsFromDb(String state) {
        return Single.fromCallable(() -> appDatabase.getPointDao().getAllTargets(state))
                .flattenAsObservable(items -> items)
                .map(PointMapper::dbToEntity)
                .toList();
    }

    public Single<Boolean> setNextTargetToDb(String type, String oldState, String newState) {
        return Single.fromCallable(() -> appDatabase.getPointDao().getFirstSelectTarget(type, oldState))
                .flattenAsObservable(items -> items)
                .flatMap(point -> updateTargetStateToDb(point.getSeqId(), newState).toObservable())
                .filter(updated -> updated)
                .single(false);
    }

    public Single<Boolean> updateTargetStateToDb(int seqId, String state) {
        return Single.fromCallable(() -> appDatabase.getPointDao().updateStateBySeqId(seqId, state))
                .map(rows -> rows > 0);
    }
}
