package ru.irikolis.caravan.data.database.artifact;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Dao
public interface ArtifactDao {
    @Query("SELECT * FROM artifacts")
    List<ArtifactDb> getAll();

    @Query("SELECT * FROM artifacts WHERE mac = :mac")
    List<ArtifactDb> getByMac(long mac);

    @Insert
    long insert(ArtifactDb artifactDb);

    @Update
    void update(ArtifactDb artifactDb);

    @Delete
    void delete(ArtifactDb artifactDb);
}
