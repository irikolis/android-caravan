package ru.irikolis.caravan.data.network.questbuild;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings("unused")
public class QuestBuildResponse {
    @Expose
    @SerializedName("user_id")
    private int gamerId;

    @SerializedName("route_id")
    private int routeId;

    @SerializedName("route_name")
    private String routeName;

    @SerializedName("route_level")
    private int routeLevel;

    @SerializedName("route_description")
    private String routeDescription;

    @SerializedName("master_instruction")
    private String masterInstruction;

    @Expose
    @SerializedName("points")
    private List<TargetNetwork> targets;

    @Expose
    @SerializedName("map_visible")
    private boolean mapVisible;

    @Expose
    @SerializedName("route_visible")
    private boolean targetVisible;

    @Expose
    @SerializedName("ordered")
    private boolean ordered;

    @Expose
    @SerializedName("last_update")
    private String lastModifiedTime;

    public List<TargetNetwork> getTargets() {
        return targets;
    }

    public int getGamerId() { return gamerId; }

    public boolean isMapVisible() {
        return mapVisible;
    }

    public boolean isTargetVisible() {
        return targetVisible;
    }

    public boolean isOrdered() {
        return ordered;
    }
}
