package ru.irikolis.caravan.data.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.domain.interactors.ServiceInteractor;
import ru.irikolis.caravan.domain.models.AudioMessage;
import ru.irikolis.caravan.domain.models.BaseMessage;
import ru.irikolis.caravan.domain.models.Coord;
import ru.irikolis.caravan.domain.models.CoordState;
import ru.irikolis.caravan.domain.models.TargetMessage;
import ru.irikolis.caravan.domain.models.TextMessage;
import ru.irikolis.caravan.domain.models.TargetCheckState;
import ru.irikolis.caravan.presentation.game.GameActivity;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import ru.irikolis.caravan.utils.SchedulersProvider;
import timber.log.Timber;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GameService extends Service {
    private static final int MAIN_INFORMATION_NOTIFY_ID = 1;
    private static final int NEW_MESSAGE_NOTIFY_ID = 2;
    private static final String MAIN_INFORMATION_CHANNEL_ID = "GameService";
    private static final String NEW_MESSAGE_CHANNEL_ID = "NewMessageService";

    private static final String PATH_OF_SOUND_NEW_MESSAGE = "sounds/sound_new_message.wav";
    private static final String PATH_OF_SOUND_CONGRATULATION = "sounds/sound_congratulation.wav";

    private int soundIdNewMessage;
    private int soundIdCongratulation;

    private CompositeDisposable compositeDisposable;
    private RxBus rxBus;

    @Inject ServiceInteractor interactor;
    @Inject SchedulersProvider schedulersProvider;

    public GameService() {
        compositeDisposable = new CompositeDisposable();
        this.rxBus = RxBus.getInstance();
    }

    @Override
    public void onCreate() {
        CaravanApplication.getInstance().getServiceComponent().inject(this);
        super.onCreate();

        if (Build.VERSION.SDK_INT >= 26) {
            startForeground(MAIN_INFORMATION_NOTIFY_ID, createMainInformationNotification());
        }
        else {
            NotificationManagerCompat.from(this).notify(MAIN_INFORMATION_NOTIFY_ID, createMainInformationNotification());
        }
    }

    protected void unsubscribeOnDestroy(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if ((flags & START_FLAG_RETRY) != START_FLAG_RETRY) {
            loadSoundPool();
            startLocationListenTask();
            startLocationDispatchTask();
        }
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
        CaravanApplication.getInstance().clearGameServiceComponent();
    }

    private void startLocationListenTask() {
        Disposable disposable = interactor.startLocationListener()
                .doOnNext(location -> {
                    // the permission is required
                    if (location.getState() == CoordState.PERMISSION_DENIED) {
                        sendPermissionEvent();
                        cancelNotifications();
                        stopSelf();
                    }
                    // the player location
                    else if (location.getData() != null) {
                        Coord coord = location.getData();
                        sendLocationEvent(coord);
                    }
                })
                .flatMap(location -> interactor.checkTargets(location.getData()).toObservable())
                .subscribe(target -> {
                    // if the player has reached the target
                    if (target.getState() != TargetCheckState.MISSED_TARGET &&
                            target.getData() != null) {

                        TargetCheckState state = target.getState();
                        for (TargetMessage message: target.getData()) {
                            sendCompletedTargetEvent(state, message);

                            interactor.playSound(soundIdNewMessage);
                            createMainInformationNotification();
                            if (((CaravanApplication) getApplicationContext()).isAppPaused())
                                NotificationManagerCompat.from(this).notify(NEW_MESSAGE_NOTIFY_ID, createNewMessageNotification());

                            if (state == TargetCheckState.LAST_COMPLETED_TARGET) {
                                cancelNotifications();
                                showCongratulation();
                                stopSelf();
                            }
                        }
                    }}, Timber::d
                );
        unsubscribeOnDestroy(disposable);
    }

    private void startLocationDispatchTask() {
        Disposable disposable = interactor.startLocationDispatchAndMessagesCheck()
                .observeOn(schedulersProvider.ui())
                .subscribe(
                        message -> {
                            sendMessageEvent(message);

                            interactor.playSound(soundIdNewMessage);
                            createMainInformationNotification();
                            if (((CaravanApplication) getApplicationContext()).isAppPaused())
                                NotificationManagerCompat.from(this).notify(NEW_MESSAGE_NOTIFY_ID, createNewMessageNotification());
                        },
                        Timber::d
                );
        unsubscribeOnDestroy(disposable);
    }

    private Notification createMainInformationNotification() {
        String text = getString(R.string.completed) + ": " + interactor.getCompletedNumberOfNecessaryTargets() +
                " / " + getString(R.string.remained) + ": " + interactor.getNumberOfNecessaryTargets();

        Intent notificationIntent = new Intent(this, GameActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        return new NotificationCompat.Builder(this, MAIN_INFORMATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_caravan_app_64dp)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(text)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(contentIntent)
                .build();
    }

    private Notification createNewMessageNotification() {
        Intent notificationIntent = new Intent(this, GameActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra(GameActivity.PARAM_MENU_ITEM_ID, R.id.item_messages);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        return new NotificationCompat.Builder(this, NEW_MESSAGE_CHANNEL_ID)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_caravan_app_64dp)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.new_message))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setLights(Color.YELLOW, 1, 0)
                .setContentIntent(contentIntent)
                .build();
    }

    private void cancelNotifications() {
        NotificationManagerCompat.from(this).cancelAll();
    }

    private void loadSoundPool() {
        soundIdNewMessage = interactor.loadSound(PATH_OF_SOUND_NEW_MESSAGE);
        soundIdCongratulation = interactor.loadSound(PATH_OF_SOUND_CONGRATULATION);
    }

    private void sendPermissionEvent() {
        rxBus.publish(new RxBusEvent.RequestPermissionEvent());
    }

    private void sendLocationEvent(Coord location) {
        rxBus.publish(new RxBusEvent.LocationEvent(location.getLatitude(), location.getLongitude()));
    }

    private void sendCompletedTargetEvent(TargetCheckState state, TargetMessage message) {
        if (state == TargetCheckState.LAST_COMPLETED_TARGET)
            rxBus.publish(new RxBusEvent.LastCompletedTargetEvent(message));
        else
            rxBus.publish(new RxBusEvent.CompletedTargetEvent(message));
    }

    private void sendMessageEvent(BaseMessage message) {
        if (message instanceof AudioMessage)
            rxBus.publish(new RxBusEvent.AudioMessageEvent((AudioMessage) message));
        else if (message instanceof TextMessage)
            rxBus.publish(new RxBusEvent.TextMessageEvent((TextMessage) message));
    }

    private void showCongratulation() {
        Intent intent = new Intent(this, GameActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(GameActivity.PARAM_MENU_ITEM_ID, R.id.item_messages);
        intent.putExtra(GameActivity.PARAM_SOUND_ID, soundIdCongratulation);
        startActivity(intent);
    }
}
