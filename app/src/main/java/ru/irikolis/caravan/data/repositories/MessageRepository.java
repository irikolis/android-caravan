package ru.irikolis.caravan.data.repositories;

import io.reactivex.Observable;
import ru.irikolis.caravan.data.database.AppDatabase;
import ru.irikolis.caravan.data.database.gamemessage.AudioMessageDb;
import ru.irikolis.caravan.data.database.gamemessage.TargetMessageDb;
import ru.irikolis.caravan.data.database.gamemessage.TextMessageDb;
import ru.irikolis.caravan.data.mappers.MessageMapper;
import ru.irikolis.caravan.data.network.ApiService;

import java.io.InputStream;
import java.util.List;

import io.reactivex.Single;
import ru.irikolis.caravan.domain.models.BaseMessage;
import ru.irikolis.caravan.domain.models.TargetMessage;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class MessageRepository {
    private ApiService apiService;
    private AppDatabase appDatabase;

    public MessageRepository(ApiService apiService, AppDatabase appDatabase) {
        this.apiService = apiService;
        this.appDatabase = appDatabase;
    }

    /**
     * Put message to the database
     */
    public Single<Long> putMessageToDb(BaseMessage message) {
        return Single.just(message)
                .map(MessageMapper::entityToDb)
                .map(messageDb -> {
                    if (messageDb instanceof TextMessageDb)
                        return appDatabase.getMessageDao().insert((TextMessageDb) messageDb);
                    else if (messageDb instanceof AudioMessageDb)
                        return appDatabase.getMessageDao().insert((AudioMessageDb) messageDb);
                    else if (messageDb instanceof TargetMessageDb)
                        return appDatabase.getMessageDao().insert((TargetMessageDb) messageDb);
                    else
                        throw new ClassNotFoundException();
                });
    }

    /**
     * Reset messages in the database
     */
    public Single<List<Long>> resetMessagesDb(List<TargetMessage> messages) {
        return Single.fromCallable(() ->
                    appDatabase.clearTable("target_messages") &
                    appDatabase.clearTable("text_messages") &
                    appDatabase.clearTable("audio_messages")
                )
                .map(cleared -> messages)
                .flattenAsObservable(items -> items)
                .flatMap(message -> putMessageToDb(message).toObservable())
                .toList();
    }

    /**
     * Get target message from the database
     */
    public Single<TargetMessage> getTargetMessageFromDb(int id) {
        return Single.fromCallable(() -> appDatabase.getMessageDao().getTargetMessageById(id))
                .map(MessageMapper::dbToEntity)
                .map(message -> (TargetMessage) message);
    }

    /**
     * Update message from the database
     */
    public Single<Integer> updateMessageFromDb(BaseMessage message) {
        return Single.just(message)
                .map(MessageMapper::entityToDb)
                .map(messageDb -> {
                    if (messageDb instanceof TextMessageDb)
                        return appDatabase.getMessageDao().update((TextMessageDb) messageDb);
                    else if (messageDb instanceof AudioMessageDb)
                        return appDatabase.getMessageDao().update((AudioMessageDb) messageDb);
                    else if (messageDb instanceof TargetMessageDb)
                        return appDatabase.getMessageDao().update((TargetMessageDb) messageDb);
                    else
                        throw new ClassNotFoundException();
                });
    }

    /**
     * Get master message by master
     */
    public Single<List<BaseMessage>> getMasterMessagesApi(int startMessageId, int quantity) {
        return apiService.getMessages(startMessageId, quantity)
                .flattenAsObservable(items -> items)
                .map(MessageMapper::networkToEntity)
                .toList();
    }

    /**
     * Download audio from url
     */
    public Single<InputStream> downloadAudioFromApi(String audioUrl) {
        return apiService.downloadAudio(audioUrl);
    }

    /**
     * Get master message from the database
     */
    public Single<Integer> getLastIdOfMasterMessagesFromDb() {
        return Single.fromCallable(() -> appDatabase.getMessageDao().getLastSentId())
                .onErrorReturnItem(0);
    }

    /**
     * Get all messages from the database
     */
    public Single<List<BaseMessage>> getAllMessagesFromDb() {
        return Observable.merge(
                Observable.fromCallable(() -> appDatabase.getMessageDao().getAllTargetMessages()),
                Observable.fromCallable(() -> appDatabase.getMessageDao().getAllTextMessages()),
                Observable.fromCallable(() -> appDatabase.getMessageDao().getAllAudioMessages())
        )
                .flatMapIterable(items -> items)
                .map(MessageMapper::dbToEntity)
                .filter(message -> message.getPostingTime() != null)
                .toSortedList((p1, p2) -> p1.getPostingTime().compareTo(p2.getPostingTime()));
    }
}
