package ru.irikolis.caravan.data.mappers;

import ru.irikolis.caravan.data.database.route.RouteDb;
import ru.irikolis.caravan.data.network.route.RouteResponse;
import ru.irikolis.caravan.domain.models.Route;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class RouteMapper {
    public static Route networkToEntity(RouteResponse response) {
        return new Route(
                response.getRouteId(),
                response.getRouteName(),
                response.getRouteLevel(),
                response.getRouteDescription(),
                response.getMasterInstruction(),
                response.isMapVisible(),
                response.isRouteVisible(),
                response.isOrdered(),
                response.getLastModifiedTime());
    }

    public static Route dbToEntity(RouteDb routeDb) {
        return new Route(
                routeDb.getRouteId(),
                routeDb.getName(),
                routeDb.getLevel(),
                routeDb.getDescription(),
                routeDb.getInstruction(),
                routeDb.isMapVisible(),
                routeDb.isRouteVisible(),
                routeDb.isOrdered(),
                routeDb.getLastModifiedTime());
    }

    public static RouteDb entityToDb(Route route) {
        return new RouteDb(
                route.getRouteId(),
                route.getName(),
                route.getLevel(),
                route.getDescription(),
                route.getInstruction(),
                route.isMapVisible(),
                route.isRouteVisible(),
                route.isOrdered(),
                route.getLastModifiedTime());
    }
}
