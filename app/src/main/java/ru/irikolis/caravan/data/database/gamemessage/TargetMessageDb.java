package ru.irikolis.caravan.data.database.gamemessage;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Entity(tableName = "target_messages")
public class TargetMessageDb extends BaseMessageDb {
    @PrimaryKey
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "text")
    private String text;

    public TargetMessageDb(int id, String text, Long postingTime) {
        super(postingTime);
        this.id = id;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }
}
