package ru.irikolis.caravan.data.database.collected;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Dao
public interface CollectedArtifactDao {
    @Query("SELECT * FROM collected_artifacts")
    List<CollectedArtifactDb> getAll();

    @Query("SELECT * FROM collected_artifacts WHERE mac = :mac")
    List<CollectedArtifactDb> getByMac(long mac);

    @Insert
    long insert(CollectedArtifactDb collectedArtifactDb);

    @Update
    int update(CollectedArtifactDb collectedArtifactDb);

    @Delete
    void delete(CollectedArtifactDb collectedArtifactDb);
}
