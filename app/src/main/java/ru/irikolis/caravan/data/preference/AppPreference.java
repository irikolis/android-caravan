package ru.irikolis.caravan.data.preference;

import android.content.Context;
import android.content.SharedPreferences;

import ru.irikolis.caravan.application.AppConstants;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class AppPreference {
    private final String PREF_KEY_BASE_URL = "base_url";
    private final String PREF_KEY_TOKEN = "token";

    private final String PREF_KEY_GAME_STATE = "game_state";
    private final String PREF_KEY_GAMER_NAME = "gamer_name";
    private final String PREF_KEY_GAMER_ID = "gamer_id";
    private final String PREF_KEY_ROUTE_ID = "route_id";
    private final String PREF_KEY_ROUTE_NAME = "route_name";
    private final String PREF_KEY_ROUTE_DESCRIPTION = "route_description";

    private final String PREF_KEY_MAP_IMAGE_URL = "map_image_url";
    private final String PREF_KEY_MAP_MIN_LAT = "map_min_lat";
    private final String PREF_KEY_MAP_MAX_LAT = "map_max_lat";
    private final String PREF_KEY_MAP_MIN_LONG = "map_min_long";
    private final String PREF_KEY_MAP_MAX_LONG = "map_max_long";

    private final String PREF_KEY_MAP_VISIBLE = "map_visible";
    private final String PREF_KEY_TARGETS_VISIBLE = "targets_visible";
    private final String PREF_KEY_ORDERED = "ordered";

    private final String PREF_KEY_NUMBER_OF_NECESSARY_TARGETS = "number_of_necessary_targets";
    private final String PREF_KEY_COMPLETED_NUMBER_OF_NECESSARY_TARGETS = "completed_number_of_necessary_targets";
    private final String PREF_KEY_COMPLETED_NUMBER_OF_OPTIONAL_TARGETS = "completed_number_of_optional_targets";

    private SharedPreferences sharedPreferences;

    public AppPreference(Context context, String prefName) {
        sharedPreferences = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
    }

    public void clear() {
        sharedPreferences.edit().clear().apply();
    }

    public void putBaseUrl(String url) {
        sharedPreferences.edit().putString(PREF_KEY_BASE_URL, url).apply();
    }

    public String getBaseUrl() {
        return sharedPreferences.getString(PREF_KEY_BASE_URL, AppConstants.BASE_URL);
    }

    public void putToken(String token) {
        sharedPreferences.edit().putString(PREF_KEY_TOKEN, token).apply();
    }

    public String getToken() {
        return sharedPreferences.getString(PREF_KEY_TOKEN, "");
    }

    public void putGameState(String gameState) {
        sharedPreferences.edit().putString(PREF_KEY_GAME_STATE, gameState).apply();
    }

    public String getGameState() {
        return sharedPreferences.getString(PREF_KEY_GAME_STATE, "not initialized");
    }

    public void putGamerName(String gamerName) {
        sharedPreferences.edit().putString(PREF_KEY_GAMER_NAME, gamerName).apply();
    }

    public String getGamerName() {
        return sharedPreferences.getString(PREF_KEY_GAMER_NAME, "");
    }

    public void putGamerId(int gamerId) {
        sharedPreferences.edit().putInt(PREF_KEY_GAMER_ID, gamerId).apply();
    }

    public int getGamerId() {
        return sharedPreferences.getInt(PREF_KEY_GAMER_ID, 0);
    }

    public void putRouteId(int routeId) {
        sharedPreferences.edit().putInt(PREF_KEY_ROUTE_ID, routeId).apply();
    }

    public int getRouteId() {
        return sharedPreferences.getInt(PREF_KEY_ROUTE_ID, 0);
    }

    public void putRouteName(String routeName) {
        sharedPreferences.edit().putString(PREF_KEY_ROUTE_NAME, routeName).apply();
    }

    public String getRouteName() {
        return sharedPreferences.getString(PREF_KEY_ROUTE_NAME, "");
    }

    public void putRouteDescription(String routeDescription) {
        sharedPreferences.edit().putString(PREF_KEY_ROUTE_DESCRIPTION, routeDescription).apply();
    }

    public String getRouteDescription() {
        return sharedPreferences.getString(PREF_KEY_ROUTE_DESCRIPTION, "");
    }

    public void putMapImageUrl(String mapImageUrl) {
        sharedPreferences.edit().putString(PREF_KEY_MAP_IMAGE_URL, mapImageUrl).apply();
    }

    public String getMapImageUrl() {
        return sharedPreferences.getString(PREF_KEY_MAP_IMAGE_URL, "");
    }

    public void putMapMinLatitude(double mapMinLatitude) {
        sharedPreferences.edit().putLong(PREF_KEY_MAP_MIN_LAT, Double.doubleToRawLongBits(mapMinLatitude)).apply();
    }

    public double getMapMinLatitude() {
        return Double.longBitsToDouble(sharedPreferences.getLong(PREF_KEY_MAP_MIN_LAT, 0));
    }

    public void putMapMaxLatitude(double mapMaxLatitude) {
        sharedPreferences.edit().putLong(PREF_KEY_MAP_MAX_LAT, Double.doubleToRawLongBits(mapMaxLatitude)).apply();
    }

    public double getMapMaxLatitude() {
        return Double.longBitsToDouble(sharedPreferences.getLong(PREF_KEY_MAP_MAX_LAT, 0));
    }

    public void putMapMinLongitude(double mapMinLongitude) {
        sharedPreferences.edit().putLong(PREF_KEY_MAP_MIN_LONG, Double.doubleToRawLongBits(mapMinLongitude)).apply();
    }

    public double getMapMinLongitude() {
        return Double.longBitsToDouble(sharedPreferences.getLong(PREF_KEY_MAP_MIN_LONG, 0));
    }

    public void putMapMaxLongitude(double mapMaxLongitude) {
        sharedPreferences.edit().putLong(PREF_KEY_MAP_MAX_LONG, Double.doubleToRawLongBits(mapMaxLongitude)).apply();
    }

    public double getMapMaxLongitude() {
        return Double.longBitsToDouble(sharedPreferences.getLong(PREF_KEY_MAP_MAX_LONG, 0));
    }

    public void putMapVisible(boolean visible) {
        sharedPreferences.edit().putBoolean(PREF_KEY_MAP_VISIBLE, visible).apply();
    }

    public boolean isMapVisible() {
        return sharedPreferences.getBoolean(PREF_KEY_MAP_VISIBLE, false);
    }

    public void putTargetsVisible(boolean visible) {
        sharedPreferences.edit().putBoolean(PREF_KEY_TARGETS_VISIBLE, visible).apply();
    }

    public boolean isTargetsVisible() {
        return sharedPreferences.getBoolean(PREF_KEY_TARGETS_VISIBLE, false);
    }

    public void putOrdered(boolean ordered) {
        sharedPreferences.edit().putBoolean(PREF_KEY_ORDERED, ordered).apply();
    }

    public boolean isOrdered() {
        return sharedPreferences.getBoolean(PREF_KEY_ORDERED, false);
    }

    public void putNumberOfNessesaryTargets(int number) {
        sharedPreferences.edit().putInt(PREF_KEY_NUMBER_OF_NECESSARY_TARGETS, number).apply();
    }

    public int getNumberOfNessesaryTargets() {
        return sharedPreferences.getInt(PREF_KEY_NUMBER_OF_NECESSARY_TARGETS, 0);
    }

    public void putCompletedNumberOfNessesaryTargets(int number) {
        sharedPreferences.edit().putInt(PREF_KEY_COMPLETED_NUMBER_OF_NECESSARY_TARGETS, number).apply();
    }

    public int getCompletedNumberOfNessesaryTargets() {
        return sharedPreferences.getInt(PREF_KEY_COMPLETED_NUMBER_OF_NECESSARY_TARGETS, 0);
    }

    public void putCompletedNumberOfOptionalTargets(int number) {
        sharedPreferences.edit().putInt(PREF_KEY_COMPLETED_NUMBER_OF_OPTIONAL_TARGETS, number).apply();
    }

    public int getCompletedNumberOfOptionalTargets() {
        return sharedPreferences.getInt(PREF_KEY_COMPLETED_NUMBER_OF_OPTIONAL_TARGETS, 0);
    }

    public void putMapParams(boolean mapVisible, boolean targetsVisible, boolean ordered) {
        putMapVisible(mapVisible);
        putTargetsVisible(targetsVisible);
        putOrdered(ordered);
    }

    public void putRouteParams(String routeName, String routeDescription) {
        putRouteName(routeName);
        putRouteDescription(routeDescription);
    }

    public void putGameParams(String gameState, int gamerId) {
        putGameState(gameState);
        putGamerId(gamerId);
    }

    public void putTargetParams(int necessaryTargets, int completedNecessaryTargets, int completedOptionalTargets) {
        putNumberOfNessesaryTargets(necessaryTargets);
        putCompletedNumberOfNessesaryTargets(completedNecessaryTargets);
        putCompletedNumberOfOptionalTargets(completedOptionalTargets);
    }


}
