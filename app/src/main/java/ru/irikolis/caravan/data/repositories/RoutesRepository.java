package ru.irikolis.caravan.data.repositories;

import ru.irikolis.caravan.data.database.AppDatabase;
import ru.irikolis.caravan.data.mappers.RouteMapper;
import ru.irikolis.caravan.data.network.ApiService;
import ru.irikolis.caravan.data.network.gameselect.GameSelectRequest;
import ru.irikolis.caravan.domain.models.Route;

import java.util.List;

import io.reactivex.Single;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class RoutesRepository {
    private ApiService apiService;
    private AppDatabase appDatabase;

    public RoutesRepository(ApiService apiService, AppDatabase appDatabase) {
        this.apiService = apiService;
        this.appDatabase = appDatabase;
    }

    public Single<List<Route>> getRoutesFromApi() {
        return apiService.getRoutes()
                .flattenAsObservable(items -> items)
                .map(RouteMapper::networkToEntity)
                .toList();
    }


    public Single<Boolean> performGameSelectionApi(int gameId) {
        return apiService.performGameSelect(new GameSelectRequest(gameId))
                .map(response -> true);
    }

    /**
     * Reset routes in the database
     */
    public Single<List<Long>> resetRoutesDb(List<Route> routes) {
        return Single.fromCallable(() -> appDatabase.clearTable("routes"))
                .map(cleared -> routes)
                .flattenAsObservable(items -> items)
                .map(RouteMapper::entityToDb)
                .map(route -> appDatabase.getRouteDao().insert(route))
                .toList();
    }

    public Single<Route> getRouteFromDb(int routeId) {
        return Single.fromCallable(() -> appDatabase.getRouteDao().getById(routeId))
                .map(RouteMapper::dbToEntity);
    }
}
