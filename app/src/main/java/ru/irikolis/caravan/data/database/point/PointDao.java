package ru.irikolis.caravan.data.database.point;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Dao
public abstract class PointDao {
    @Query( "SELECT seq_id, title, latitude, longitude, type, state " +
            "FROM targets " +
            "JOIN geo_points ON targets.geo_id = geo_points.geo_id " +
            "WHERE state = :state")
    public abstract List<PointDb> getAllTargets(String state);

    @Query( "SELECT seq_id, title, latitude, longitude, type, state " +
            "FROM targets " +
            "JOIN geo_points ON targets.geo_id = geo_points.geo_id")
    public abstract List<PointDb> getAllTargets();

    @Query( "SELECT seq_id, title, latitude, longitude, type, state " +
            "FROM targets " +
            "JOIN geo_points ON targets.geo_id = geo_points.geo_id " +
            "WHERE type = :type AND state = :state " +
            "ORDER BY seq_id " +
            "LIMIT 1")
    public abstract List<PointDb> getFirstSelectTarget(String type, String state);

    @Query("UPDATE targets SET state = :state WHERE seq_id = :seqId")
    public abstract int updateStateBySeqId(int seqId, String state);
}
