package ru.irikolis.caravan.data.network.gamemap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.irikolis.caravan.data.network.geopoint.GeoPointNetwork;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings("unused")
public class GameMapResponse {
    @Expose
    @SerializedName("id")
    private int id;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("description")
    private String description;

    @SerializedName("url")
    private String url;

    @Expose
    @SerializedName("picture")
    private String imageUrl;

    @Expose
    @SerializedName("north_west")
    private GeoPointNetwork northWestCoord;

    @Expose
    @SerializedName("north_east")
    private GeoPointNetwork northEastCoord;

    @Expose
    @SerializedName("south_west")
    private GeoPointNetwork southWestCoord;

    @Expose
    @SerializedName("south_east")
    private GeoPointNetwork southEastCoord;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public GeoPointNetwork getNorthWestCoord() {
        return northWestCoord;
    }

    public GeoPointNetwork getNorthEastCoord() {
        return northEastCoord;
    }

    public GeoPointNetwork getSouthWestCoord() {
        return southWestCoord;
    }

    public GeoPointNetwork getSouthEastCoord() {
        return southEastCoord;
    }
}
