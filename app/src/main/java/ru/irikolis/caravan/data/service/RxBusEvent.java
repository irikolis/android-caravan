package ru.irikolis.caravan.data.service;

import ru.irikolis.caravan.domain.models.AudioMessage;
import ru.irikolis.caravan.domain.models.Coord;
import ru.irikolis.caravan.domain.models.TargetMessage;
import ru.irikolis.caravan.domain.models.TextMessage;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class RxBusEvent {

    public static class RequestPermissionEvent {
        public RequestPermissionEvent() {
        }
    }

    public static class LocationEvent extends Coord {
        public LocationEvent(double latitude, double longitude) {
            super(latitude, longitude);
        }

        public Coord getCoord() {
            return new Coord(getLatitude(), getLongitude());
        }
    }

    public static class CompletedTargetEvent extends TargetMessage {
        public CompletedTargetEvent(TargetMessage message) {
            super(message.getId(), message.getText(), message.getPostingTime());
        }

        public TargetMessage getTargetMessage() {
            return new TargetMessage(getId(), getText(), getPostingTime());
        }
    }

    public static class LastCompletedTargetEvent extends TargetMessage {
        public LastCompletedTargetEvent(TargetMessage message) {
            super(message.getId(), message.getText(), message.getPostingTime());
        }

        public TargetMessage getTargetMessage() {
            return new TargetMessage(getId(), getText(), getPostingTime());
        }
    }

    public static class TextMessageEvent extends TextMessage {
        public TextMessageEvent(TextMessage message) {
            super(message.getSentId(), message.getSentTime(), message.getText(), message.getPostingTime());
        }

        public TextMessage getTextMessage() {
            return new TextMessage(getSentId(), getSentTime(), getText(), getPostingTime());
        }
    }

    public static class AudioMessageEvent extends AudioMessage {
        public AudioMessageEvent(AudioMessage message) {
            super(
                    message.getSentId(),
                    message.getSentTime(),
                    message.getText(),
                    message.getAudioUrl(),
                    message.getStartTime(),
                    message.getFinalTime(),
                    message.getAudioSize(),
                    message.getPostingTime()
            );
        }

        public AudioMessage getAudioMessage() {
            return new AudioMessage(
                    getSentId(),
                    getSentTime(),
                    getText(),
                    getAudioUrl(),
                    getStartTime(),
                    getFinalTime(),
                    getAudioSize(),
                    getPostingTime()
            );
        }
    }
}
