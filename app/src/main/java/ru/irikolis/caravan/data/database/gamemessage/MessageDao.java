package ru.irikolis.caravan.data.database.gamemessage;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Dao
public interface MessageDao {

   @Query("SELECT * FROM target_messages")
   List<TargetMessageDb> getAllTargetMessages();

   @Query("SELECT * FROM audio_messages")
   List<AudioMessageDb> getAllAudioMessages();

   @Query("SELECT * FROM text_messages")
   List<TextMessageDb> getAllTextMessages();

   @Query("SELECT * FROM target_messages WHERE id=:id")
   TargetMessageDb getTargetMessageById(int id);

   @Insert
   long insert(TargetMessageDb targetMessageDb);

   @Insert
   long insert(AudioMessageDb audioMessageDb);

   @Insert
   long insert(TextMessageDb textMessageDb);

   @Update
   int update(TargetMessageDb targetMessageDb);

   @Update
   int update(AudioMessageDb audioMessageDb);

   @Update
   int update(TextMessageDb textMessageDb);

   @Query("SELECT MAX(max_id) FROM " +
          "(SELECT MAX(sent_id) as max_id FROM text_messages " +
          "UNION " +
          "SELECT MAX(sent_id) FROM audio_messages)")
   Integer getLastSentId();
}
