package ru.irikolis.caravan.data.network.artifact;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings("unused")
public class ArtifactResponse {
	@Expose
	@SerializedName("id")
	private int id;

	@Expose
	@SerializedName("mac")
	private String mac;

	@Expose
	@SerializedName("name")
	private String name;

	@Expose
	@SerializedName("description")
	private String description;

	@Expose
	@SerializedName("auto_init")
	private boolean autoInit;

	@Expose
	@SerializedName("time")
	private String time;

	public String getMac() {
		return mac;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public boolean isAutoInit() {
		return autoInit;
	}

	public String getTime() {
		return time;
	}
}
