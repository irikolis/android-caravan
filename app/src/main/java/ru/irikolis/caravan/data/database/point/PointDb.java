package ru.irikolis.caravan.data.database.point;

import androidx.room.ColumnInfo;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class PointDb {

    @ColumnInfo(name = "seq_id")
    private int seqId;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "latitude")
    private double latitude;

    @ColumnInfo(name = "longitude")
    private double longitude;

    @ColumnInfo(name = "type")
    private String type;

    @ColumnInfo(name = "state")
    private String state;

    public PointDb(int seqId, String title, double latitude, double longitude, String type, String state) {
        this.seqId = seqId;
        this.title = title;
        this.latitude = latitude;
        this.longitude = longitude;
        this.type = type;
        this.state = state;
    }

    public int getSeqId() {
        return seqId;
    }

    public String getTitle() {
        return title;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getType() {
        return type;
    }

    public String getState() {
        return state;
    }
}
