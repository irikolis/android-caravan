package ru.irikolis.caravan.data.network.auth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class AuthLoginRequest {
	@Expose
	@SerializedName("username")
	private String username;

	@Expose
	@SerializedName("password")
	private String password;

	public AuthLoginRequest(String username, String password) {
		this.username = username;
		this.password = password;
	}
}
