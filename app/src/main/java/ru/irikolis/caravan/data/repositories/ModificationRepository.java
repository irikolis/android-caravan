package ru.irikolis.caravan.data.repositories;

import ru.irikolis.caravan.data.database.AppDatabase;

import java.sql.Timestamp;

import io.reactivex.Single;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ModificationRepository {
    private AppDatabase appDatabase;

    public ModificationRepository(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }

    public Single<Long> getLastModifyFromDb(String tableName) {
        return Single.fromCallable(() -> appDatabase.getModificationDao().getByTableName(tableName))
                .flatMap(modification -> {
                    if (modification.getTriggerAction().isEmpty())
                        return Single.error(new Throwable("Trigger action is empty"));
                    else
                        return Single.fromCallable(modification::getLastModifiedTime)
                                .map(str -> Timestamp.valueOf(str).getTime());
                });
    }
}
