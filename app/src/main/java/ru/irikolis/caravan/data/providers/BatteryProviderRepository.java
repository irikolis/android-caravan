package ru.irikolis.caravan.data.providers;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import ru.irikolis.caravan.utils.BroadcastReceiverUtils;

import java.util.Objects;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class BatteryProviderRepository {
    private Context context;

    @Inject
    public BatteryProviderRepository(Context context) {
        this.context = context;
    }

    public Observable<Boolean> startBatteryChangeListener() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_BATTERY_LOW);
        intentFilter.addAction(Intent.ACTION_BATTERY_OKAY);

        return BroadcastReceiverUtils.create(context, intentFilter)
                .map(intent -> Objects.equals(intent.getAction(), Intent.ACTION_BATTERY_LOW));
    }
}
