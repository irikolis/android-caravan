package ru.irikolis.caravan.data.network;

import android.graphics.Bitmap;

import java.io.InputStream;
import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;
import ru.irikolis.caravan.data.network.artifact.ArtifactResponse;
import ru.irikolis.caravan.data.network.auth.AuthLoginRequest;
import ru.irikolis.caravan.data.network.auth.AuthPinCodeRequest;
import ru.irikolis.caravan.data.network.auth.AuthLoginResponse;
import ru.irikolis.caravan.data.network.auth.AuthPinCodeResponse;
import ru.irikolis.caravan.data.network.game.GameResponse;
import ru.irikolis.caravan.data.network.gamemap.GameMapResponse;
import ru.irikolis.caravan.data.network.gameselect.GameSelectRequest;
import ru.irikolis.caravan.data.network.gameselect.GameSelectResponse;
import ru.irikolis.caravan.data.network.geopoint.GeoPointResponse;
import ru.irikolis.caravan.data.network.messages.MessageResponse;
import ru.irikolis.caravan.data.network.play.PlayRequest;
import ru.irikolis.caravan.data.network.play.PlayResponse;
import ru.irikolis.caravan.data.network.questbuild.QuestBuildRequest;
import ru.irikolis.caravan.data.network.questbuild.QuestBuildResponse;
import ru.irikolis.caravan.data.network.route.RouteResponse;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public interface ApiService {

    String REQUEST_AUTH_TOKEN = "/api/auth-token/";
    String REQUEST_GAMES = "/api/game/";
    String REQUEST_GAME_SELECT = "/api/game/select";
    String REQUEST_ROUTES = "/api/routes/";
    String REQUEST_QUEST_BUILD = "/api/quest_build/";
    String REQUEST_GAME_MAP = "/api/game_map/";
    String REQUEST_GEO_POINTS = "/api/geo_points/";
    String REQUEST_ARTIFACTS = "/api/artifacts/";
    String REQUEST_GAME_MAP_IMAGE = "/media/maps/img_game_map_jDSQfMs.jpg";
    String REQUEST_PLAY = "/api/play/";
    String REQUEST_MESSAGES = "/api/messages";
    String REQUEST_AUDIO = "/media/audio/audio_message_01.mp3";

    @Json
    @POST(REQUEST_AUTH_TOKEN)
    Single<AuthLoginResponse> performAuthLogin(@Body AuthLoginRequest authLoginRequest);

    @Json
    @POST(REQUEST_AUTH_TOKEN)
    Single<AuthPinCodeResponse> performAuthPinCode(@Body AuthPinCodeRequest authPinCodeRequest);

    @Json
    @GET(REQUEST_GAMES)
    Single<List<GameResponse>> getGames();

    @Json
    @POST(REQUEST_GAME_SELECT)
    Single<GameSelectResponse> performGameSelect(@Body GameSelectRequest gameSelectRequest);

    @Json
    @GET(REQUEST_ROUTES)
    Single<List<RouteResponse>> getRoutes();

    @Json
    @POST(REQUEST_QUEST_BUILD)
    Single<QuestBuildResponse> performQuestBuild(@Body QuestBuildRequest questBuildRequest);

    @Json
    @GET(REQUEST_GAME_MAP)
    Single<List<GameMapResponse>> getGameMap();

    @Json
    @GET(REQUEST_GEO_POINTS)
    Single<List<GeoPointResponse>> getGeoPoints();

    @Json
    @GET(REQUEST_ARTIFACTS)
    Single<List<ArtifactResponse>> getArtifacts();

    @Img
    @GET
    @Streaming
    Single<Bitmap> downloadMapImage(@Url String imageUrl);

    @Json
    @POST(REQUEST_PLAY)
    Single<PlayResponse> dispatchLocation(@Body PlayRequest playRequest);

    @Json
    @GET(REQUEST_MESSAGES)
    Single<List<MessageResponse>> getMessages(@Query("start_message_id") int startMessageId, @Query("count_message") int messageCount);

    @Audio
    @GET
    @Streaming
    Single<InputStream> downloadAudio(@Url String audioUrl);
}
