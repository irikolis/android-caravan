package ru.irikolis.caravan.data.repositories;

import ru.irikolis.caravan.data.database.AppDatabase;
import ru.irikolis.caravan.data.mappers.ArtifactMapper;
import ru.irikolis.caravan.data.mappers.CollectedArtifactMapper;
import ru.irikolis.caravan.data.network.ApiService;
import ru.irikolis.caravan.domain.models.Artifact;

import java.util.List;

import io.reactivex.Single;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ArtifactRepository {
    private ApiService apiService;
    private AppDatabase appDatabase;

    public ArtifactRepository(ApiService apiService, AppDatabase appDatabase) {
        this.apiService = apiService;
        this.appDatabase = appDatabase;
    }

    /**
     * Get exist artifacts from api
     */
    public Single<List<Artifact>> getArtifactsFromApi() {
        return apiService.getArtifacts()
                .flattenAsObservable(items -> items)
                .map(ArtifactMapper::networkToEntity)
                .toList();
    }

    /**
     * Reset exist artifacts in the database
     */
    public Single<List<Long>> resetArtifactsDb(List<Artifact> artifacts) {
        return Single.fromCallable(() -> appDatabase.clearTable("artifacts"))
                .map(cleared -> artifacts)
                .flattenAsObservable(items -> items)
                .map(ArtifactMapper::entityToDb)
                .map(artifact -> appDatabase.getArtifactDao().insert(artifact))
                .toList();
    }

    /**
     * Get exist artifacts from the database
     */
    public Single<List<Artifact>> getArtifactsFromDb(long mac) {
        return Single.fromCallable(() -> appDatabase.getArtifactDao().getByMac(mac))
                .flattenAsObservable(items -> items)
                .map(ArtifactMapper::dbToEntity)
                .toList();
    }

    /**
     * Get collected artifacts by gamer from the database
     */
    public Single<List<Artifact>> getCollectedArtifactsFromDb() {
        return Single.fromCallable(() -> appDatabase.getCollectedArtifactDao().getAll())
                .flattenAsObservable(items -> items)
                .map(CollectedArtifactMapper::dbToEntity)
                .toList();
    }

    public Single<List<Artifact>> getCollectedArtifactsFromDb(long mac) {
        return Single.fromCallable(() -> appDatabase.getCollectedArtifactDao().getByMac(mac))
                .flattenAsObservable(items -> items)
                .map(CollectedArtifactMapper::dbToEntity)
                .toList();
    }

    /**
     * Put collected artifacts by gamer to the database
     */
    public Single<List<Long>> putCollectedArtifactsToDb(List<Artifact> artifacts) {
        return Single.just(artifacts)
                .flattenAsObservable(items -> items)
                .map(CollectedArtifactMapper::entityToDb)
                .map(collectedArtifact -> appDatabase.getCollectedArtifactDao().insert(collectedArtifact))
                .toList();
    }

    /**
     * Put collected artifact by gamer to the database
     */
    public Single<Long> putCollectedArtifactToDb(Artifact artifacts) {
        return Single.just(artifacts)
                .map(CollectedArtifactMapper::entityToDb)
                .map(collectedArtifact -> appDatabase.getCollectedArtifactDao().insert(collectedArtifact));
    }

    /**
     * Reset collected artifacts by gamer in the database
     */
    public Single<List<Long>> resetCollectedArtifactsDb(List<Artifact> artifacts) {
        return Single.fromCallable(() -> appDatabase.clearTable("collected_artifacts"))
                .map(cleared -> artifacts)
                .flattenAsObservable(items -> items)
                .map(CollectedArtifactMapper::entityToDb)
                .map(collectedArtifact -> appDatabase.getCollectedArtifactDao().insert(collectedArtifact))
                .toList();
    }
}
