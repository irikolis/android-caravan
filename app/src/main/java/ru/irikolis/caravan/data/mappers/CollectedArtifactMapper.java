package ru.irikolis.caravan.data.mappers;

import ru.irikolis.caravan.data.database.collected.CollectedArtifactDb;
import ru.irikolis.caravan.domain.models.Artifact;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class CollectedArtifactMapper {
    public static Artifact dbToEntity(CollectedArtifactDb collectedArtifactDb) {
        return ArtifactMapper.dbToEntity(collectedArtifactDb.getArtifact());
    }

    public static CollectedArtifactDb entityToDb(Artifact artifact) {
        return new CollectedArtifactDb(ArtifactMapper.entityToDb(artifact));
    }
}
