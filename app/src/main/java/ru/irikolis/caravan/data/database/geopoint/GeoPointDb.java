package ru.irikolis.caravan.data.database.geopoint;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Entity(tableName = "geo_points")
public class GeoPointDb {

    @PrimaryKey
    @ColumnInfo(name = "geo_id")
    private int geoId;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "latitude")
    private double latitude;

    @ColumnInfo(name = "longitude")
    private double longitude;

    public GeoPointDb(int geoId, String title, double latitude, double longitude) {
        this.geoId = geoId;
        this.title = title;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getGeoId() {
        return geoId;
    }

    public String getTitle() {
        return title;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
