package ru.irikolis.caravan.data.network.geopoint;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings("unused")
public class GeoPointResponse {
    @Expose
    @SerializedName("id")
    private int geoId;

    @Expose
    @SerializedName("name")
    private String label;

    @Expose
    @SerializedName("latitude")
    private double latitude;

    @Expose
    @SerializedName("longitude")
    private double longitude;

    public int getGeoId() {
        return geoId;
    }

    public String getLabel() {
        return label;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
