package ru.irikolis.caravan.data.network.gameselect;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class GameSelectRequest {
	@Expose
	@SerializedName("id")
	private int id;

	public GameSelectRequest(int id) {
		this.id = id;
	}
}
