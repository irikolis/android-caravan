package ru.irikolis.caravan.data.database.route;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Dao
public interface RouteDao {

   @Query("SELECT * FROM routes")
   List<RouteDb> getAll();

   @Query("SELECT * FROM routes WHERE route_id=:id")
   RouteDb getById(long id);

   @Insert
   long insert(RouteDb routeDb);

   @Update
   int update(RouteDb routeDb);

   @Delete
   int delete(RouteDb routeDb);
}
