package ru.irikolis.caravan.data.mappers;

import ru.irikolis.caravan.data.database.point.PointDb;
import ru.irikolis.caravan.domain.models.TargetCoord;
import ru.irikolis.caravan.domain.models.TargetState;
import ru.irikolis.caravan.domain.models.TargetType;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class PointMapper {
    public static TargetCoord dbToEntity(PointDb pointDb) {
        return new TargetCoord(
                pointDb.getSeqId(),
                pointDb.getTitle(),
                pointDb.getLatitude(),
                pointDb.getLongitude(),
                TargetType.valueOf(pointDb.getType()),
                TargetState.valueOf(pointDb.getState())
        );
    }

    public static PointDb entityToDb(TargetCoord point) {
        return new PointDb(
                point.getSeqId(),
                point.getTitle(),
                point.getLatitude(),
                point.getLongitude(),
                point.getType().name(),
                point.getState().name()
        );
    }
}
