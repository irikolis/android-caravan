package ru.irikolis.caravan.data.mappers;

import ru.irikolis.caravan.data.database.artifact.ArtifactDb;
import ru.irikolis.caravan.data.network.artifact.ArtifactResponse;
import ru.irikolis.caravan.domain.models.Artifact;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ArtifactMapper {
    public static Artifact networkToEntity(ArtifactResponse response) {
        return new Artifact(
                Long.parseLong(response.getMac().replaceAll(":", ""), 16),
                response.getName(),
                response.getDescription(),
                response.isAutoInit(),
                response.getTime()
        );
    }

    public static Artifact dbToEntity(ArtifactDb artifactDb) {
        return new Artifact(
                artifactDb.getMac(),
                artifactDb.getName(),
                artifactDb.getDescription(),
                artifactDb.isAutoInit(),
                artifactDb.getTime()
        );
    }

    public static ArtifactDb entityToDb(Artifact artifact) {
        return new ArtifactDb(
                artifact.getMac(),
                artifact.getName(),
                artifact.getDescription(),
                artifact.isAutoInit(),
                artifact.getTime()
        );
    }
}
