package ru.irikolis.caravan.data.database.gamemap;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Dao
public interface GameMapDao {
    @Query("SELECT * FROM game_maps")
    List<GameMapDb> getAll();

    @Query("SELECT * FROM game_maps WHERE id = :id")
    GameMapDb getById(long id);

    @Insert
    long insert(GameMapDb artifactDb);

    @Update
    void update(GameMapDb artifactDb);

    @Delete
    void delete(GameMapDb artifactDb);
}
