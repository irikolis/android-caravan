package ru.irikolis.caravan.data.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import ru.irikolis.caravan.data.database.artifact.ArtifactDb;
import ru.irikolis.caravan.data.database.artifact.ArtifactDao;
import ru.irikolis.caravan.data.database.collected.CollectedArtifactDao;
import ru.irikolis.caravan.data.database.gamemap.GameMapDao;
import ru.irikolis.caravan.data.database.gamemap.GameMapDb;
import ru.irikolis.caravan.data.database.collected.CollectedArtifactDb;
import ru.irikolis.caravan.data.database.gamemessage.AudioMessageDb;
import ru.irikolis.caravan.data.database.gamemessage.MessageDao;
import ru.irikolis.caravan.data.database.geopoint.GeoPointDao;
import ru.irikolis.caravan.data.database.geopoint.GeoPointDb;
import ru.irikolis.caravan.data.database.gamemessage.TextMessageDb;
import ru.irikolis.caravan.data.database.modification.ModificationDao;
import ru.irikolis.caravan.data.database.modification.ModificationDb;
import ru.irikolis.caravan.data.database.point.PointDao;
import ru.irikolis.caravan.data.database.route.RouteDao;
import ru.irikolis.caravan.data.database.route.RouteDb;
import ru.irikolis.caravan.data.database.target.TargetDao;
import ru.irikolis.caravan.data.database.target.TargetDb;
import ru.irikolis.caravan.data.database.gamemessage.TargetMessageDb;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Database(version = 1,
          exportSchema = false,
          entities = {RouteDb.class,
                      ModificationDb.class,
                      GeoPointDb.class,
                      TargetDb.class,
                      GameMapDb.class,
                      ArtifactDb.class,
                      CollectedArtifactDb.class,
                      TargetMessageDb.class,
                      TextMessageDb.class,
                      AudioMessageDb.class}
)
public abstract class AppDatabase extends RoomDatabase {
   public abstract RouteDao getRouteDao();
   public abstract ModificationDao getModificationDao();
   public abstract GeoPointDao getGeoPointDao();
   public abstract TargetDao getTargetDao();
   public abstract PointDao getPointDao();
   public abstract GameMapDao getGameMapDao();
   public abstract ArtifactDao getArtifactDao();
   public abstract CollectedArtifactDao getCollectedArtifactDao();
   public abstract MessageDao getMessageDao();

   public boolean clearTable(String tableName) {
      SupportSQLiteDatabase db = getOpenHelper().getWritableDatabase();
      db.execSQL("DELETE FROM " + tableName);
      db.execSQL("UPDATE sqlite_sequence SET seq = 0 WHERE name = '" + tableName + "'");
      return true;
   }
}
