package ru.irikolis.caravan.data.database.geopoint;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Dao
public interface GeoPointDao {

   @Query("SELECT * FROM geo_points")
   List<GeoPointDb> getAll();

   @Insert
   long insert(GeoPointDb geoPointDb);

   @Update
   void update(GeoPointDb geoPointDb);

   @Delete
   void delete(GeoPointDb geoPointDb);
}
