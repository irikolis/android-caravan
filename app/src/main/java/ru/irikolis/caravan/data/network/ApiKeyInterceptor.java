package ru.irikolis.caravan.data.network;

import androidx.annotation.NonNull;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import ru.irikolis.caravan.data.preference.AppPreference;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ApiKeyInterceptor implements Interceptor {

    private final AppPreference appPreference;

    private ApiKeyInterceptor(@NonNull AppPreference appPreference) {
        this.appPreference = appPreference;
    }

    @Override
    @NonNull
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request.Builder request = chain.request().newBuilder();

        String baseUrl = appPreference.getBaseUrl();
        if (!baseUrl.isEmpty()) {
            StringBuilder url = new StringBuilder();
            url.append(String.format("http://%s", baseUrl));
            url.append(chain.request().url().encodedPath());
            if (chain.request().url().encodedQuery() != null)
                url.append(String.format("?%s", chain.request().url().encodedQuery()));

            request.url(url.toString());
        }
        else
            request.url(chain.request().url());

        String token = appPreference.getToken();
        if (!token.isEmpty())
            request.addHeader("Authorization", String.format("Token %s", token));

        return chain.proceed(request.build());
    }

    @NonNull
    public static ApiKeyInterceptor create(@NonNull AppPreference appPreference) {
        return new ApiKeyInterceptor(appPreference);
    }
}
