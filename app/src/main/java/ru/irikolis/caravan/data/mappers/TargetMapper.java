package ru.irikolis.caravan.data.mappers;


import ru.irikolis.caravan.data.database.target.TargetDb;
import ru.irikolis.caravan.domain.models.Target;
import ru.irikolis.caravan.domain.models.TargetState;
import ru.irikolis.caravan.domain.models.TargetType;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class TargetMapper {
    public static Target dbToEntity(TargetDb targetDb) {
        return new Target(
                        targetDb.getSeqId(),
                        targetDb.getGeoId(),
                        TargetType.valueOf(targetDb.getType()),
                        TargetState.valueOf(targetDb.getState())
        );
    }

    public static TargetDb entityToDb(Target target) {
        return new TargetDb(
                        target.getSeqId(),
                        target.getGeoId(),
                        target.getType().name(),
                        target.getState().name()
        );
    }
}
