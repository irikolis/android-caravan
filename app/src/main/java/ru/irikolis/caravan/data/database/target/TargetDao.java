package ru.irikolis.caravan.data.database.target;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Dao
public interface TargetDao {

   @Query("SELECT * FROM targets")
   List<TargetDb> getAll();

   @Insert
   long insert(TargetDb targetDb);

   @Update
   int update(TargetDb targetDb);

   @Delete
   int delete(TargetDb targetDb);
}
