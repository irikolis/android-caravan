package ru.irikolis.caravan.data.mappers;

import ru.irikolis.caravan.data.database.gamemap.GameMapDb;
import ru.irikolis.caravan.data.network.gamemap.GameMapResponse;
import ru.irikolis.caravan.domain.models.GameMap;
import ru.irikolis.caravan.application.AppConstants;
import ru.irikolis.mapview.GeoCoord;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GameMapMapper {
    public static GameMap networkToEntity(GameMapResponse response) {
        return new GameMap(
                response.getImageUrl(),
                new GeoCoord(response.getNorthWestCoord().getLatitude(), response.getNorthWestCoord().getLongitude()),
                new GeoCoord(response.getSouthEastCoord().getLatitude(), response.getSouthEastCoord().getLongitude()),
                AppConstants.MIN_LOCATION_UPDATE_DISTANCE);
    }

    public static GameMap dbToEntity(GameMapDb gameMapDb) {
        return new GameMap(gameMapDb.getImageUrl(),
                new GeoCoord(gameMapDb.getNorthWestLatitude(), gameMapDb.getNorthWestLongitude()),
                new GeoCoord(gameMapDb.getSouthEastLatitude(), gameMapDb.getSouthEastLongitude()),
                gameMapDb.getAccuracy());
    }

    public static GameMapDb entityToDb(GameMap gameMap) {
        return new GameMapDb(gameMap.getImageUrl(),
                gameMap.getSouthEastCoord().getLatitude(),
                gameMap.getNorthWestCoord().getLatitude(),
                gameMap.getNorthWestCoord().getLongitude(),
                gameMap.getSouthEastCoord().getLongitude(),
                gameMap.getAccuracy());
    }
}
