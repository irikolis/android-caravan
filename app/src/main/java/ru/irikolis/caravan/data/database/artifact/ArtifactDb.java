package ru.irikolis.caravan.data.database.artifact;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Entity(tableName = "artifacts",
        primaryKeys = "mac")
public class ArtifactDb {

    @ColumnInfo(name = "mac")
    private long mac;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "auto_init")
    private boolean autoInit;

    @ColumnInfo(name = "time")
    private String time;

    public ArtifactDb(long mac, String name, String description, boolean autoInit, String time) {
        this.mac = mac;
        this.name = name;
        this.description = description;
        this.autoInit = autoInit;
        this.time = time;
    }

    public long getMac() {
        return mac;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isAutoInit() {
        return autoInit;
    }

    public String getTime() {
        return time;
    }
}
