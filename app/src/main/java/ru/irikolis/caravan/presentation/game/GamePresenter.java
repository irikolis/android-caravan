package ru.irikolis.caravan.presentation.game;

import com.arellomobile.mvp.InjectViewState;

import java.util.ArrayDeque;
import java.util.Deque;

import ru.irikolis.caravan.R;
import ru.irikolis.caravan.domain.interactors.GameInteractor;
import ru.irikolis.caravan.domain.models.GameState;
import ru.irikolis.caravan.presentation.core.BasePresenter;

import io.reactivex.disposables.Disposable;
import ru.irikolis.caravan.utils.SchedulersProvider;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.Screen;
import timber.log.Timber;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class GamePresenter extends BasePresenter<GameView> {
    private Router router;
    private GameInteractor interactor;
    private SchedulersProvider schedulersProvider;

    private Deque<String> screenKeys = new ArrayDeque<>();

    public GamePresenter(Router router, GameInteractor interactor, SchedulersProvider schedulersProvider) {
        super();
        this.router = router;
        this.interactor = interactor;
        this.schedulersProvider = schedulersProvider;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        onMainItemClicked();
        getViewState().setMapItemEnabled(interactor.isMapVisible());

        startLocationPermissionListener();
        startMessageListener();
        startGameOverListener();

        if (!interactor.isServiceRunning()) {
            getViewState().startGameService();
        }
    }

    public void playCongratulation(int soundId) {
        interactor.playSound(soundId);
    }

    private void navigateToScreen(Screen screen) {
        String screenKey = screen.getScreenKey();
        screenKey = screenKey.substring(screenKey.lastIndexOf('.')+1);
        if (screenKey.equals("InformationScreen") || screenKey.equals("CongratulationScreen")) {
            router.newRootScreen(screen);
            screenKeys.clear();
        }
        else {
            router.navigateTo(screen);
        }
        screenKeys.push(screenKey);
    }

    private void navigateToBack() {
        screenKeys.pollFirst();
        router.exit();
    }

    public void onMainItemClicked() {
        if (interactor.getNumberOfNecessaryTargets() != interactor.getCompletedNumberOfNecessaryTargets())
            navigateToScreen(new GameScreens.InformationScreen());
        else
            navigateToScreen(new GameScreens.CongratulationScreen());
    }

    public void onMessageItemClicked() {
        navigateToScreen(new GameScreens.MessageScreen());
        getViewState().setMessageRingVisible(false);
    }

    public void onArtifactItemClicked() {
        navigateToScreen(new GameScreens.ArtifactScreen());
    }

    public void onMapItemClicked() {
        navigateToScreen(new GameScreens.MapScreen());
    }

    public void onGameRulesItemClicked() {
        navigateToScreen(new GameScreens.RulesScreen());
    }

    public void onGameAbortItemClicked() {
        getViewState().showGameFinishDialog();
    }

    public void onBackPressed() {
        navigateToBack();
    }

    public void setFinishDialogResult() {
        interactor.setGameState(GameState.GAME_STATE_ABORTED);
        getViewState().finishGame();
    }

    private void startLocationPermissionListener() {
        Disposable disposable = interactor.startPermissionRequestListener()
                .observeOn(schedulersProvider.ui())
                .subscribe(
                        request -> getViewState().showPermissionDialog(new String[]{ACCESS_FINE_LOCATION}),
                        Timber::d
                );
        unsubscribeOnDestroy(disposable);
    }

    private void startGameOverListener() {
        Disposable disposable = interactor.startGameOverListener()
                .observeOn(schedulersProvider.ui())
                .subscribe(
                        isGameOver -> navigateToScreen(new GameScreens.CongratulationScreen()),
                        Timber::d
                );
        unsubscribeOnDestroy(disposable);
    }

    public void startMessageListener() {
        Disposable disposable = interactor.startMessageListener()
                .observeOn(schedulersProvider.ui())
                .subscribe(
                        message -> {
                            if (!screenKeys.isEmpty() && !screenKeys.getFirst().equals("MessageScreen"))
                                getViewState().setMessageRingVisible(true);
                        },
                        Timber::d
                );
        unsubscribeOnDestroy(disposable);
    }

    public void permissionGranted() {
        if (!interactor.isServiceRunning()) {
            getViewState().startGameService();
        }
    }

    public void permissionDenied() {
        getViewState().showError(R.string.access_location_was_denied);
        getViewState().navigateMain();
    }
}
