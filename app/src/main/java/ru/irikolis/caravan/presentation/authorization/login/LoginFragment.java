package ru.irikolis.caravan.presentation.authorization.login;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.domain.interactors.AuthInteractor;
import ru.irikolis.caravan.presentation.configuration.ConfigurationActivity;
import ru.irikolis.caravan.utils.KeyboardUtils;
import ru.irikolis.caravan.utils.OnTextChanged;
import ru.irikolis.caravan.utils.SchedulersProvider;
import ru.irikolis.caravan.utils.IpAddressInputFilter;
import ru.irikolis.widgets.LoadingButtonView;

import static androidx.core.content.ContextCompat.getDrawable;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class LoginFragment extends MvpAppCompatFragment implements LoginView {
    @BindView(R.id.edit_text_ip_address) EditText edtAddress;
    @BindView(R.id.edit_text_username) EditText edtUsername;
    @BindView(R.id.edit_text_password) EditText edtPassword;
    @BindView(R.id.loading_button) LoadingButtonView btnSignIn;

    @InjectPresenter LoginPresenter presenter;
    @Inject AuthInteractor interactor;
    @Inject SchedulersProvider schedulersProvider;

    @ProvidePresenter
    LoginPresenter providePresenter() {
        CaravanApplication.getInstance().getAuthComponent().inject(this);
        return new LoginPresenter(interactor, schedulersProvider);
    }

    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        edtAddress.setFilters(new InputFilter[] { new IpAddressInputFilter() });
        edtAddress.addTextChangedListener(new OnTextChanged() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                presenter.onAddressChanged(edtAddress.getText().toString());            }
        });
        edtUsername.addTextChangedListener(new OnTextChanged() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                presenter.onUsernameChanged(edtUsername.getText().toString());            }
        });
        edtPassword.addTextChangedListener(new OnTextChanged() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                presenter.onPasswordChanged(edtPassword.getText().toString());            }
        });
        btnSignIn.setOnClickListener(v -> {
            KeyboardUtils.hideKeyboard(v);
            presenter.onSignInClicked(edtAddress.getText().toString(), edtUsername.getText().toString(), edtPassword.getText().toString());
        });
   }

    @Override
    public void showLoginError() {
        Toast.makeText(getActivity(), R.string.invalid_login, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLoadingError() {
        Toast.makeText(getActivity(), R.string.error_loading_network_data, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateGameSelection() {
        Intent intent = new Intent(getActivity(), ConfigurationActivity.class);
        startActivity(intent);
    }

    @Override
    public void showLoading(boolean loading, boolean animated) {
        if (animated)
            btnSignIn.setAnimatedLoading(loading);
        else
            btnSignIn.setLoading(loading);
    }

    @Override
    public void setIpAddress(String address) {
        edtAddress.setText(address);
    }

    @Override
    public void setIpAddressValid(Boolean valid) {
        if (getContext() != null) {
            Drawable drawable = (valid) ? null : getDrawable(getContext(), R.drawable.ic_data_error_24dp);
            edtAddress.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
        }
    }

    @Override
    public void setUsernameValid(Boolean valid) {
        if (getContext() != null) {
            Drawable drawable = (valid) ? null : getDrawable(getContext(), R.drawable.ic_data_error_24dp);
            edtUsername.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
        }
    }

    @Override
    public void setPasswordValid(Boolean valid) {
        if (getContext() != null) {
            Drawable drawable = (valid) ? null : getDrawable(getContext(), R.drawable.ic_data_error_24dp);
            edtPassword.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
        }
    }
}
