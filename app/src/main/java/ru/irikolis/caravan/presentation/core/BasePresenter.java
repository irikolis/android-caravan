package ru.irikolis.caravan.presentation.core;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings({"unused", "RedundantSuppression"})
public abstract class BasePresenter<T extends MvpView> extends MvpPresenter<T> {
    private CompositeDisposable compositeDisposable;
    private CompositeDisposable compositeDisposableOnView;

    public BasePresenter() {
        compositeDisposable = new CompositeDisposable();
        compositeDisposableOnView = new CompositeDisposable();
    }

    protected void unsubscribeOnDestroy(@NonNull Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    protected void unsubscribeOnDestroy(@NonNull Disposable... disposables) {
        compositeDisposable.addAll(disposables);
    }

    protected void unsubscribeOnDestroyView(@NonNull Disposable disposable) {
        compositeDisposableOnView.add(disposable);
    }

    protected void unsubscribeOnDestroyView(@NonNull Disposable... disposables) {
        compositeDisposableOnView.addAll(disposables);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }

    @Override
    public void destroyView(T view) {
        super.destroyView(view);
        compositeDisposableOnView.clear();
    }
}
