package ru.irikolis.caravan.presentation.configuration.configurator;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextWatcher;
import android.transition.TransitionInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.text.MessageFormat;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.application.AppConstants;
import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.domain.interactors.ConfiguratorInteractor;
import ru.irikolis.caravan.domain.models.Route;
import ru.irikolis.caravan.presentation.core.BaseFragment;
import ru.irikolis.caravan.presentation.main.MainActivity;
import ru.irikolis.caravan.presentation.core.CustomFragmentTransition;
import ru.irikolis.caravan.presentation.configuration.ConfigurationActivity;
import ru.irikolis.caravan.utils.SchedulersProvider;
import ru.irikolis.widgets.GameDataView;
import ru.irikolis.widgets.LoadingButtonView;
import ru.terrakok.cicerone.Router;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ConfiguratorFragment extends BaseFragment implements ConfiguratorView, CustomFragmentTransition {
    private static final String ARG_ROUTE_ID = "route_id";
    private static final String ARG_ROUTE_NAME = "route_name";

    @BindView(R.id.edit_text_gamer_name) EditText edtGamerName;
    @BindView(R.id.data_view_map) GameDataView dataViewMap;
    @BindView(R.id.data_view_artifact) GameDataView dataViewArtifact;
    @BindView(R.id.data_view_geo_point) GameDataView dataViewGeoPoint;
    @BindView(R.id.data_view_rules) GameDataView dataViewRules;
    @BindView(R.id.text_view_route_description) TextView txtRouteDescription;
    @BindView(R.id.button_configure) LoadingButtonView btnConfigure;

    @Inject Router router;
    @Inject ConfiguratorInteractor interactor;
    @Inject SchedulersProvider schedulersProvider;
    @InjectPresenter ConfiguratorPresenter presenter;

    @ProvidePresenter
    ConfiguratorPresenter providePresenter() {
        CaravanApplication.getInstance().getConfigurationComponent().inject(this);
        int routeId = 0;
        if (getArguments() != null) {
            routeId = getArguments().getInt(ARG_ROUTE_ID);
        }
        return new ConfiguratorPresenter(router, interactor, schedulersProvider, routeId);
    }

    public ConfiguratorFragment() {
        // Required empty public constructor
    }

    public static ConfiguratorFragment newInstance(Route route) {
        ConfiguratorFragment fragment = new ConfiguratorFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_ROUTE_ID, route.getRouteId());
        args.putString(ARG_ROUTE_NAME, route.getName());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_configurator;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // animation
        TransitionInflater inflater = TransitionInflater.from(getContext());
        setAllowEnterTransitionOverlap(false);
        setEnterTransition(inflater.inflateTransition(R.transition.trans_slide_end));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        if (getArguments() != null)
            ((ConfigurationActivity) requireActivity()).setTitle(getArguments().getString(ARG_ROUTE_NAME));

        edtGamerName.addTextChangedListener(gamerNameTextWatcher);
        btnConfigure.setOnClickListener(v -> presenter.onConfigureClicked(edtGamerName.getText().toString()));

        dataViewMap.setMaxDays(AppConstants.MAX_GAME_DAYS);
        dataViewMap.setOnClickListener(v -> presenter.onMapUpdateClicked());

        dataViewArtifact.setMaxDays(AppConstants.MAX_GAME_DAYS);
        dataViewArtifact.setOnClickListener(v -> presenter.onArtifactUpdateClicked());

        dataViewGeoPoint.setMaxDays(AppConstants.MAX_GAME_DAYS);
        dataViewGeoPoint.setOnClickListener(v -> presenter.onGeoPointUpdateClicked());

        dataViewRules.setMaxDays(AppConstants.MAX_GAME_DAYS);
        dataViewRules.setOnClickListener(v -> presenter.onRulesUpdateClicked());
    }

    @Override
    public void addTransaction(@NotNull Fragment currentFragment,
                               @NotNull Fragment nextFragment,
                               @NotNull FragmentTransaction fragmentTransaction) {
        fragmentTransaction.addSharedElement(dataViewMap.getImgData(), dataViewMap.getImgData().getTransitionName());
    }

    private TextWatcher gamerNameTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            presenter.onGamerNameChanged(s.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    public void showError(String text) {
        Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showError(@StringRes int text) {
        showError(getString(text));
    }

    @Override
    public void setGamerNameValid(boolean valid) {
        if (getContext() != null) {
            Drawable drawable = (valid) ? null : ContextCompat.getDrawable(getContext(), R.drawable.ic_data_warring_24dp);
            edtGamerName.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
        }
    }

    @Override
    public void setMapImage(String fileName) {
        if (getContext() != null) {
            String url = MessageFormat.format("file:///{0}/{1}", getContext().getFilesDir(), fileName);
            Picasso.get()
                    .load(url)
                    .placeholder(R.drawable.ic_image_placeholder_24dp)
                    .error(R.drawable.ic_image_error_24dp)
                    .into(dataViewMap.getImgData(), new Callback() {
                        @Override
                        public void onSuccess() {
                            dataViewMap.getImgData().setTransitionName(getResources().getString(R.string.trans_map_preview));
                            dataViewMap.getImgData().setOnClickListener(v -> presenter.onMapClicked());
                        }

                        @Override
                        public void onError(Exception e) {
                        }
                    });
        }
    }

    @Override
    public void setDescription(String description) {
        txtRouteDescription.setText(description);
    }

    @Override
    public void setMapInformation(long time) {
        dataViewMap.setLastUpdateTime(time);
    }

    @Override
    public void showMapLoading(boolean loading) {
        dataViewMap.setLoading(loading);
    }

    @Override
    public void showArtifactLoading(boolean loading) {
        dataViewArtifact.setLoading(loading);
    }

    @Override
    public void setArtifactInformation(long time) {
        dataViewArtifact.setLastUpdateTime(time);
    }

    @Override
    public void setGeoPointInformation(long time) {
        dataViewGeoPoint.setLastUpdateTime(time);
    }

    @Override
    public void showGeoPointLoading(boolean loading) {
        dataViewGeoPoint.setLoading(loading);
    }

    @Override
    public void setRulesInformation(long time) {
        dataViewRules.setLastUpdateTime(time);
    }

    @Override
    public void showRulesLoading(boolean loading) {
        dataViewRules.setLoading(loading);
    }

    @Override
    public void showLoading(boolean loading, boolean animated) {
        if (animated)
            btnConfigure.setAnimatedLoading(loading);
        else
            btnConfigure.setLoading(loading);
    }

    @Override
    public void showConfigurationSuccess() {
        Toast.makeText(getContext(), R.string.game_configuration_success, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateMain() {
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}