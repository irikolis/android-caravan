package ru.irikolis.caravan.presentation.configuration.games;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.domain.models.Game;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class GamesAdapter extends RecyclerView.Adapter<GamesAdapter.GameViewHolder> {
    private final DiffUtil.ItemCallback<Game> diffCallback = new DiffUtil.ItemCallback<Game>() {
        @Override
        public boolean areItemsTheSame(@NonNull Game oldItem, @NonNull Game newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Game oldItem, @NonNull Game newItem) {
            return oldItem.equals(newItem);
        }
    };
    private AsyncListDiffer<Game> differ = new AsyncListDiffer<>(this, diffCallback);
    private OnItemClickListener onItemClickListener;

    public GamesAdapter(OnItemClickListener l) {
        onItemClickListener = l;
    }

    @NonNull
    @Override
    public GameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_game, parent, false);
        GameViewHolder holder = new GameViewHolder(view);
        view.setOnClickListener(v -> {
            if (onItemClickListener != null) {
                int position = holder.getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onItemClickListener.onItemClick(getItem(position));
                }
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull GameViewHolder holder, int position) {
        Game game = getItem(position);
        holder.txtName.setText(game.getName());
        holder.txtDescription.setText(game.getDescription());
    }

    @Override
    public int getItemCount() {
        return differ.getCurrentList().size();
    }

    public Game getItem(int position) {
        return differ.getCurrentList().get(position);
    }

    public void setGames(List<Game> games) {
        differ.submitList(games);
    }

    public interface OnItemClickListener {
        void onItemClick(Game game);
    }

    static class GameViewHolder extends RecyclerView.ViewHolder  {
        @BindView(R.id.text_view_game_name) TextView txtName;
        @BindView(R.id.text_view_game_description) TextView txtDescription;

        GameViewHolder(@NonNull View item) {
            super(item);
            ButterKnife.bind(this, item);
        }
    }
}
