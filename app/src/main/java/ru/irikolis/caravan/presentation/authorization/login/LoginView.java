package ru.irikolis.caravan.presentation.authorization.login;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@StateStrategyType(OneExecutionStateStrategy.class)
interface LoginView extends MvpView {
    void showLoginError();
    void showLoadingError();
    void navigateGameSelection();
    void showLoading(boolean show, boolean animated);

    @StateStrategyType(SkipStrategy.class)
    void setIpAddress(String address);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setIpAddressValid(Boolean valid);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setUsernameValid(Boolean valid);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setPasswordValid(Boolean valid);
}
