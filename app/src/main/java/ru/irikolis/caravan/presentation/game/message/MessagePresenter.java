package ru.irikolis.caravan.presentation.game.message;

import com.arellomobile.mvp.InjectViewState;

import ru.irikolis.caravan.domain.interactors.MessageInteractor;
import ru.irikolis.caravan.presentation.core.BasePresenter;

import io.reactivex.disposables.Disposable;
import ru.irikolis.caravan.utils.SchedulersProvider;
import timber.log.Timber;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class MessagePresenter extends BasePresenter<MessageView> {
    private MessageInteractor interactor;
    private SchedulersProvider schedulersProvider;

    public MessagePresenter(MessageInteractor interactor, SchedulersProvider schedulersProvider) {
        super();
        this.interactor = interactor;
        this.schedulersProvider = schedulersProvider;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        // Output the game message logging
        loadAllMessages();
        // Output the current game message
        startMessageListener();
    }

    public void loadAllMessages() {
        Disposable disposable = interactor.getAllMessages()
                .observeOn(schedulersProvider.ui())
                .subscribe(
                        messages -> {
                            if (messages.isEmpty())
                                getViewState().setEmptyText();
                            else
                                getViewState().setAllMessages(messages);
                        },
                        e -> getViewState().setEmptyText()
                );
        unsubscribeOnDestroy(disposable);
    }

    public void startMessageListener() {
        Disposable disposable = interactor.startMessageListener()
                .observeOn(schedulersProvider.ui())
                .subscribe(
                        message -> getViewState().addMessage(message),
                        Timber::d
                );
        unsubscribeOnDestroy(disposable);
    }
}
