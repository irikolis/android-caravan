package ru.irikolis.caravan.presentation.game.artifact;

import com.arellomobile.mvp.InjectViewState;

import ru.irikolis.caravan.domain.interactors.ArtifactInteractor;
import ru.irikolis.caravan.presentation.core.BasePresenter;

import io.reactivex.disposables.Disposable;
import ru.irikolis.caravan.utils.SchedulersProvider;
import timber.log.Timber;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class ArtifactPresenter extends BasePresenter<ArtifactView> {
    private ArtifactInteractor interactor;
    private SchedulersProvider schedulersProvider;

    public ArtifactPresenter(ArtifactInteractor interactor, SchedulersProvider schedulersProvider) {
        super();
        this.interactor = interactor;
        this.schedulersProvider = schedulersProvider;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadCollectedArtifacts();
    }

    public void loadCollectedArtifacts() {
        Disposable disposable = interactor.getCollectedArtifacts()
                .observeOn(schedulersProvider.ui())
                .doOnSubscribe(s -> getViewState().showLoading(true))
                .doAfterTerminate(() -> getViewState().showLoading(false))
                .subscribe(
                        artifacts -> getViewState().setArtifacts(artifacts),
                        Timber::d
                );
        unsubscribeOnDestroy(disposable);
    }

    public void onSearchClicked() {
        Disposable disposable = interactor.searchNewArtifacts()
                .observeOn(schedulersProvider.ui())
                .doOnSubscribe(s -> getViewState().showLoading(true))
                .doAfterTerminate(() -> getViewState().showLoading(false))
                .subscribe(
                    artifacts -> getViewState().addArtifact(artifacts),

                    Timber::d
                );
        unsubscribeOnDestroy(disposable);
    }
}
