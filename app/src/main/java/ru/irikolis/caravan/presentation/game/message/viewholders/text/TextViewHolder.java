package ru.irikolis.caravan.presentation.game.message.viewholders.text;

import android.text.format.DateFormat;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.arellomobile.mvp.MvpDelegate;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.domain.models.BaseMessage;
import ru.irikolis.caravan.domain.models.TextMessage;
import ru.irikolis.caravan.presentation.core.BaseViewHolder;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class TextViewHolder extends BaseViewHolder {
    @BindView(R.id.text_view_sent_time) TextView txtSentTime;
    @BindView(R.id.text_view_posting_time) TextView txtPostingTime;
    @BindView(R.id.text_view_message) TextView txtMessage;

    public TextViewHolder(@NonNull View itemView) {
        super(null, itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    protected String getMvpChildId() {
        return null;
    }

    @SuppressWarnings({"unused", "RedundantSuppression"})
    public static BaseViewHolder createView(MvpDelegate<?> parentDelegate, @NonNull View itemView) {
        return new TextViewHolder(itemView);
    }

    @Override
    public void bindView(BaseMessage message) {
        TextMessage msg = (TextMessage) message;

        txtSentTime.setText(DateFormat.format("Отправленно: HH:mm", msg.getSentTime()));
        txtMessage.setText(msg.getText());
        txtPostingTime.setText(DateFormat.format("HH:mm", msg.getPostingTime()));
    }
}
