package ru.irikolis.caravan.presentation.game.congratulation;

import com.arellomobile.mvp.InjectViewState;
import ru.irikolis.caravan.presentation.core.BasePresenter;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class CongratulationPresenter extends BasePresenter<CongratulationView> {
    public CongratulationPresenter() {
        super();
    }
}
