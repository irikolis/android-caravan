package ru.irikolis.caravan.presentation.core.mediaplayer;

import android.media.MediaPlayer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ExpandedMediaPlayer extends MediaPlayer {
    private static final int NO_TRACK = -1;
    private int currentTrackId = NO_TRACK;

    public ExpandedMediaPlayer() {
        setOnCompletionListener(mp -> stopCurrentTrack());
    }

    public interface OnChangedTrackListener {
        void onChangedTrack(int currentTrackId);
    }

    private List<OnChangedTrackListener> listeners = new ArrayList<>();

    public void addOnChangedTrackListener(OnChangedTrackListener l) {
        listeners.add(l);
        l.onChangedTrack(currentTrackId);
    }

    public void removeOnChangedTrackListener(OnChangedTrackListener l) {
        listeners.remove(l);
    }

    public void clearOnChangedTrackListeners() {
        listeners.clear();
    }

    public int getCurrentTrackId() {
        return currentTrackId;
    }

    public boolean isPlayingTrack(int trackId) {
        return currentTrackId == trackId;
    }

    public void setCurrentTrackId(int currentTrackId) {
        this.currentTrackId = currentTrackId;
        for (OnChangedTrackListener l: listeners)
            l.onChangedTrack(currentTrackId);
    }

    public void startTrack(int trackId) {
        if (currentTrackId != NO_TRACK && trackId != currentTrackId)
            stopTrack(currentTrackId);
        setCurrentTrackId(trackId);
        super.start();
    }

    public void stopTrack(int trackId) {
        if (trackId != -1 && trackId == currentTrackId) {
            setCurrentTrackId(NO_TRACK);
            stop();
            reset();
        }
    }

    public void stopCurrentTrack() {
        stopTrack(getCurrentTrackId());
    }
}
