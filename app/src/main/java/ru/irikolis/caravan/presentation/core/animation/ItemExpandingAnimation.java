package ru.irikolis.caravan.presentation.core.animation;

import android.animation.ValueAnimator;
import android.view.View;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ItemExpandingAnimation {
    private View expandedView;
    private View arrowView = null;
    private long duration = 300;
    private long startDelay = 0;

    private int realViewHeight;

    public ItemExpandingAnimation(View expandedView) {
        this.expandedView = expandedView;

        // the max width is necessary, so it can calculate how height the view is when we give UNSPECIFIED
        int width = View.MeasureSpec.makeMeasureSpec(((View) expandedView.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int height = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        expandedView.measure(width, height);

        realViewHeight = expandedView.getMeasuredHeight();

        expandedView.getLayoutParams().height = 0;
        expandedView.setVisibility(View.VISIBLE);
    }

    public ItemExpandingAnimation setArrowView(View arrowView) {
        this.arrowView = arrowView;
        return this;
    }

    public ItemExpandingAnimation setDuration(long duration) {
        this.duration = duration;
        return this;
    }

    public ItemExpandingAnimation setStartDelay(long startDelay) {
        this.startDelay = startDelay;
        return this;
    }

    public void start() {
        if (arrowView != null)
            arrowView.animate().setDuration(duration).setStartDelay(startDelay).rotation(180F);

        ValueAnimator animator = ValueAnimator.ofInt(0, realViewHeight);
        animator.setDuration(duration);
        animator.setStartDelay(startDelay);
        animator.addUpdateListener(animation -> {
            expandedView.getLayoutParams().height = (int) animation.getAnimatedValue();
            expandedView.requestLayout();
        });
        animator.start();
    }
}
