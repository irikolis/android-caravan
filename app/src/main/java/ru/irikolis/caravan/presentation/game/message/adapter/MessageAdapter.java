package ru.irikolis.caravan.presentation.game.message.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.MvpDelegate;

import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.presentation.core.BaseViewHolder;
import ru.irikolis.caravan.presentation.core.mediaplayer.ExpandedMediaPlayer;
import ru.irikolis.caravan.presentation.game.message.viewholders.audio.AudioViewHolder;
import ru.irikolis.caravan.domain.models.BaseMessage;
import ru.irikolis.caravan.domain.models.MessageType;
import ru.irikolis.caravan.presentation.game.message.viewholders.target.TargetViewHolder;
import ru.irikolis.caravan.presentation.game.message.viewholders.text.TextViewHolder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class MessageAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private List<BaseMessage> messages = new ArrayList<>();
    private MvpDelegate<?> parentDelegate;

    @Inject ExpandedMediaPlayer mediaPlayer;

    public MessageAdapter(MvpDelegate<?> parentDelegate) {
        CaravanApplication.getInstance().getGameComponent().inject(this);
        this.parentDelegate = parentDelegate;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);

        if (viewType == MessageType.AUDIO_MESSAGE.getLayoutId())
            return AudioViewHolder.createView(parentDelegate, itemView);

        if (viewType == MessageType.TEXT_MESSAGE.getLayoutId())
            return TextViewHolder.createView(null, itemView);

        if (viewType == MessageType.TARGET_MESSAGE.getLayoutId())
            return TargetViewHolder.createView(null, itemView);

        throw new RuntimeException("Unknown message type");
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.bindView(messages.get(position));
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    @Override
    public int getItemViewType(int position) {
        return messages.get(position).getMessageType().getLayoutId();
    }

    public void setMessages(List<BaseMessage> messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }

    public void addMessage(BaseMessage message) {
        this.messages.add(message);
        notifyItemInserted(messages.size()-1);
    }

    public void destroy(boolean changedConfiguration) {
        if (!changedConfiguration)
            mediaPlayer.stopCurrentTrack();
        mediaPlayer.clearOnChangedTrackListeners();
    }
}
