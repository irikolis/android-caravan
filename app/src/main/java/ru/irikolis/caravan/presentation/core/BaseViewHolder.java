package ru.irikolis.caravan.presentation.core;

import android.view.View;

import androidx.annotation.NonNull;

import com.arellomobile.mvp.MvpDelegate;

import ru.irikolis.caravan.domain.models.BaseMessage;
import ru.irikolis.caravan.presentation.core.mvp.MvpViewHolder;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public abstract class BaseViewHolder extends MvpViewHolder {
    public BaseViewHolder(MvpDelegate<?> parentDelegate, @NonNull View itemView) {
        super(parentDelegate, itemView);
    }

    public abstract void bindView(BaseMessage message);

    public void onDestroy() {
    }
}
