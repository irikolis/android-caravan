package ru.irikolis.caravan.presentation.game.map;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import ru.irikolis.caravan.domain.models.Coord;
import ru.irikolis.caravan.domain.models.GameMap;
import ru.irikolis.caravan.domain.models.TargetType;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface MapView extends MvpView {
    void setZoom(float zoom);
    void setMapImage(String fileName);
    void initMapImage(GameMap gameMap);
    void drawLocation(String title, Coord coord, boolean visible);

    @StateStrategyType(AddToEndStrategy.class)
    void drawTarget(TargetType type, String title, Coord coord, boolean visible);
    @StateStrategyType(AddToEndStrategy.class)
    void drawCompletedTarget(String title, Coord coord, boolean visible);
}
