package ru.irikolis.caravan.presentation.game.message.viewholders.audio;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface AudioViewHolderView extends MvpView {

    void setPlaybackToggle(boolean playing);
    void setTimedText(long startTime, long finalTime);
    void setMediaPlayerSlider(int position);
    void updateTrackbar();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void playTrack(int trackId, String audioUrl, int startTime);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void stopTrack(int trackId);
}
