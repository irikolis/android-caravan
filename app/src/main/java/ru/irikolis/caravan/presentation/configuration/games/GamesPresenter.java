package ru.irikolis.caravan.presentation.configuration.games;

import com.arellomobile.mvp.InjectViewState;

import io.reactivex.disposables.Disposable;
import ru.irikolis.caravan.domain.interactors.GamesInteractor;
import ru.irikolis.caravan.domain.models.Game;
import ru.irikolis.caravan.presentation.core.BasePresenter;
import ru.irikolis.caravan.presentation.configuration.ConfigurationScreens;
import ru.irikolis.caravan.utils.SchedulersProvider;
import ru.terrakok.cicerone.Router;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class GamesPresenter extends BasePresenter<GamesView> {
    private Router router;
    private GamesInteractor interactor;
    private SchedulersProvider schedulersProvider;

    GamesPresenter(Router router, GamesInteractor interactor, SchedulersProvider schedulersProvider) {
        super();
        this.router = router;
        this.interactor = interactor;
        this.schedulersProvider = schedulersProvider;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadGames();
    }

    public void loadGames() {
        Disposable disposable = interactor.getAllGames()
                .observeOn(schedulersProvider.ui())
                .doOnSubscribe(s -> getViewState().showRefreshing(true))
                .doOnEvent((v, throwable) -> getViewState().showRefreshing(false))
                .subscribe(
                        games -> getViewState().setAllGames(games),
                        ex -> getViewState().setEmptyText()
                );
        unsubscribeOnDestroy(disposable);
    }

    public void onItemClicked(Game game) {
        router.navigateTo(new ConfigurationScreens.RoutesScreen(game));
    }
}
