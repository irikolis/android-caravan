package ru.irikolis.caravan.presentation.game;

import androidx.fragment.app.Fragment;

import ru.irikolis.caravan.presentation.game.artifact.ArtifactFragment;
import ru.irikolis.caravan.presentation.game.congratulation.CongratulationFragment;
import ru.irikolis.caravan.presentation.game.information.InformationFragment;
import ru.irikolis.caravan.presentation.game.map.MapFragment;
import ru.irikolis.caravan.presentation.game.message.MessageFragment;
import ru.irikolis.caravan.presentation.game.rules.RulesFragment;
import ru.terrakok.cicerone.android.support.SupportAppScreen;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GameScreens {
    public static final class InformationScreen extends SupportAppScreen {
        @Override
        public Fragment getFragment() {
            return InformationFragment.newInstance();
        }
    }

    public static final class CongratulationScreen extends SupportAppScreen {
        @Override
        public Fragment getFragment() {
            return CongratulationFragment.newInstance();
        }
    }

    public static final class ArtifactScreen extends SupportAppScreen {
        @Override
        public Fragment getFragment() {
            return ArtifactFragment.newInstance();
        }
    }

    public static final class MapScreen extends SupportAppScreen {
        @Override
        public Fragment getFragment() {
            return MapFragment.newInstance();
        }
    }

    public static final class MessageScreen extends SupportAppScreen {
        @Override
        public Fragment getFragment() {
            return MessageFragment.newInstance();
        }
    }

    public static final class RulesScreen extends SupportAppScreen {
        @Override
        public Fragment getFragment() {
            return RulesFragment.newInstance();
        }
    }
}
