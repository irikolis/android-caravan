package ru.irikolis.caravan.presentation.game.rules;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;

import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.presentation.core.BaseFragment;
import ru.irikolis.caravan.presentation.game.GameActivity;

import java.text.MessageFormat;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class RulesFragment extends BaseFragment implements RulesView {
    @BindView(R.id.web_view_rules) WebView webRules;

    @InjectPresenter RulesPresenter presenter;

    @ProvidePresenter
    RulesPresenter providePresenter() {
        CaravanApplication.getInstance().getGameComponent().inject(this);
        return new RulesPresenter();
    }

    public RulesFragment() {
        // Required empty public constructor
    }

    public static RulesFragment newInstance() {
        return new RulesFragment();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_rules;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        ((GameActivity) requireActivity()).setTitle(getString(R.string.game_rules));
        webRules.setWebViewClient(webViewClient);
    }

    @Override
    public void loadRules(String fileName) {
        String url = MessageFormat.format("file:///{0}/{1}", requireActivity().getFilesDir(), fileName);
        webRules.loadUrl(url);
    }

    WebViewClient webViewClient = new WebViewClient() {

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            // the external hyperlinks are disabled
            return true;
        }
    };
}
