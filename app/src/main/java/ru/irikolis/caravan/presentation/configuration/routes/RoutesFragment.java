package ru.irikolis.caravan.presentation.configuration.routes;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.transition.TransitionInflater;
import android.view.View;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.domain.interactors.RoutesInteractor;
import ru.irikolis.caravan.domain.models.Game;
import ru.irikolis.caravan.domain.models.Route;
import ru.irikolis.caravan.presentation.core.BaseFragment;
import ru.irikolis.caravan.presentation.configuration.ConfigurationActivity;
import ru.irikolis.caravan.utils.SchedulersProvider;
import ru.terrakok.cicerone.Router;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class RoutesFragment extends BaseFragment implements RoutesView {
    private static final String ARG_GAME_ID = "game_id";

    @BindView(R.id.swipe_refresh_route) SwipeRefreshLayout swipe;
    @BindView(R.id.recycler_view_routes) RecyclerView recyclerRoute;
    @BindView(R.id.text_view_empty_routes) TextView txtEmpty;

    private RouteAdapter adapter;

    @Inject Router router;
    @Inject RoutesInteractor interactor;
    @Inject SchedulersProvider schedulersProvider;
    @InjectPresenter RoutesPresenter presenter;

    @ProvidePresenter
    RoutesPresenter providePresenter() {
        CaravanApplication.getInstance().getConfigurationComponent().inject(this);
        int gameId = 0;
        if (getArguments() != null) {
            gameId = getArguments().getInt(ARG_GAME_ID);
        }
        return new RoutesPresenter(router, interactor, schedulersProvider, gameId);
    }

    public RoutesFragment() {
        // Required empty public constructor
    }

    public static RoutesFragment newInstance(Game game) {
        RoutesFragment fragment = new RoutesFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_GAME_ID, game.getId());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_routes;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // animation
        TransitionInflater inflater = TransitionInflater.from(getContext());
        setAllowEnterTransitionOverlap(false);
        setEnterTransition(inflater.inflateTransition(R.transition.trans_slide_end));
        setAllowReturnTransitionOverlap(false);
        setExitTransition(inflater.inflateTransition(R.transition.trans_slide_start));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        ((ConfigurationActivity) requireActivity()).setTitle(getString(R.string.route_list));

        adapter = new RouteAdapter(route -> presenter.onItemClicked(route));
        recyclerRoute.setAdapter(adapter);
        swipe.setOnRefreshListener(() -> presenter.loadRoutes());
    }

    @Override
    public void setAllRoutes(List<Route> routes) {
        txtEmpty.setVisibility(View.GONE);
        recyclerRoute.setVisibility(View.VISIBLE);
        adapter.setRoutes(routes);
        recyclerRoute.scrollToPosition(adapter.getItemCount()-1);
    }

    @Override
    public void setEmptyText() {
        recyclerRoute.setVisibility(View.GONE);
        txtEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void showRefreshing(boolean refreshing) {
        swipe.setRefreshing(refreshing);
    }
}