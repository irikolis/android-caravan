package ru.irikolis.caravan.presentation.game.information;

import com.arellomobile.mvp.InjectViewState;
import ru.irikolis.caravan.domain.interactors.InformationInteractor;
import ru.irikolis.caravan.presentation.core.BasePresenter;

import io.reactivex.disposables.Disposable;
import ru.irikolis.caravan.utils.SchedulersProvider;
import timber.log.Timber;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class InformationPresenter extends BasePresenter<InformationView> {
    private InformationInteractor interactor;
    private SchedulersProvider schedulersProvider;

    public InformationPresenter(InformationInteractor interactor, SchedulersProvider schedulersProvider) {
        super();
        this.interactor = interactor;
        this.schedulersProvider = schedulersProvider;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().setRouteName(interactor.getRouteName());
        getViewState().setRouteDescription(interactor.getRouteDescription());
        getViewState().setGamerName(interactor.getGamerName());

        updateInformation();
        startCompletedTargetListener();
    }

    public void updateInformation() {
        getViewState().setCompletedTargetsInformation(interactor.getCompletedNumberOfNecessaryTargets());
        getViewState().setRemainedTargetsInformation(interactor.getNumberOfNecessaryTargets());
        getViewState().setOptionalTargetsInformation(interactor.getCompletedNumberOfOptionalTargets());
    }

    public void startCompletedTargetListener() {
        Disposable disposable = interactor.startCompletedTargetListener()
                .observeOn(schedulersProvider.ui())
                .subscribe(
                        message -> updateInformation(),
                        Timber::d
                );
        unsubscribeOnDestroy(disposable);
    }
}
