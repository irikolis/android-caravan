package ru.irikolis.caravan.presentation.authorization;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@StateStrategyType(OneExecutionStateStrategy.class)
interface AuthView extends MvpView {
    void navigateMain();
}
