package ru.irikolis.caravan.presentation.authorization;

import com.arellomobile.mvp.InjectViewState;

import ru.irikolis.caravan.presentation.core.BasePresenter;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class AuthPresenter extends BasePresenter<AuthView> {

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }
}
