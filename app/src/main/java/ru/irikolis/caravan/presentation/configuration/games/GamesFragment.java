package ru.irikolis.caravan.presentation.configuration.games;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.transition.TransitionInflater;
import android.view.View;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.domain.interactors.GamesInteractor;
import ru.irikolis.caravan.domain.models.Game;
import ru.irikolis.caravan.presentation.core.BaseFragment;
import ru.irikolis.caravan.presentation.configuration.ConfigurationActivity;
import ru.irikolis.caravan.utils.SchedulersProvider;
import ru.terrakok.cicerone.Router;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GamesFragment extends BaseFragment implements GamesView {
    @BindView(R.id.swipe_refresh_game) SwipeRefreshLayout swipe;
    @BindView(R.id.recycler_view_games) RecyclerView recyclerGame;
    @BindView(R.id.text_view_empty_games) TextView txtEmpty;

    @Inject Router router;
    @Inject GamesInteractor interactor;
    @Inject SchedulersProvider schedulersProvider;
    @InjectPresenter GamesPresenter presenter;

    private GamesAdapter adapter;

    @ProvidePresenter
    GamesPresenter providePresenter() {
        CaravanApplication.getInstance().getConfigurationComponent().inject(this);
        return new GamesPresenter(router, interactor, schedulersProvider);
    }

    public GamesFragment() {
        // Required empty public constructor
    }

    public static GamesFragment newInstance() {
        return new GamesFragment();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_games;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // animation
        TransitionInflater inflater = TransitionInflater.from(getContext());
        setAllowReturnTransitionOverlap(false);
        setExitTransition(inflater.inflateTransition(R.transition.trans_slide_start));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        ((ConfigurationActivity) requireActivity()).setTitle(getString(R.string.game_list));

        adapter = new GamesAdapter(game -> presenter.onItemClicked(game));
        recyclerGame.setAdapter(adapter);
        swipe.setOnRefreshListener(presenter::loadGames);
    }

    @Override
    public void setAllGames(List<Game> games) {
        txtEmpty.setVisibility(View.GONE);
        recyclerGame.setVisibility(View.VISIBLE);
        adapter.setGames(games);
        recyclerGame.scrollToPosition(adapter.getItemCount()-1);
    }

    @Override
    public void setEmptyText() {
        recyclerGame.setVisibility(View.GONE);
        txtEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void showRefreshing(boolean refreshing) {
        swipe.setRefreshing(refreshing);
    }
}