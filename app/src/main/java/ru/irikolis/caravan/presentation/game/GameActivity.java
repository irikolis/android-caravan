package ru.irikolis.caravan.presentation.game;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.google.android.material.navigation.NavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.domain.interactors.GameInteractor;
import ru.irikolis.caravan.presentation.game.finish.FinishDialogFragment;
import ru.irikolis.caravan.presentation.main.MainActivity;
import ru.irikolis.caravan.data.service.GameService;
import ru.irikolis.caravan.utils.SchedulersProvider;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

import javax.inject.Inject;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GameActivity extends MvpAppCompatActivity implements GameView {
    public static final String PARAM_MENU_ITEM_ID = "menu_item_id";
    public static final String PARAM_SOUND_ID = "sound_id";

    @BindView(R.id.toolbar_back) Toolbar toolbar;
    @BindView(R.id.drawer_layout_game) DrawerLayout drawerLayout;
    @BindView(R.id.navigation_view_game) NavigationView navigationView;

    private GameNavigator navigator;

    @Inject Router router;
    @Inject NavigatorHolder navigatorHolder;
    @Inject GameInteractor interactor;
    @Inject SchedulersProvider schedulersProvider;
    @InjectPresenter GamePresenter presenter;

    @ProvidePresenter
    GamePresenter providePresenter() {
        return new GamePresenter(router, interactor, schedulersProvider);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        // if the service has sent intent
        Bundle params = intent.getExtras();
        if(params != null) {
            if (params.containsKey(PARAM_MENU_ITEM_ID)) {
                onItemClicked(params.getInt(PARAM_MENU_ITEM_ID));
            }
            if (params.containsKey(PARAM_SOUND_ID)) {
                int sound_id = params.getInt(PARAM_SOUND_ID);
                presenter.playCongratulation(sound_id);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        CaravanApplication.getInstance().getGameComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);

        navigator = new GameNavigator(this, R.id.frame_container);
        toolbar.inflateMenu(R.menu.menu_toolbar_game);
        toolbar.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.item_ring_message) {
                presenter.onMessageItemClicked();
                return true;
            }
            return false;
        });
        initNavigateView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    private void initNavigateView() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(item -> {
            drawerLayout.closeDrawer(GravityCompat.START);
            onItemClicked(item.getItemId());
            return true;
        });
    }

    public void onItemClicked(@IdRes int itemId) {
        MenuItem currentItem = navigationView.getCheckedItem();
        if (currentItem != null && currentItem.getItemId() == itemId)
            return;

        switch (itemId) {
            case R.id.item_main:
                presenter.onMainItemClicked();
                break;
            case R.id.item_messages:
                presenter.onMessageItemClicked();
                break;
            case R.id.item_artifacts:
                presenter.onArtifactItemClicked();
                break;
            case R.id.item_map:
                presenter.onMapItemClicked();
                break;
            case R.id.item_game_rules:
                presenter.onGameRulesItemClicked();
                break;
            case R.id.item_game_abort:
                presenter.onGameAbortItemClicked();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.isCheckable())
            item.setChecked(true);
        return true;
    }

    @Override
    public void onBackPressed() {
        MenuItem item = navigationView.getCheckedItem();
        if (item != null && item.getItemId() != R.id.item_main && item.getItemId() != R.id.item_game_abort)
            presenter.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            presenter.permissionGranted();
        } else {
            presenter.permissionDenied();
        }
    }

    @Override
    public void startGameService() {
        Intent intent = new Intent(this, GameService.class);
        ContextCompat.startForegroundService(this, intent);
    }

    @Override
    public void navigateMain() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showError(@StringRes int text) {
        Toast.makeText(this, getString(text), Toast.LENGTH_LONG).show();
    }


    @Override
    public void showPermissionDialog(String[] permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, 0);
        }
    }

    @Override
    public void showGameFinishDialog() {
        FinishDialogFragment dialog = FinishDialogFragment.newInstance();
        dialog.show(getSupportFragmentManager(), "FinishDialog");
    }

    public void confirmFinishDialog() {
        presenter.setFinishDialogResult();
    }

    @Override
    public void finishGame() {
        Intent intent = new Intent(this, GameService.class);
        stopService(intent);
        finish();
    }

    @Override
    public void setMapItemEnabled(boolean enabled) {
        navigationView.getMenu().findItem(R.id.item_map).setEnabled(enabled);
    }

    @Override
    public void setMessageRingVisible(boolean visible) {
        toolbar.getMenu().findItem(R.id.item_ring_message).setVisible(visible);
    }

    @Override
    public void setTitle(String title) {
        toolbar.setTitle(title);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CaravanApplication.getInstance().clearGameComponent();
    }
}
