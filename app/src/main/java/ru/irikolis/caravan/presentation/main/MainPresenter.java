package ru.irikolis.caravan.presentation.main;

import com.arellomobile.mvp.InjectViewState;
import ru.irikolis.caravan.domain.interactors.MainInteractor;
import ru.irikolis.caravan.domain.models.GameState;
import ru.irikolis.caravan.presentation.core.BasePresenter;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class MainPresenter extends BasePresenter<MainView> {
    private MainInteractor interactor;

    public MainPresenter(MainInteractor interactor) {
        super();
        this.interactor = interactor;
    }

    @Override
    public void attachView(MainView view) {
        super.attachView(view);
        checkGameInitialized();
    }

    public void checkGameInitialized() {
        String gameState = interactor.getGameState();
        if (gameState.equals(GameState.GAME_STATE_INITIALIZED) || gameState.equals(GameState.GAME_STATE_IN_PROCESS)) {
            interactor.setGameState(GameState.GAME_STATE_IN_PROCESS);
            getViewState().setGameEnabled(true);
        }
        else {
            getViewState().setGameEnabled(false);
        }
    }

    public void onGameClicked() {
        getViewState().navigateGame();
    }

    public void onMasterSettingsClicked() {
        getViewState().navigateAuth();
    }

}
