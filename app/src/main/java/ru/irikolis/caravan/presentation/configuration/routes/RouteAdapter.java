package ru.irikolis.caravan.presentation.configuration.routes;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.domain.models.Route;

import java.util.List;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class RouteAdapter extends RecyclerView.Adapter<RouteAdapter.RouteViewHolder> {
    private final DiffUtil.ItemCallback<Route> diffCallback = new DiffUtil.ItemCallback<Route>() {
        @Override
        public boolean areItemsTheSame(@NonNull Route oldItem, @NonNull Route newItem) {
            return oldItem.getRouteId() == newItem.getRouteId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Route oldItem, @NonNull Route newItem) {
            return oldItem.equals(newItem);
        }
    };

    private AsyncListDiffer<Route> differ = new AsyncListDiffer<>(this, diffCallback);
    private OnItemClickListener onItemClickListener;

    public RouteAdapter(OnItemClickListener l) {
        onItemClickListener = l;
    }

    @NonNull
    @Override
    public RouteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_route, parent, false);
        RouteViewHolder holder = new RouteViewHolder(view);
        view.setOnClickListener(v -> {
            if (onItemClickListener != null) {
                int position = holder.getAdapterPosition();
                if (position != RecyclerView.NO_POSITION)
                    onItemClickListener.onItemClick(getItem(position));
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RouteViewHolder holder, int position) {
        Route route = getItem(position);
        holder.txtName.setText(route.getName());
        holder.ratingLevel.setRating(route.getLevel());
        holder.txtDescription.setText(route.getDescription());
    }

    @Override
    public int getItemCount() {
        return differ.getCurrentList().size();
    }

    public Route getItem(int position) {
        return differ.getCurrentList().get(position);
    }

    public void setRoutes(List<Route> routes) {
        differ.submitList(routes);
    }

    public interface OnItemClickListener {
        void onItemClick(Route route);
    }

    static class RouteViewHolder extends RecyclerView.ViewHolder  {
        @BindView(R.id.text_view_route_name) TextView txtName;
        @BindView(R.id.rating_bar_level) RatingBar ratingLevel;
        @BindView(R.id.text_view_route_description) TextView txtDescription;

        RouteViewHolder(@NonNull View item) {
            super(item);
            ButterKnife.bind(this, item);
        }
    }
}
