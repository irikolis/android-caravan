package ru.irikolis.caravan.presentation.configuration;

import com.arellomobile.mvp.InjectViewState;

import ru.irikolis.caravan.presentation.core.BasePresenter;
import ru.terrakok.cicerone.Router;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class ConfigurationPresenter extends BasePresenter<ConfigurationView> {
    private Router router;

    public ConfigurationPresenter(Router router) {
        this.router = router;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        router.newRootScreen(new ConfigurationScreens.GamesScreen());
    }

    public void onBackClicked() {
        router.exit();
    }
}
