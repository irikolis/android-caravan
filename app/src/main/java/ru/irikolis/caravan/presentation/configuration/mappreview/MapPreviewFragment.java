package ru.irikolis.caravan.presentation.configuration.mappreview;

import android.os.Bundle;

import android.transition.TransitionInflater;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.squareup.picasso.Picasso;

import java.text.MessageFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.presentation.configuration.ConfigurationActivity;
import ru.irikolis.caravan.presentation.core.BaseFragment;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class MapPreviewFragment extends BaseFragment implements MapPreviewView {
    private static final String ARG_MAP_FILE_NAME = "map_file_name";

    @BindView(R.id.image_view_preview_map) ImageView imgMap;

    private String mapFileName;

    @InjectPresenter MapPreviewPresenter presenter;

    @ProvidePresenter
    MapPreviewPresenter providePresenter() {
        CaravanApplication.getInstance().getConfigurationComponent().inject(this);
        return new MapPreviewPresenter();
    }

    public MapPreviewFragment() {
        // Required empty public constructor
    }

    public static MapPreviewFragment newInstance(String mapFileName) {
        MapPreviewFragment fragment = new MapPreviewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_MAP_FILE_NAME, mapFileName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mapFileName = getArguments().getString(ARG_MAP_FILE_NAME);
        }

        // animation
        setSharedElementEnterTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move));
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_map_preview;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        ((ConfigurationActivity) requireActivity()).setTitle(getString(R.string.game_map));
    }

    @Override
    public void setMapImage() {
        if (getContext() != null) {
            String url = MessageFormat.format("file:///{0}/{1}", getContext().getFilesDir(), mapFileName);
            Picasso.get()
                    .load(url)
                    .placeholder(R.drawable.ic_image_placeholder_24dp)
                    .error(R.drawable.ic_image_error_24dp)
                    .into(imgMap);
        }
    }
}