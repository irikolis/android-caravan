package ru.irikolis.caravan.presentation.core.animation;

import android.animation.ValueAnimator;
import android.view.View;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ItemCollapsingAnimation {
    private View collapsedView;
    private View arrowView = null;
    private long duration = 300;
    private long startDelay = 0;

    public ItemCollapsingAnimation(View collapsedView) {
        this.collapsedView = collapsedView;
    }

    public ItemCollapsingAnimation setArrowView(View arrowView) {
        this.arrowView = arrowView;
        return this;
    }

    public ItemCollapsingAnimation setDuration(long duration) {
        this.duration = duration;
        return this;
    }

    public ItemCollapsingAnimation setStartDelay(long startDelay) {
        this.startDelay = startDelay;
        return this;
    }

    public void start() {
        if (arrowView != null)
            arrowView.animate().setDuration(duration).setStartDelay(startDelay).rotation(0F);

        ValueAnimator animator = ValueAnimator.ofInt(collapsedView.getLayoutParams().height, 0);
        animator.setDuration(duration);
        animator.setStartDelay(startDelay);
        animator.addUpdateListener(anim -> {
            collapsedView.getLayoutParams().height = (int) anim.getAnimatedValue();
            collapsedView.requestLayout();
        });
        animator.start();
    }
}
