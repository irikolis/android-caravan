package ru.irikolis.caravan.presentation.authorization.pincode;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@StateStrategyType(OneExecutionStateStrategy.class)
interface PinCodeView extends MvpView {
    void showConfigurationSuccess();
    void showPinCodeError();
    void showLoadingError();
    void navigateMain();
    void showLoading(boolean show, boolean animated);

    @StateStrategyType(SkipStrategy.class)
    void setIpAddress(String address);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setIpAddressValid(Boolean valid);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setPinCodeValid(Boolean valid);
}
