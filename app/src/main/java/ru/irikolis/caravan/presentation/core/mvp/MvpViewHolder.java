package ru.irikolis.caravan.presentation.core.mvp;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.MvpDelegate;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public abstract class MvpViewHolder extends RecyclerView.ViewHolder {

    private final MvpDelegate<?> parentDelegate;
    private MvpDelegate<? extends MvpViewHolder> mvpDelegate;

    public MvpViewHolder(MvpDelegate<?> parentDelegate, @NonNull View itemView) {
        super(itemView);
        this.parentDelegate = parentDelegate;
    }

    public MvpDelegate<? extends MvpViewHolder> getMvpDelegate() {
        if (mvpDelegate == null) {
            mvpDelegate = new MvpDelegate<>(this);
            mvpDelegate.setParentDelegate(parentDelegate, getMvpChildId());
        }
        return mvpDelegate;
    }

    public void createMvpDelegate() {
        if (getMvpDelegate() != null) {
            getMvpDelegate().onCreate();
            getMvpDelegate().onAttach();
            getMvpDelegate().onSaveInstanceState();
        }
    }

    public void destroyMvpDelegate() {
        if (mvpDelegate != null) {
            getMvpDelegate().onSaveInstanceState();
            getMvpDelegate().onDetach();
            getMvpDelegate().onDestroyView();
            mvpDelegate = null;
        }
    }

    public void restartMvpDelegate() {
        destroyMvpDelegate();
        createMvpDelegate();
    }

    protected abstract String getMvpChildId();
}