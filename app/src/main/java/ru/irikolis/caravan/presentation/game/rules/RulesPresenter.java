package ru.irikolis.caravan.presentation.game.rules;

import com.arellomobile.mvp.InjectViewState;
import ru.irikolis.caravan.presentation.core.BasePresenter;

import static ru.irikolis.caravan.application.AppConstants.RULES_FILE_NAME;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class RulesPresenter extends BasePresenter<RulesView> {
    public RulesPresenter() {
        super();
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().loadRules(RULES_FILE_NAME);
    }
}
