package ru.irikolis.caravan.presentation.configuration;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.presentation.main.MainActivity;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ConfigurationActivity extends MvpAppCompatActivity implements ConfigurationView {
    @BindView(R.id.toolbar_back) Toolbar toolbar;
    private ConfigurationNavigator navigator;

    @Inject Router router;
    @Inject NavigatorHolder navigatorHolder;
    @InjectPresenter ConfigurationPresenter presenter;

    @ProvidePresenter
    ConfigurationPresenter providePresenter() {
        return new ConfigurationPresenter(router);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        CaravanApplication.getInstance().getConfigurationComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        ButterKnife.bind(this);

        toolbar.setNavigationOnClickListener(v -> presenter.onBackClicked());
        navigator = new ConfigurationNavigator(this, R.id.frame_container);
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        presenter.onBackClicked();
    }

    @Override
    public void navigateMain() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void setTitle(String title) {
        toolbar.setTitle(title);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CaravanApplication.getInstance().clearConfigurationComponent();
    }
}