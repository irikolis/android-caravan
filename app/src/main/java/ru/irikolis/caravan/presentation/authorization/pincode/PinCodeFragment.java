package ru.irikolis.caravan.presentation.authorization.pincode;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.domain.interactors.AuthInteractor;
import ru.irikolis.caravan.domain.interactors.ConfiguratorInteractor;
import ru.irikolis.caravan.presentation.main.MainActivity;
import ru.irikolis.caravan.utils.IpAddressInputFilter;
import ru.irikolis.caravan.utils.KeyboardUtils;
import ru.irikolis.caravan.utils.OnTextChanged;
import ru.irikolis.caravan.utils.SchedulersProvider;
import ru.irikolis.widgets.LoadingButtonView;

import static androidx.core.content.ContextCompat.getDrawable;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class PinCodeFragment extends MvpAppCompatFragment implements PinCodeView {
    @BindView(R.id.edit_text_ip_address) EditText edtAddress;
    @BindView(R.id.edit_text_pin_code) EditText edtPinCode;
    @BindView(R.id.loading_button) LoadingButtonView btnSignIn;

    @InjectPresenter PinCodePresenter presenter;
    @Inject AuthInteractor authInteractor;
    @Inject ConfiguratorInteractor configuratorInteractor;
    @Inject SchedulersProvider schedulersProvider;

    @ProvidePresenter
    PinCodePresenter providePresenter() {
        CaravanApplication.getInstance().getAuthComponent().inject(this);
        return new PinCodePresenter(authInteractor, configuratorInteractor, schedulersProvider);
    }

    public PinCodeFragment() {
        // Required empty public constructor
    }

    public static PinCodeFragment newInstance() {
        return new PinCodeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pin_code, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        edtAddress.setFilters(new InputFilter[] { new IpAddressInputFilter() });
        edtAddress.addTextChangedListener(new OnTextChanged() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                presenter.onAddressChanged(edtAddress.getText().toString());
            }
        });
        edtPinCode.addTextChangedListener(new OnTextChanged() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                presenter.onPinCodeChanged(edtPinCode.getText().toString());
            }
        });
        btnSignIn.setOnClickListener(v -> {
            KeyboardUtils.hideKeyboard(v);
            presenter.onSignInClicked(edtAddress.getText().toString(), edtPinCode.getText().toString());
        });
    }

    @Override
    public void showConfigurationSuccess() {
        Toast.makeText(getActivity(), R.string.game_configuration_success, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showPinCodeError() {
        Toast.makeText(getActivity(), R.string.invalid_pin_code, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLoadingError() {
        Toast.makeText(getActivity(), R.string.error_loading_network_data, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateMain() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showLoading(boolean loading, boolean animated) {
        if (animated)
            btnSignIn.setAnimatedLoading(loading);
        else
            btnSignIn.setLoading(loading);
    }

    @Override
    public void setIpAddress(String address) {
        edtAddress.setText(address);
    }

    @Override
    public void setIpAddressValid(Boolean valid) {
        if (getContext() != null) {
            Drawable drawable = (valid) ? null : getDrawable(getContext(), R.drawable.ic_data_error_24dp);
            edtAddress.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
        }
    }

    @Override
    public void setPinCodeValid(Boolean valid) {
        if (getContext() != null) {
            Drawable drawable = (valid) ? null : getDrawable(getContext(), R.drawable.ic_data_error_24dp);
            edtPinCode.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
        }
    }
}
