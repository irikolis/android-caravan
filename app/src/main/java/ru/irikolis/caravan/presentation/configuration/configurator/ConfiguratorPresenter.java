package ru.irikolis.caravan.presentation.configuration.configurator;

import com.arellomobile.mvp.InjectViewState;

import ru.irikolis.caravan.R;
import ru.irikolis.caravan.domain.interactors.ConfiguratorInteractor;
import ru.irikolis.caravan.domain.models.GlobalGameDataType;
import ru.irikolis.caravan.presentation.core.BasePresenter;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.disposables.Disposable;
import ru.irikolis.caravan.presentation.configuration.ConfigurationScreens;
import ru.irikolis.caravan.utils.SchedulersProvider;
import ru.terrakok.cicerone.Router;

import static ru.irikolis.caravan.application.AppConstants.MAP_FILE_NAME;
import static ru.irikolis.caravan.domain.models.GlobalGameDataType.DATA_ARTIFACT;
import static ru.irikolis.caravan.domain.models.GlobalGameDataType.DATA_GEO_POINT;
import static ru.irikolis.caravan.domain.models.GlobalGameDataType.DATA_MAP;
import static ru.irikolis.caravan.domain.models.GlobalGameDataType.DATA_RULES;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class ConfiguratorPresenter extends BasePresenter<ConfiguratorView> {
    private Router router;
    private ConfiguratorInteractor interactor;
    private SchedulersProvider schedulersProvider;
    private int routeId;
    private Map<GlobalGameDataType, Boolean> hasData = new HashMap<>();

    public ConfiguratorPresenter(Router router, ConfiguratorInteractor interactor, SchedulersProvider schedulersProvider, int routeId) {
        super();
        this.router = router;
        this.interactor = interactor;
        this.schedulersProvider = schedulersProvider;
        this.routeId = routeId;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        loadRouteInformation(routeId);
        loadLastModifiedTimes();
    }

    public void loadRouteInformation(int routeId) {
        Disposable disposable = interactor.getRoute(routeId)
                .observeOn(schedulersProvider.ui())
                .subscribe(
                        route -> getViewState().setDescription(route.getDescription())
                );
        unsubscribeOnDestroy(disposable);
    }

    private void loadLastModifiedTimes() {
        for (GlobalGameDataType type: GlobalGameDataType.values())
            loadLastModifiedTime(type);
    }

    private void setInformation(GlobalGameDataType type, long time) {
        switch (type) {
            case DATA_MAP:
                getViewState().setMapImage(MAP_FILE_NAME);
                getViewState().setMapInformation(time);
                break;
            case DATA_ARTIFACT:
                getViewState().setArtifactInformation(time);
                break;
            case DATA_GEO_POINT:
                getViewState().setGeoPointInformation(time);
                break;
            case DATA_RULES:
                getViewState().setRulesInformation(time);
                break;
        }
    }

    private void loadLastModifiedTime(GlobalGameDataType type) {
        Disposable disposable = interactor.getLastModifiedTime(type)
                .observeOn(schedulersProvider.ui())
                .subscribe(
                        time -> {
                            setInformation(type, time);
                            hasData.put(type, time != 0);
                        });
        unsubscribeOnDestroy(disposable);
    }

    public void onMapUpdateClicked() {
        Disposable disposable = interactor.loadGameMap()
            .flatMap(bytes -> interactor.getLastModifiedTime(DATA_MAP))
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe(s -> getViewState().showMapLoading(true))
            .doAfterTerminate(() -> getViewState().showMapLoading(false))
            .subscribe(
                time -> {
                    getViewState().setMapInformation(time);
                    getViewState().setMapImage(MAP_FILE_NAME);
                    hasData.put(GlobalGameDataType.DATA_MAP, time != 0);
                },

                ex -> {
                    hasData.put(GlobalGameDataType.DATA_MAP, false);
                    getViewState().showError(R.string.map_update_error);
                }
            );
        unsubscribeOnDestroy(disposable);
    }

    public void onArtifactUpdateClicked() {
        Disposable disposable = interactor.loadArtifacts()
                .flatMap(loaded -> interactor.getLastModifiedTime(DATA_ARTIFACT))
                .observeOn(schedulersProvider.ui())
                .doOnSubscribe(s -> getViewState().showArtifactLoading(true))
                .doAfterTerminate(() -> getViewState().showArtifactLoading(false))
                .subscribe(
                    time -> {
                        getViewState().setArtifactInformation(time);
                        hasData.put(GlobalGameDataType.DATA_ARTIFACT, time != 0);
                    },

                    ex -> {
                        hasData.put(GlobalGameDataType.DATA_ARTIFACT, false);
                        getViewState().showError(R.string.artifacts_update_error);
                    }
                );
        unsubscribeOnDestroy(disposable);
    }

    public void onGeoPointUpdateClicked() {
        Disposable disposable = interactor.loadGeoPoints()
                .flatMap(rows -> interactor.getLastModifiedTime(DATA_GEO_POINT))
                .observeOn(schedulersProvider.ui())
                .doOnSubscribe(s -> getViewState().showGeoPointLoading(true))
                .doAfterTerminate(() -> getViewState().showGeoPointLoading(false))
                .subscribe(
                    time -> {
                        getViewState().setGeoPointInformation(time);
                        hasData.put(GlobalGameDataType.DATA_GEO_POINT, time != 0);
                    },

                    ex -> {
                        hasData.put(GlobalGameDataType.DATA_GEO_POINT, false);
                        getViewState().showError(R.string.geopoints_update_error);
                    }
                );
        unsubscribeOnDestroy(disposable);
    }

    public void onRulesUpdateClicked() {
        Disposable disposable = interactor.loadGameRules()
                .flatMap(bytes -> interactor.getLastModifiedTime(DATA_RULES))
                .observeOn(schedulersProvider.ui())
                .doOnSubscribe(s -> getViewState().showRulesLoading(true))
                .doAfterTerminate(() -> getViewState().showRulesLoading(false))
                .subscribe(
                        time -> {
                            getViewState().setRulesInformation(time);
                            hasData.put(GlobalGameDataType.DATA_RULES, time != 0);
                        },

                        ex -> {
                            hasData.put(GlobalGameDataType.DATA_RULES, false);
                            getViewState().showError(R.string.rules_update_error);
                        }
                );
        unsubscribeOnDestroy(disposable);
    }

    public void onConfigureClicked(String gamerName) {
        if (gamerName.isEmpty()) {
            getViewState().setGamerNameValid(false);
            return;
        }

        if (hasData.containsValue(false)) {
            getViewState().showError(R.string.upload_data);
            return;
        }

        Disposable disposable = interactor.performManualInitialization(gamerName, routeId)
                .observeOn(schedulersProvider.ui())
                .doOnSubscribe(s -> getViewState().showLoading(true, true))
                .doOnEvent((v, throwable) -> getViewState().showLoading(false, throwable != null))
                .subscribe(
                    success -> {
                        getViewState().showConfigurationSuccess();
                        getViewState().navigateMain();
                    },

                    ex -> getViewState().showError(R.string.game_configuration_error)
                );
        unsubscribeOnDestroy(disposable);
    }

    public void onGamerNameChanged(@NotNull String gamerName) {
        getViewState().setGamerNameValid(!gamerName.isEmpty());
    }

    public void onMapClicked() {
        router.navigateTo(new ConfigurationScreens.MapPreviewScreen(MAP_FILE_NAME));
    }
}
