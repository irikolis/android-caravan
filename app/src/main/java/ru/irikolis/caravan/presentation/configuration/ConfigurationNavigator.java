package ru.irikolis.caravan.presentation.configuration;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

import ru.irikolis.caravan.presentation.core.CustomFragmentTransition;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ConfigurationNavigator extends SupportAppNavigator {

    public ConfigurationNavigator(@NotNull FragmentActivity activity, int containerId) {
        super(activity, containerId);
    }

    @Override
    protected void setupFragmentTransaction(
            @NotNull Command command,
            @Nullable Fragment currentFragment,
            @Nullable Fragment nextFragment,
            @NotNull FragmentTransaction fragmentTransaction
    ) {
        Class<?>[] interfaces = null;
        if (currentFragment != null && nextFragment != null) {
            interfaces = currentFragment.getClass().getInterfaces();
        }

        if (interfaces != null && Arrays.asList(interfaces).contains(CustomFragmentTransition.class)) {
            ((CustomFragmentTransition) currentFragment).addTransaction(currentFragment, nextFragment, fragmentTransaction);
        }

        super.setupFragmentTransaction(command, currentFragment, nextFragment, fragmentTransaction);
    }
}
