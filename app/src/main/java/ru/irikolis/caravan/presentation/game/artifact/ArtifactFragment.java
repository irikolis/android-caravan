package ru.irikolis.caravan.presentation.game.artifact;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.domain.interactors.ArtifactInteractor;
import ru.irikolis.caravan.domain.models.Artifact;
import ru.irikolis.caravan.presentation.core.BaseFragment;
import ru.irikolis.caravan.presentation.game.GameActivity;
import ru.irikolis.caravan.utils.SchedulersProvider;
import ru.irikolis.widgets.LoadingButtonView;

import java.util.List;

import javax.inject.Inject;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ArtifactFragment extends BaseFragment implements ArtifactView {
    @BindView(R.id.layout_root) ConstraintLayout rootLayout;
    @BindView(R.id.recycler_view_artifacts) RecyclerView recyclerArtifact;
    @BindView(R.id.text_view_empty_artifacts) TextView txtEmpty;
    @BindView(R.id.loading_button_artifact_search) LoadingButtonView btnArtifactSearch;

    private ArtifactAdapter adapter;

    @Inject ArtifactInteractor interactor;
    @Inject SchedulersProvider schedulersProvider;
    @InjectPresenter ArtifactPresenter presenter;

    @ProvidePresenter
    ArtifactPresenter providePresenter() {
        CaravanApplication.getInstance().getGameComponent().inject(this);
        return new ArtifactPresenter(interactor, schedulersProvider);
    }

    public ArtifactFragment() {
        // Required empty public constructor
    }

    public static ArtifactFragment newInstance() {
        return new ArtifactFragment();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_artifact;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        ((GameActivity) requireActivity()).setTitle(getString(R.string.artifacts));
        adapter = new ArtifactAdapter();
        recyclerArtifact.setAdapter(adapter);
        btnArtifactSearch.setOnClickListener(v -> presenter.onSearchClicked());
    }

    @Override
    public void setEmptyText() {
        recyclerArtifact.setVisibility(View.GONE);
        txtEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void setArtifacts(List<Artifact> artifacts) {
        txtEmpty.setVisibility(View.GONE);
        recyclerArtifact.setVisibility(View.VISIBLE);
        adapter.setArtifacts(artifacts);
        recyclerArtifact.scrollToPosition(adapter.getItemCount()-1);
    }

    @Override
    public void showLoading(boolean loading) {
        btnArtifactSearch.setAnimatedLoading(loading);
    }

    @Override
    public void addArtifact(Artifact artifact) {
        txtEmpty.setVisibility(View.GONE);
        recyclerArtifact.setVisibility(View.VISIBLE);
        adapter.addArtifact(artifact);
        recyclerArtifact.scrollToPosition(adapter.getItemCount()-1);
    }

    @Override
    public void showNewArtifactNotFound(boolean show) {
        Toast.makeText(getContext(), R.string.new_artifact_not_found, Toast.LENGTH_SHORT).show();
    }
}
