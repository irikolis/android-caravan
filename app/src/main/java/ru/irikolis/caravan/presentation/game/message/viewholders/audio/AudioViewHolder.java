package ru.irikolis.caravan.presentation.game.message.viewholders.audio;

import android.text.format.DateFormat;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;

import com.arellomobile.mvp.MvpDelegate;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.domain.interactors.MessageInteractor;
import ru.irikolis.caravan.domain.models.AudioMessage;
import ru.irikolis.caravan.domain.models.BaseMessage;
import ru.irikolis.caravan.presentation.core.BaseViewHolder;
import ru.irikolis.caravan.presentation.core.mediaplayer.ExpandedMediaPlayer;
import ru.irikolis.caravan.utils.ExpandedMediaPlayerUtils;
import ru.irikolis.caravan.utils.SchedulersProvider;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class AudioViewHolder extends BaseViewHolder implements AudioViewHolderView {
    @BindView(R.id.text_view_sent_time) TextView txtSentTime;
    @BindView(R.id.text_view_message) TextView txtMessage;
    @BindView(R.id.toggle_button_playback) ToggleButton togglePlayback;
    @BindView(R.id.seek_bar_audio_track) SeekBar seekbarTrack;
    @BindView(R.id.text_view_audio_time) TextView txtAudioTime;
    @BindView(R.id.text_view_posting_time) TextView txtPostingTime;

    @Inject MessageInteractor interactor;
    @Inject SchedulersProvider schedulersProvider;
    @Inject ExpandedMediaPlayer mediaPlayer;
    private View itemView;

    @InjectPresenter AudioViewHolderPresenter presenter;

    @ProvidePresenter
    AudioViewHolderPresenter provideAudioMessagePresenter() {
        CaravanApplication.getInstance().getGameComponent().inject(this);
        return new AudioViewHolderPresenter(interactor, schedulersProvider);
    }

    public AudioViewHolder(MvpDelegate<?> parentDelegate, @NonNull View itemView) {
        super(parentDelegate, itemView);
        ButterKnife.bind(this, itemView);
        this.itemView = itemView;
    }

    @Override
    protected String getMvpChildId() {
        return String.valueOf(itemView);
    }

    public static BaseViewHolder createView(MvpDelegate<?> parentDelegate, @NonNull View itemView) {
        return new AudioViewHolder(parentDelegate, itemView);
    }

    @Override
    public void bindView(BaseMessage message) {
        AudioMessage msg = (AudioMessage) message;
        restartMvpDelegate();
        presenter.onBind(msg);

        OnSeekBarChangeListener onSeekBarChangeListener = new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                presenter.onTrackbarChanged(progress, seekBar.getMax(), fromUser);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        };
        seekbarTrack.setOnSeekBarChangeListener(onSeekBarChangeListener);
        togglePlayback.setOnCheckedChangeListener((v, isChecked) -> presenter.onPlaybackChecked(isChecked));

        txtSentTime.setText(DateFormat.format("Отправленно: HH:mm", msg.getSentTime()));
        txtMessage.setText(msg.getText());
        txtPostingTime.setText(DateFormat.format("HH:mm", msg.getPostingTime()));

        if (msg.getFinalTime() == null) {
            seekbarTrack.setEnabled(false);
            txtAudioTime.setText(String.format("%sКб", msg.getAudioSize()));
        }
        else {
            seekbarTrack.setEnabled(true);
            seekbarTrack.setMax(msg.getFinalTime());
            seekbarTrack.setProgress(msg.getStartTime());
            setTimedText(msg.getStartTime(), msg.getFinalTime());
        }

        boolean isPlaying = mediaPlayer.isPlayingTrack(msg.getSentId());
        if (isPlaying)
            msg.setStartTime(mediaPlayer.getCurrentPosition());
        setPlaybackToggle(isPlaying);
    }

    @Override
    public void setPlaybackToggle(boolean playing) {
        if (playing && togglePlayback.isChecked())
            presenter.onPlaybackChecked(true);
        else
            togglePlayback.setChecked(playing);
    }

    @Override
    public void playTrack(int trackId, String audioUrl, int startTime) {
        if (mediaPlayer.getCurrentTrackId() != trackId)
            mediaPlayer.stopCurrentTrack();

        mediaPlayer.setCurrentTrackId(trackId);
        mediaPlayer.setOnPreparedListener(l -> {
            mediaPlayer.seekTo(startTime);

            int finishTime = mediaPlayer.getDuration();
            setTimedText(startTime, finishTime);

            seekbarTrack.setEnabled(true);
            seekbarTrack.setMax(finishTime);
            seekbarTrack.setProgress(startTime);

            mediaPlayer.startTrack(trackId);
        });

        ExpandedMediaPlayerUtils.playMedia(itemView.getContext(), mediaPlayer, audioUrl);
    }

    @Override
    public void stopTrack(int trackId) {
        mediaPlayer.stopTrack(trackId);
    }

    @Override
    public void setTimedText(long startTime, long finalTime) {
        String text =
                DateFormat.format("mm:ss", startTime) + "/" +
                DateFormat.format("mm:ss", finalTime);
        txtAudioTime.setText(text);
    }

    @Override
    public void updateTrackbar() {
        seekbarTrack.setProgress(mediaPlayer.getCurrentPosition());
    }

    @Override
    public void setMediaPlayerSlider(int position) {
        mediaPlayer.seekTo(position);
    }

    @Override
    public void createMvpDelegate() {
        super.createMvpDelegate();
        if (mediaPlayer != null)
            mediaPlayer.addOnChangedTrackListener(onChangedTrackListener);
    }

    @Override
    public void destroyMvpDelegate() {
        if (mediaPlayer != null)
            mediaPlayer.removeOnChangedTrackListener(onChangedTrackListener);
        super.destroyMvpDelegate();
    }

    ExpandedMediaPlayer.OnChangedTrackListener onChangedTrackListener = new ExpandedMediaPlayer.OnChangedTrackListener() {
        @Override
        public void onChangedTrack(int currentTrackId) {
            presenter.onChangedCurrentTrack(currentTrackId);
        }
    };
}
