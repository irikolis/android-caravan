package ru.irikolis.caravan.presentation.main;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.domain.interactors.MainInteractor;
import ru.irikolis.caravan.presentation.authorization.AuthActivity;
import ru.irikolis.caravan.presentation.game.GameActivity;

import javax.inject.Inject;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class MainActivity extends MvpAppCompatActivity implements MainView {
    @BindView(R.id.button_game) Button btnGame;
    @BindView(R.id.button_settings) Button btnSettings;

    @Inject MainInteractor interactor;
    @InjectPresenter MainPresenter presenter;

    @ProvidePresenter
    MainPresenter providePresenter() {
        CaravanApplication.getInstance().getMainComponent().inject(this);
        return new MainPresenter(interactor);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        btnGame.setOnClickListener(v -> presenter.onGameClicked());
        btnSettings.setOnClickListener(v -> presenter.onMasterSettingsClicked());
    }

    @Override
    public void navigateGame() {
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

    @Override
    public void navigateAuth() {
        Intent intent = new Intent(this, AuthActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    @Override
    public void setGameEnabled(boolean enabled) {
        btnGame.setEnabled(enabled);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CaravanApplication.getInstance().clearMainComponent();
    }
}
