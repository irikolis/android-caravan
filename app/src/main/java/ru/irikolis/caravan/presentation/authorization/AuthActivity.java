package ru.irikolis.caravan.presentation.authorization;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.presentation.authorization.login.LoginFragment;
import ru.irikolis.caravan.presentation.authorization.pincode.PinCodeFragment;
import ru.irikolis.caravan.presentation.main.MainActivity;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class AuthActivity extends MvpAppCompatActivity implements AuthView {
    @BindView(R.id.toolbar_back) Toolbar toolbar;
    @BindView(R.id.tab_layout_auth) TabLayout tabLayout;
    @BindView(R.id.view_pager_auth) ViewPager viewPager;

    @InjectPresenter AuthPresenter presenter;

    @ProvidePresenter
    AuthPresenter providePresenter() {
        return new AuthPresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        CaravanApplication.getInstance().getAuthComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.autorization);

        setupViewPager();
        tabLayout.setupWithViewPager(viewPager);
        toolbar.setNavigationOnClickListener(v -> navigateMain());
    }

    private void setupViewPager() {
        AuthViewPagerAdapter adapter = new AuthViewPagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        adapter.addFragment(LoginFragment.newInstance(), getString(R.string.login));
        adapter.addFragment(PinCodeFragment.newInstance(), getString(R.string.pin_code));
        viewPager.setAdapter(adapter);
    }

    @Override
    public void navigateMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CaravanApplication.getInstance().clearAuthComponent();
    }
}
