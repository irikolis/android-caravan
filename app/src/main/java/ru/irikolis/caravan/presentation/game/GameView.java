package ru.irikolis.caravan.presentation.game;

import androidx.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@StateStrategyType(OneExecutionStateStrategy.class)
public interface GameView extends MvpView {
    void navigateMain();
    void finishGame();

    void showError(@StringRes int text);
    void showPermissionDialog(String[] permissions);
    void showGameFinishDialog();
    void startGameService();

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setMapItemEnabled(boolean enabled);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setTitle(String title);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setMessageRingVisible(boolean visible);
}
