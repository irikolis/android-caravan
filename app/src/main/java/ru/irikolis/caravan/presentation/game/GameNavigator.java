package ru.irikolis.caravan.presentation.game;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.terrakok.cicerone.android.support.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Forward;

import static androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_CLOSE;
import static androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GameNavigator extends SupportAppNavigator {

    public GameNavigator(@NotNull FragmentActivity activity, int containerId) {
        super(activity, containerId);
    }

    @Override
    protected void setupFragmentTransaction(
            @NotNull Command command,
            @Nullable Fragment currentFragment,
            @Nullable Fragment nextFragment,
            @NotNull FragmentTransaction fragmentTransaction
    ) {
        int transition = (command instanceof Forward) ? TRANSIT_FRAGMENT_OPEN : TRANSIT_FRAGMENT_CLOSE;
        fragmentTransaction.setTransition(transition);
        super.setupFragmentTransaction(command, currentFragment, nextFragment, fragmentTransaction);
    }
}
