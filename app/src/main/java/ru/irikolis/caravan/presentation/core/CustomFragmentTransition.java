package ru.irikolis.caravan.presentation.core;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import org.jetbrains.annotations.NotNull;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public interface CustomFragmentTransition {
    void addTransaction(@NotNull Fragment currentFragment,
                        @NotNull Fragment nextFragment,
                        @NotNull FragmentTransaction fragmentTransaction);
}
