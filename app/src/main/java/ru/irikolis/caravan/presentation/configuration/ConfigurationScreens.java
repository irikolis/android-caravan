package ru.irikolis.caravan.presentation.configuration;

import androidx.fragment.app.Fragment;

import ru.irikolis.caravan.domain.models.Game;
import ru.irikolis.caravan.domain.models.Route;
import ru.irikolis.caravan.presentation.configuration.configurator.ConfiguratorFragment;
import ru.irikolis.caravan.presentation.configuration.games.GamesFragment;
import ru.irikolis.caravan.presentation.configuration.mappreview.MapPreviewFragment;
import ru.irikolis.caravan.presentation.configuration.routes.RoutesFragment;
import ru.terrakok.cicerone.android.support.SupportAppScreen;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ConfigurationScreens {
    public static final class GamesScreen extends SupportAppScreen {
        @Override
        public Fragment getFragment() {
            return GamesFragment.newInstance();
        }
    }

    public static final class RoutesScreen extends SupportAppScreen {
        private Game game;

        public RoutesScreen(Game game) {
            this.game = game;
        }

        @Override
        public Fragment getFragment() {
            return RoutesFragment.newInstance(game);
        }
    }

    public static final class ConfiguratorScreen extends SupportAppScreen {
        private Route route;

        public ConfiguratorScreen(Route route) {
            this.route = route;
        }

        @Override
        public Fragment getFragment() {
            return ConfiguratorFragment.newInstance(route);
        }
    }

    public static final class MapPreviewScreen extends SupportAppScreen {
        private String mapFileName;

        public MapPreviewScreen(String mapFileName) {
            this.mapFileName = mapFileName;
        }

        @Override
        public Fragment getFragment() {
            return MapPreviewFragment.newInstance(mapFileName);
        }
    }
}
