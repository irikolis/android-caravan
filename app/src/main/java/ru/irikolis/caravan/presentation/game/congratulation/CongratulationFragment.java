package ru.irikolis.caravan.presentation.game.congratulation;

import android.os.Bundle;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import ru.irikolis.caravan.R;
import ru.irikolis.caravan.presentation.core.BaseFragment;
import ru.irikolis.caravan.presentation.game.GameActivity;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class CongratulationFragment extends BaseFragment implements CongratulationView {

    public CongratulationFragment() {
        // Required empty public constructor
    }

    public static CongratulationFragment newInstance() {
        return new CongratulationFragment();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_congratulation;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((GameActivity) requireActivity()).setTitle(getString(R.string.main));
    }
}
