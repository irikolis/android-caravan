package ru.irikolis.caravan.presentation.game.artifact;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import ru.irikolis.caravan.domain.models.Artifact;

import java.util.List;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface ArtifactView extends MvpView {
    void setEmptyText();
    void setArtifacts(List<Artifact> artifacts);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showLoading(boolean loading);

    @StateStrategyType(AddToEndStrategy.class)
    void addArtifact(Artifact artifact);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showNewArtifactNotFound(boolean show);
}
