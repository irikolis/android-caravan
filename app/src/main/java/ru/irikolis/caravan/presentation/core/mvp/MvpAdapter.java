package ru.irikolis.caravan.presentation.core.mvp;

import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.MvpDelegate;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public abstract class MvpAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    private final MvpDelegate<?> parentDelegate;
    private MvpDelegate<? extends MvpAdapter> mvpDelegate;

    public MvpAdapter(MvpDelegate<?> parentDelegate) {
        this.parentDelegate = parentDelegate;
        getMvpDelegate().onCreate();
    }

    public MvpDelegate<? extends MvpAdapter> getMvpDelegate() {
        if (mvpDelegate == null) {
            mvpDelegate = new MvpDelegate<>(this);
            mvpDelegate.setParentDelegate(parentDelegate, getMvpChildId());
        }
        return mvpDelegate;
    }

    public void createMvpDelegate() {
        if (getMvpDelegate() != null) {
            getMvpDelegate().onCreate();
            getMvpDelegate().onAttach();
            getMvpDelegate().onSaveInstanceState();
        }
    }

    public void destroyMvpDelegate() {
        if (mvpDelegate != null) {
            getMvpDelegate().onSaveInstanceState();
            getMvpDelegate().onDetach();
            mvpDelegate = null;
        }
    }

    public void restartMvpDelegate() {
        destroyMvpDelegate();
        createMvpDelegate();
    }

    public MvpDelegate<?> getParentDelegate() {
        return parentDelegate;
    }

    protected abstract String getMvpChildId();
}
