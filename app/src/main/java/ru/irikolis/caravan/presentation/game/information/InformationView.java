package ru.irikolis.caravan.presentation.game.information;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface InformationView extends MvpView {
    void setRouteName(String name);
    void setRouteDescription(String description);
    void setGamerName(String name);
    void setCompletedTargetsInformation(int completedTargets);
    void setRemainedTargetsInformation(int remainedTargets);
    void setOptionalTargetsInformation(int optionalTargets);
}
