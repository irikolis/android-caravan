package ru.irikolis.caravan.presentation.configuration.configurator;

import androidx.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface ConfiguratorView extends MvpView {
    void setDescription(String description);

    void setMapInformation(long time);
    void showMapLoading(boolean loading);

    void setArtifactInformation(long time);
    void showArtifactLoading(boolean loading);

    void setGeoPointInformation(long time);
    void showGeoPointLoading(boolean loading);

    void setRulesInformation(long time);
    void showRulesLoading(boolean loading);

    void setMapImage(String fileName);
    void setGamerNameValid(boolean empty);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showLoading(boolean loading, boolean animated);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showError(String text);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showError(@StringRes int text);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showConfigurationSuccess();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void navigateMain();
}
