package ru.irikolis.caravan.presentation.authorization.login;

import com.arellomobile.mvp.InjectViewState;

import io.reactivex.disposables.Disposable;
import ru.irikolis.caravan.domain.interactors.AuthInteractor;
import ru.irikolis.caravan.presentation.core.BasePresenter;
import ru.irikolis.caravan.utils.AppException;
import ru.irikolis.caravan.utils.AppExceptionFactory;
import ru.irikolis.caravan.utils.SchedulersProvider;
import timber.log.Timber;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class LoginPresenter extends BasePresenter<LoginView> {
    private AuthInteractor interactor;
    private SchedulersProvider schedulersProvider;

    public LoginPresenter(AuthInteractor interactor, SchedulersProvider schedulersProvider) {
        super();
        this.interactor = interactor;
        this.schedulersProvider = schedulersProvider;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().setIpAddress(interactor.getServerAddress());
    }

    public void onSignInClicked(String address, String username, String password) {
        if (isInputDataValid(address, username, password)) {
            interactor.setServerAddress(address);
            Disposable disposable = interactor.performAuthLogin(username, password)
                    .observeOn(schedulersProvider.ui())
                    .doOnSubscribe((s) -> getViewState().showLoading(true, true))
                    .doOnEvent((v, throwable) -> getViewState().showLoading(false, throwable != null))
                    .subscribe(
                            valid -> getViewState().navigateGameSelection(),

                            ex -> {
                                Timber.d(ex);
                                if (ex instanceof AppException &&
                                        ((AppException) ex).getType().equals(AppExceptionFactory.EX_TOKEN))
                                    getViewState().showLoginError();
                                else
                                    getViewState().showLoadingError();
                            }
                    );
            unsubscribeOnDestroy(disposable);
        }
    }

    private Boolean isInputDataValid(String address, String username, String password) {
        getViewState().setIpAddressValid(!address.isEmpty());
        getViewState().setUsernameValid(!username.isEmpty());
        getViewState().setPasswordValid(!password.isEmpty());
        return !(address.isEmpty() || username.isEmpty() || password.isEmpty());
    }

    public void onAddressChanged(String address) {
        if (!address.isEmpty()) {
            getViewState().setIpAddressValid(true);
        }
    }

    public void onUsernameChanged(String username) {
        if (!username.isEmpty()) {
            getViewState().setUsernameValid(true);
        }
    }

    public void onPasswordChanged(String password) {
        if (!password.isEmpty()) {
            getViewState().setPasswordValid(true);
        }
    }
}
