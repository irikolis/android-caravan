package ru.irikolis.caravan.presentation.main;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@StateStrategyType(OneExecutionStateStrategy.class)
public interface MainView extends MvpView {
    void navigateGame();
    void navigateAuth();

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setGameEnabled(boolean enabled);
}
