package ru.irikolis.caravan.presentation.configuration.routes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import ru.irikolis.caravan.domain.models.Route;

import java.util.List;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface RoutesView extends MvpView {
    void setAllRoutes(List<Route> routes);
    void setEmptyText();
    void showRefreshing(boolean show);
}
