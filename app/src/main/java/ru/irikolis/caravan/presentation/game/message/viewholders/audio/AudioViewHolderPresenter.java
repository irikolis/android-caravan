package ru.irikolis.caravan.presentation.game.message.viewholders.audio;

import com.arellomobile.mvp.InjectViewState;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import ru.irikolis.caravan.domain.interactors.MessageInteractor;
import ru.irikolis.caravan.domain.models.AudioMessage;
import ru.irikolis.caravan.presentation.core.BasePresenter;
import ru.irikolis.caravan.utils.SchedulersProvider;
import timber.log.Timber;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class AudioViewHolderPresenter extends BasePresenter<AudioViewHolderView> {
    private static final int PROGRESS_UPDATE_INTERVAL = 100;
    public int currentTrackId;

    private AudioMessage message;

    private MessageInteractor interactor;
    private SchedulersProvider schedulersProvider;

    public AudioViewHolderPresenter(MessageInteractor interactor, SchedulersProvider schedulersProvider) {
        super();
        this.interactor = interactor;
        this.schedulersProvider = schedulersProvider;
    }

    public void onBind(AudioMessage msg) {
        message = msg;
    }

    public void onPlaybackChecked(boolean isChecked) {
        if (isChecked) {
            if (!isPlayingTrack()) {
                if (message.getStartTime() == null || message.getFinalTime() == null || message.getFinalTime() <= message.getStartTime())
                    message.setStartTime(0);
                getViewState().playTrack(message.getSentId(), message.getAudioUrl(), message.getStartTime());
            }
            startTrackUpdateTimer();
        }
        else {
            saveTimes();
            getViewState().updateTrackbar();
            getViewState().stopTrack(message.getSentId());
        }
    }

    public void onChangedCurrentTrack(int currentTrackId) {
        this.currentTrackId = currentTrackId;
    }

    public boolean isPlayingTrack() {
        return message.getSentId() == currentTrackId;
    }

    public void onTrackbarChanged(int progress, int max, boolean fromUser) {
        message.setStartTime(progress);
        message.setFinalTime(max);

        if (fromUser && isPlayingTrack())
            getViewState().setMediaPlayerSlider(progress);

        getViewState().setTimedText(progress, max);
    }

    public void startTrackUpdateTimer() {
        Disposable disposable = Observable.interval(PROGRESS_UPDATE_INTERVAL, TimeUnit.MILLISECONDS)
                .observeOn(schedulersProvider.ui())
                .takeWhile(count -> isPlayingTrack() && message.getStartTime() < message.getFinalTime())
                .doAfterTerminate(() -> getViewState().setPlaybackToggle(false))
                .subscribe(
                        count -> getViewState().updateTrackbar(),
                        Timber::d
                );
        unsubscribeOnDestroyView(disposable);
    }

    private void saveTimes() {
        AudioMessage msg = new AudioMessage(message);
        msg.setStartTime(0);
        Disposable disposable = interactor.updateMessage(msg)
                .subscribeOn(schedulersProvider.io())
                .subscribe(
                        res -> {},
                        Timber::d
                );
        unsubscribeOnDestroyView(disposable);
    }
}
