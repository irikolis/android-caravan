package ru.irikolis.caravan.presentation.configuration.routes;

import com.arellomobile.mvp.InjectViewState;

import ru.irikolis.caravan.domain.interactors.RoutesInteractor;
import ru.irikolis.caravan.domain.models.Route;
import ru.irikolis.caravan.presentation.core.BasePresenter;

import io.reactivex.disposables.Disposable;
import ru.irikolis.caravan.presentation.configuration.ConfigurationScreens;
import ru.irikolis.caravan.utils.SchedulersProvider;
import ru.terrakok.cicerone.Router;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class RoutesPresenter extends BasePresenter<RoutesView> {
    private Router router;
    private RoutesInteractor interactor;
    private SchedulersProvider schedulersProvider;
    private int gameId;

    public RoutesPresenter(Router router, RoutesInteractor interactor, SchedulersProvider schedulersProvider, int gameId) {
        super();
        this.router = router;
        this.interactor = interactor;
        this.schedulersProvider = schedulersProvider;
        this.gameId = gameId;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadRoutes();
    }

    public void loadRoutes() {
        Disposable disposable = interactor.performGameSelection(gameId)
                .flatMap(selected -> interactor.getAllRoutes())
                .observeOn(schedulersProvider.ui())
                .doOnSubscribe(s -> getViewState().showRefreshing(true))
                .doOnEvent((v, throwable) -> getViewState().showRefreshing(false))
                .subscribe(
                    routes -> getViewState().setAllRoutes(routes),
                    ex -> getViewState().setEmptyText()
                );
        unsubscribeOnDestroy(disposable);
    }

    public void onItemClicked(Route route) {
        router.navigateTo(new ConfigurationScreens.ConfiguratorScreen(route));
    }
}
