package ru.irikolis.caravan.presentation.configuration.mappreview;

import com.arellomobile.mvp.InjectViewState;

import ru.irikolis.caravan.presentation.core.BasePresenter;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class MapPreviewPresenter extends BasePresenter<MapPreviewView> {

    public MapPreviewPresenter() {
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().setMapImage();
    }
}
