package ru.irikolis.caravan.presentation.configuration;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@StateStrategyType(OneExecutionStateStrategy.class)
public interface ConfigurationView extends MvpView {
    void navigateMain();

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setTitle(String title);
}
