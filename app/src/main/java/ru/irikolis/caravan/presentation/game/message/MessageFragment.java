package ru.irikolis.caravan.presentation.game.message;


import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.domain.interactors.MessageInteractor;
import ru.irikolis.caravan.domain.models.BaseMessage;
import ru.irikolis.caravan.presentation.core.BaseFragment;
import ru.irikolis.caravan.presentation.game.GameActivity;
import ru.irikolis.caravan.presentation.game.message.adapter.MessageAdapter;
import ru.irikolis.caravan.utils.SchedulersProvider;

import javax.inject.Inject;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class MessageFragment extends BaseFragment implements MessageView {
    @BindView(R.id.recycler_view_messages) RecyclerView recyclerMessage;
    @BindView(R.id.text_view_empty_routes) TextView txtEmpty;

    @Inject MessageInteractor interactor;
    @Inject SchedulersProvider schedulersProvider;
    @InjectPresenter MessagePresenter presenter;

    private MessageAdapter adapter;
    private boolean changedConfiguration = false;


    @ProvidePresenter
    MessagePresenter providePresenter() {
        CaravanApplication.getInstance().getGameComponent().inject(this);
        return new MessagePresenter(interactor, schedulersProvider);
    }

    public MessageFragment() {
        // Required empty public constructor
    }

    public static MessageFragment newInstance() {
        return new MessageFragment();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_message;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        ((GameActivity) requireActivity()).setTitle(getString(R.string.messages));
        adapter = new MessageAdapter(getMvpDelegate());
        recyclerMessage.setAdapter(adapter);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        changedConfiguration = true;
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void setEmptyText() {
        recyclerMessage.setVisibility(View.GONE);
        txtEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void setAllMessages(List<BaseMessage> messages) {
        txtEmpty.setVisibility(View.GONE);
        recyclerMessage.setVisibility(View.VISIBLE);
        adapter.setMessages(messages);
        recyclerMessage.scrollToPosition(adapter.getItemCount()-1);
    }

    @Override
    public void addMessage(BaseMessage message) {
        txtEmpty.setVisibility(View.GONE);
        recyclerMessage.setVisibility(View.VISIBLE);
        adapter.addMessage(message);
        recyclerMessage.scrollToPosition(adapter.getItemCount()-1);
    }

    @Override
    public void onDestroyView() {
        adapter.destroy(changedConfiguration);
        super.onDestroyView();
    }
}
