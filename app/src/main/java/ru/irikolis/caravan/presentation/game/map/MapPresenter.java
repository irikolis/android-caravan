package ru.irikolis.caravan.presentation.game.map;

import com.arellomobile.mvp.InjectViewState;
import ru.irikolis.caravan.domain.interactors.MapInteractor;
import ru.irikolis.caravan.domain.models.TargetState;
import ru.irikolis.caravan.domain.models.TargetType;
import ru.irikolis.caravan.presentation.core.BasePresenter;

import io.reactivex.disposables.Disposable;
import ru.irikolis.caravan.utils.SchedulersProvider;
import timber.log.Timber;

import static ru.irikolis.caravan.application.AppConstants.MAP_FILE_NAME;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class MapPresenter extends BasePresenter<MapView> {
    private final String MARKER_GAMER_LOCATION = "Gamer Location";

    private MapInteractor interactor;
    private SchedulersProvider schedulersProvider;
    private float scale = 1.0f;

    public MapPresenter(MapInteractor interactor, SchedulersProvider schedulersProvider) {
        super();
        this.interactor = interactor;
        this.schedulersProvider = schedulersProvider;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        // Load map image from the file
        getViewState().setMapImage(MAP_FILE_NAME);

        // Bind the map image to coordinations
        bindMapImageToCoords();

        // Draw the last player location on the map
        loadLastLocation();
        // Draw the current player location on the map
        startLocationListener();

        if (interactor.isTargetsVisible()) {
            // Draw targets
            drawTargets();
            // Draw the completed targets
            startCompletedTargetListener();
        }
    }

    private void bindMapImageToCoords() {
        Disposable disposable = interactor.getGameMap()
                .observeOn(schedulersProvider.ui())
                .subscribe(
                        gameMap -> getViewState().initMapImage(gameMap),
                        Timber::d
                );
        unsubscribeOnDestroy(disposable);
    }

    public void loadLastLocation() {
        Disposable disposable = interactor.getLastLocation()
                .observeOn(schedulersProvider.ui())
                .subscribe(
                        location -> {
                            if (location.getData() != null) {
                                getViewState().drawLocation(MARKER_GAMER_LOCATION, location.getData(), true);
                            }
                        },
                        Timber::d
                );
        unsubscribeOnDestroy(disposable);
    }

    private void drawTargets() {
        Disposable disposable = interactor.getAllTargets()
                .observeOn(schedulersProvider.ui())
                .subscribe(
                        target -> {
                            if (target.getState() == TargetState.COMPLETED)
                                getViewState().drawCompletedTarget(target.getTitle(), target.getCoord(), true);
                            else if (target.getType() == TargetType.NECESSARY && target.getState() == TargetState.UNBLOCKED)
                                getViewState().drawTarget(target.getType(), target.getTitle(), target.getCoord(), true);
                        },
                        Timber::d
                );
        unsubscribeOnDestroy(disposable);
    }

    private void startLocationListener() {
        Disposable disposable = interactor.startLocationListener()
                .observeOn(schedulersProvider.ui())
                .subscribe(
                        location -> getViewState().drawLocation(MARKER_GAMER_LOCATION, location, true),
                        Timber::d
                );
        unsubscribeOnDestroy(disposable);
    }

    private void startCompletedTargetListener() {
        Disposable disposable = interactor.startCompletedTargetListener()
                .observeOn(schedulersProvider.ui())
                .subscribe(
                        target -> drawTargets(),
                        Timber::d
                );
        unsubscribeOnDestroy(disposable);
    }

    public void onZoomInClicked() {
        scale += 0.1f;
        getViewState().setZoom(scale);
    }

    public void onZoomOutClicked() {
        scale -= 0.1f;
        getViewState().setZoom(scale);
    }
}
