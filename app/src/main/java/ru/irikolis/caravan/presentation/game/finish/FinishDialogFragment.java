package ru.irikolis.caravan.presentation.game.finish;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import ru.irikolis.caravan.R;
import ru.irikolis.caravan.presentation.game.GameActivity;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class FinishDialogFragment extends DialogFragment {

    public FinishDialogFragment() {
    }

    public static FinishDialogFragment newInstance() {
        return new FinishDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.warring)
                .setMessage(R.string.you_want_to_finish_game)
                .setNegativeButton(R.string.no, (dialog, which) -> dialog.cancel())
                .setPositiveButton(R.string.yes, (dialog, which) -> ((GameActivity) requireActivity()).confirmFinishDialog());
        return builder.create();
    }
}
