package ru.irikolis.caravan.presentation.game.congratulation;

import com.arellomobile.mvp.MvpView;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public interface CongratulationView extends MvpView {
}
