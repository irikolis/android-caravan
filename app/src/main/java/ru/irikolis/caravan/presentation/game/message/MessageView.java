package ru.irikolis.caravan.presentation.game.message;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import ru.irikolis.caravan.domain.models.BaseMessage;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface MessageView extends MvpView {
    void setEmptyText();
    void setAllMessages(List<BaseMessage> messages);

    @StateStrategyType(AddToEndStrategy.class)
    void addMessage(BaseMessage message);
}
