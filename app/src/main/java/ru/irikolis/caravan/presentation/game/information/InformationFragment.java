package ru.irikolis.caravan.presentation.game.information;

import android.os.Bundle;

import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.domain.interactors.InformationInteractor;
import ru.irikolis.caravan.presentation.core.BaseFragment;
import ru.irikolis.caravan.presentation.game.GameActivity;
import ru.irikolis.caravan.utils.SchedulersProvider;

import javax.inject.Inject;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class InformationFragment extends BaseFragment implements InformationView {
    @BindView(R.id.text_view_route_name) TextView txtRouteName;
    @BindView(R.id.text_view_route_description) TextView txtRouteDescription;
    @BindView(R.id.text_view_gamer_name) TextView txtGamerName;
    @BindView(R.id.text_view_completed_targets) TextView txtCompletedTargets;
    @BindView(R.id.text_view_remained_targets) TextView txtRemainedTargets;
    @BindView(R.id.text_view_optional_targets) TextView txtOptionalTargets;

    @Inject InformationInteractor interactor;
    @Inject SchedulersProvider schedulersProvider;
    @InjectPresenter InformationPresenter presenter;

    @ProvidePresenter
    InformationPresenter providePresenter() {
        CaravanApplication.getInstance().getGameComponent().inject(this);
        return new InformationPresenter(interactor, schedulersProvider);
    }

    public InformationFragment() {
        // Required empty public constructor
    }

    public static InformationFragment newInstance() {
        return new InformationFragment();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_information;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        ((GameActivity) requireActivity()).setTitle(getString(R.string.main));
        txtRouteDescription.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    public void setRouteName(String name) {
        txtRouteName.setText(name);
    }

    @Override
    public void setRouteDescription(String description) {
        txtRouteDescription.setText(description);
    }

    @Override
    public void setGamerName(String name) {
        txtGamerName.setText(name);
    }

    @Override
    public void setCompletedTargetsInformation(int completedTargets) {
        String text = getString(R.string.completed) + ": " + completedTargets;
        txtCompletedTargets.setText(text);
    }

    @Override
    public void setRemainedTargetsInformation(int remainedTargets) {
        String text = getString(R.string.remained) + ": " + remainedTargets;
        txtRemainedTargets.setText(text);
    }

    @Override
    public void setOptionalTargetsInformation(int optionalTargets) {
        String text = getString(R.string.optional) + ": " + optionalTargets;
        txtOptionalTargets.setText(text);
    }
}
