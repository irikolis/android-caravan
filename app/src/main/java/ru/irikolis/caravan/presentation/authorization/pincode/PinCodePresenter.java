package ru.irikolis.caravan.presentation.authorization.pincode;

import com.arellomobile.mvp.InjectViewState;

import io.reactivex.disposables.Disposable;
import ru.irikolis.caravan.domain.interactors.AuthInteractor;
import ru.irikolis.caravan.domain.interactors.ConfiguratorInteractor;
import ru.irikolis.caravan.presentation.core.BasePresenter;
import ru.irikolis.caravan.utils.AppException;
import ru.irikolis.caravan.utils.AppExceptionFactory;
import ru.irikolis.caravan.utils.SchedulersProvider;
import timber.log.Timber;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@InjectViewState
public class PinCodePresenter extends BasePresenter<PinCodeView> {
    private AuthInteractor authInteractor;
    private ConfiguratorInteractor configuratorInteractor;
    private SchedulersProvider schedulersProvider;

    public PinCodePresenter(AuthInteractor authInteractor, ConfiguratorInteractor configuratorInteractor, SchedulersProvider schedulersProvider) {
        super();
        this.authInteractor = authInteractor;
        this.configuratorInteractor = configuratorInteractor;
        this.schedulersProvider = schedulersProvider;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().setIpAddress(authInteractor.getServerAddress());
    }

    public void onSignInClicked(String address, String pinCode) {
        if (isInputDataValid(address, pinCode)) {
            authInteractor.setServerAddress(address);
            Disposable disposable = authInteractor.performAuthPinCode(pinCode)
                    .flatMap(valid -> configuratorInteractor.performAutoInitialization())
                    .observeOn(schedulersProvider.ui())
                    .doOnSubscribe((s) -> getViewState().showLoading(true, true))
                    .doOnEvent((v, throwable) -> getViewState().showLoading(false, throwable != null))
                    .subscribe(
                            valid -> {
                                getViewState().showConfigurationSuccess();
                                getViewState().navigateMain();
                            },

                            ex -> {
                                Timber.d(ex);
                                if (ex instanceof AppException &&
                                        ((AppException) ex).getType().equals(AppExceptionFactory.EX_TOKEN))
                                    getViewState().showPinCodeError();
                                else
                                    getViewState().showLoadingError();
                            }
                    );
            unsubscribeOnDestroy(disposable);
        }
    }

    private Boolean isInputDataValid(String address, String pinCode) {
        getViewState().setIpAddressValid(!address.isEmpty());
        getViewState().setPinCodeValid(!pinCode.isEmpty());
        return !(address.isEmpty() || pinCode.isEmpty());
    }

    public void onAddressChanged(String address) {
        if (!address.isEmpty()) {
            getViewState().setIpAddressValid(true);
        }
    }

    public void onPinCodeChanged(String pinCode) {
        if (!pinCode.isEmpty()) {
            getViewState().setPinCodeValid(true);
        }
    }
}
