package ru.irikolis.caravan.presentation.configuration.games;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import ru.irikolis.caravan.domain.models.Game;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface GamesView extends MvpView {
    void setAllGames(List<Game> games);
    void setEmptyText();
    void showRefreshing(boolean show);
}
