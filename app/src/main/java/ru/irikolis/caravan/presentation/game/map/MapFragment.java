package ru.irikolis.caravan.presentation.game.map;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.domain.interactors.MapInteractor;
import ru.irikolis.caravan.domain.models.Coord;
import ru.irikolis.caravan.domain.models.GameMap;
import ru.irikolis.caravan.domain.models.TargetType;
import ru.irikolis.caravan.presentation.core.BaseFragment;
import ru.irikolis.caravan.presentation.game.GameActivity;
import ru.irikolis.caravan.utils.BitmapUtils;
import ru.irikolis.caravan.utils.SchedulersProvider;
import ru.irikolis.mapview.GeoCoord;
import ru.irikolis.mapview.MapImageView;
import ru.irikolis.mapview.Marker;

import com.squareup.picasso.Picasso;

import java.text.MessageFormat;

import javax.inject.Inject;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class MapFragment extends BaseFragment implements MapView {
    @BindView(R.id.image_view_map) MapImageView imgMap;
    @BindView(R.id.image_view_zoom_in) ImageView imgZoomIn;
    @BindView(R.id.image_view_zoom_out) ImageView imgZoomOut;

    @Inject MapInteractor interactor;
    @Inject SchedulersProvider schedulersProvider;
    @InjectPresenter MapPresenter presenter;

    @ProvidePresenter
    MapPresenter providePresenter() {
        CaravanApplication.getInstance().getGameComponent().inject(this);
        return new MapPresenter(interactor, schedulersProvider);
    }

    public MapFragment() {
        // Required empty public constructor
    }

    public static MapFragment newInstance() {
        return new MapFragment();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_map;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        ((GameActivity) requireActivity()).setTitle(getString(R.string.map));
        imgZoomIn.setOnClickListener(v -> presenter.onZoomInClicked());
        imgZoomOut.setOnClickListener(v -> presenter.onZoomOutClicked());
    }

    @Override
    public void setZoom(float zoom) {
        imgMap.setZoom(zoom);
    }

    @Override
    public void setMapImage(String fileName) {
        String url = MessageFormat.format("file:///{0}/{1}", requireActivity().getFilesDir(), fileName);
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.ic_image_placeholder_24dp)
                .error(R.drawable.ic_image_error_24dp)
                .into(imgMap);
    }

    @Override
    public void initMapImage(GameMap gameMap) {
        imgMap.bindGeoCoords(gameMap.getNorthWestCoord(), gameMap.getSouthEastCoord());
    }

    private void drawMarker(String title, Coord coord, boolean visible, int resId) {
        Marker marker = imgMap.getMarker(title);
        if (marker == null) {
            marker = new Marker().title(title);
        }

        Bitmap bitmap = BitmapUtils.getBitmapFromVectorDrawable(getContext(), resId);
        GeoCoord geoCoord = new GeoCoord(coord.getLatitude(), coord.getLongitude());
        marker.icon(bitmap).position(geoCoord).visible(visible);
        imgMap.addMarker(marker);
    }

    @Override
    public void drawLocation(String title, Coord coord, boolean visible) {
        drawMarker(title, coord, visible, R.drawable.ic_gamer_location_24dp);
    }

    @Override
    public void drawTarget(TargetType type, String title, Coord coord, boolean visible) {
        if (type == TargetType.NECESSARY)
            drawMarker(title, coord, visible, R.drawable.ic_target_necessary_24dp);
        else if (type == TargetType.OPTIONAL)
            drawMarker(title, coord, visible, R.drawable.ic_target_optional_24dp);
        // for debug
//        else
//            drawMarker(title, coord, visible, R.drawable.ic_target_unknown_24dp);
    }

    @Override
    public void drawCompletedTarget(String title, Coord coord, boolean visible) {
        drawMarker(title, coord, visible, R.drawable.ic_target_completed_24dp);
    }
}
