package ru.irikolis.caravan.presentation.game.artifact;

import java.util.Objects;

import ru.irikolis.caravan.domain.models.Artifact;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class ArtifactItem {
    private Artifact artifact;
    private boolean expanded;

    public ArtifactItem(Artifact artifact, boolean expanded) {
        this.artifact = artifact;
        this.expanded = expanded;
    }

    public ArtifactItem(ArtifactItem item) {
        this.artifact = new Artifact(item.artifact);
        this.expanded = item.expanded;
    }

    public Artifact getArtifact() {
        return artifact;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        ArtifactItem artifactItem = (ArtifactItem) obj;
        return artifact.equals(artifactItem.artifact) &&
               expanded == artifactItem.expanded;
    }

    @Override
    public int hashCode() {
        return Objects.hash(artifact, expanded);
    }
}
