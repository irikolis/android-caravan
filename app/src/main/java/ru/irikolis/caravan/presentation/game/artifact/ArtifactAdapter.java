package ru.irikolis.caravan.presentation.game.artifact;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.irikolis.caravan.R;
import ru.irikolis.caravan.domain.models.Artifact;
import ru.irikolis.caravan.presentation.core.animation.ItemCollapsingAnimation;
import ru.irikolis.caravan.presentation.core.animation.ItemExpandingAnimation;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ArtifactAdapter extends RecyclerView.Adapter<ArtifactAdapter.ArtifactViewHolder> {
    private static final long ANIMATION_DURATION = 1000L;

    private static final String ITEM_EXPANDING_PAYLOAD = "expanding_payload";
    private static final String ITEM_COLLAPSING_PAYLOAD = "collapsing_payload";

    private final DiffUtil.ItemCallback<ArtifactItem> diffCallback = new DiffUtil.ItemCallback<ArtifactItem>() {
        @Override
        public boolean areItemsTheSame(@NonNull ArtifactItem oldItem, @NonNull ArtifactItem newItem) {
            return oldItem.getArtifact().getMac() == newItem.getArtifact().getMac();
        }

        @Override
        public boolean areContentsTheSame(@NonNull ArtifactItem oldItem, @NonNull ArtifactItem newItem) {
            return oldItem.equals(newItem);
        }

        @Nullable
        @Override
        public Object getChangePayload(@NonNull ArtifactItem oldItem, @NonNull ArtifactItem newItem) {
            // should expand item
            if (!oldItem.isExpanded()  && newItem.isExpanded())
                return ITEM_EXPANDING_PAYLOAD;

            // should collapse item
            if (oldItem.isExpanded() && !newItem.isExpanded())
                return ITEM_COLLAPSING_PAYLOAD;

            return null;
        }
    };
    private AsyncListDiffer<ArtifactItem> differ = new AsyncListDiffer<>(this, diffCallback);
    private int lastExtendedPosition = RecyclerView.NO_POSITION;
    private List<ArtifactItem> artifactItems = new ArrayList<>();

    public ArtifactAdapter() {
    }

    public void setArtifacts(List<Artifact> artifacts) {
        List<ArtifactItem> newItems = new ArrayList<>();
        for (Artifact artifact: artifacts) {
            boolean expanded = false;
            for (ArtifactItem item: artifactItems) {
                if (artifact.getMac() == item.getArtifact().getMac()) {
                    expanded = item.isExpanded();
                    break;
                }
            }
            newItems.add(new ArtifactItem(artifact, expanded));
        }
        differ.submitList(newItems);
    }

    public void addArtifact(Artifact artifact) {
        this.artifactItems.add(new ArtifactItem(artifact, false));
        differ.submitList(artifactItems);
    }

    private void setArtifactItems(List<ArtifactItem> artifactItems) {
        this.artifactItems = artifactItems;
        differ.submitList(artifactItems);
    }

    @NonNull
    @Override
    public ArtifactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_artifact, parent, false);
        ArtifactViewHolder holder = new ArtifactViewHolder(view);
        view.setOnClickListener(v -> {
            int position = holder.getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                List<ArtifactItem> newItems = new ArrayList<>();
                for (ArtifactItem item: artifactItems)
                    newItems.add(new ArtifactItem(item));
                if (lastExtendedPosition != RecyclerView.NO_POSITION && lastExtendedPosition != position)
                    newItems.get(lastExtendedPosition).setExpanded(false);
                newItems.get(position).setExpanded(lastExtendedPosition != position);
                setArtifactItems(newItems);
                lastExtendedPosition = (lastExtendedPosition != position) ?position :RecyclerView.NO_POSITION;
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ArtifactViewHolder holder, int position) {
        Artifact artifact = getItem(position).getArtifact();
        String text = "\"" + artifact.getName() + "\"";
        holder.txtArtifactName.setText(text);
        holder.txtArtifactDescription.setText(artifact.getDescription());
        if (getItem(position).isExpanded()) {
            holder.imgArrow.setRotation(180f);
            holder.txtArtifactDescription.setVisibility(View.VISIBLE);
        }
        else {
            holder.imgArrow.setRotation(0f);
            holder.txtArtifactDescription.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ArtifactViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
            return;
        }

        String payload = (String) payloads.get(payloads.size()-1);
        switch (payload) {
            case ITEM_EXPANDING_PAYLOAD:
                new ItemExpandingAnimation(holder.txtArtifactDescription)
                        .setArrowView(holder.imgArrow)
                        .setDuration(ANIMATION_DURATION)
                        .setStartDelay((lastExtendedPosition != RecyclerView.NO_POSITION) ?ANIMATION_DURATION :0)
                        .start();
                break;
            case ITEM_COLLAPSING_PAYLOAD:
                new ItemCollapsingAnimation(holder.txtArtifactDescription)
                        .setArrowView(holder.imgArrow)
                        .setDuration(ANIMATION_DURATION)
                        .setStartDelay(0)
                        .start();
                break;
        }
    }

    @Override
    public int getItemCount() {
        return differ.getCurrentList().size();
    }

    public ArtifactItem getItem(int position) {
        return differ.getCurrentList().get(position);
    }

    static class ArtifactViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_view_artifact_name) TextView txtArtifactName;
        @BindView(R.id.image_view_arrow) ImageView imgArrow;
        @BindView(R.id.text_view_artifact_description) TextView txtArtifactDescription;

        ArtifactViewHolder(@NonNull View item) {
            super(item);
            ButterKnife.bind(this, item);
        }
    }
}
