package ru.irikolis.caravan.di.component;

import ru.irikolis.caravan.di.ActivityScope;
import ru.irikolis.caravan.di.module.ArtifactModule;
import ru.irikolis.caravan.di.module.ConfigurationModule;
import ru.irikolis.caravan.di.module.GamesModule;
import ru.irikolis.caravan.di.module.MapModule;
import ru.irikolis.caravan.di.module.MessageModule;
import ru.irikolis.caravan.di.module.NavigationModule;
import ru.irikolis.caravan.di.module.RoutesModule;
import ru.irikolis.caravan.di.module.RulesModule;
import ru.irikolis.caravan.di.module.TargetModule;
import ru.irikolis.caravan.presentation.configuration.ConfigurationActivity;

import dagger.Subcomponent;
import ru.irikolis.caravan.presentation.configuration.configurator.ConfiguratorFragment;
import ru.irikolis.caravan.presentation.configuration.games.GamesFragment;
import ru.irikolis.caravan.presentation.configuration.mappreview.MapPreviewFragment;
import ru.irikolis.caravan.presentation.configuration.routes.RoutesFragment;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Subcomponent(
        modules = {
                ConfigurationModule.class,
                NavigationModule.class,
                GamesModule.class,
                RoutesModule.class,
                TargetModule.class,
                ArtifactModule.class,
                MessageModule.class,
                MapModule.class,
                RulesModule.class
        }
)
@ActivityScope
public interface ConfigurationComponent {
    void inject(ConfigurationActivity configurationActivity);

    void inject(GamesFragment gamesFragment);
    void inject(RoutesFragment routesFragment);
    void inject(ConfiguratorFragment configuratorFragment);
    void inject(MapPreviewFragment mapPreviewFragment);
}
