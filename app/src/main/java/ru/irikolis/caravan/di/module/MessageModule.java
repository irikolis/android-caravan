package ru.irikolis.caravan.di.module;

import dagger.Module;
import dagger.Provides;
import ru.irikolis.caravan.data.database.AppDatabase;
import ru.irikolis.caravan.data.network.ApiService;
import ru.irikolis.caravan.data.repositories.MessageRepository;
import ru.irikolis.caravan.di.ActivityScope;
import ru.irikolis.caravan.presentation.game.message.MessageFragment;
import ru.irikolis.caravan.presentation.game.message.adapter.MessageAdapter;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
public class MessageModule {

    @Provides
    @ActivityScope
    MessageRepository provideMessageRepository(ApiService apiService, AppDatabase appDatabase) {
        return new MessageRepository(apiService, appDatabase);
    }
}
