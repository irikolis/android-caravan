package ru.irikolis.caravan.di.component;

import ru.irikolis.caravan.di.ActivityScope;
import ru.irikolis.caravan.di.module.MainModule;
import ru.irikolis.caravan.presentation.main.MainActivity;

import dagger.Subcomponent;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Subcomponent(
        modules = {
                MainModule.class
        }
)
@ActivityScope
public interface MainComponent {
    void inject(MainActivity mainActivity);
}
