package ru.irikolis.caravan.di.module;

import android.app.Application;
import android.media.AudioAttributes;
import android.media.SoundPool;

import androidx.room.Room;

import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import ru.irikolis.caravan.data.database.AppDatabase;
import ru.irikolis.caravan.data.database.modification.ModificationTriggerCallback;
import ru.irikolis.caravan.data.network.ApiService;
import ru.irikolis.caravan.data.network.MultipleConverterFactory;
import ru.irikolis.caravan.data.network.OkHttpProvider;
import ru.irikolis.caravan.data.preference.AppPreference;
import ru.irikolis.caravan.application.AppConstants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.irikolis.caravan.presentation.core.mediaplayer.ExpandedMediaPlayer;
import ru.irikolis.caravan.utils.SchedulersProvider;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
public class AppModule {

    @Provides
    @Singleton
    AppPreference provideAppPreference(Application application) {
        return new AppPreference(application, AppConstants.APP_PREFERENCES);
    }

    @Provides
    @Singleton
    AppDatabase provideAppDatabase(Application application) {
        return Room.databaseBuilder(application, AppDatabase.class, AppConstants.APP_DATABASE)
                .addCallback(new ModificationTriggerCallback())
                .build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(AppPreference preference) {
        return new Retrofit.Builder()
                .baseUrl(String.format("http://%s/", AppConstants.BASE_URL))
                .addConverterFactory(MultipleConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(OkHttpProvider.buildClient(preference))
                .build();
    }

    @Provides
    @Singleton
    public ApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @Singleton
    SchedulersProvider provideSchedulersProvider() {
        return new SchedulersProvider();
    }

    @Provides
    @Singleton
    SoundPool provideSoundPool(Application application) {
        AudioAttributes audioAttr = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();

        return new SoundPool.Builder()
                .setAudioAttributes(audioAttr)
                .setMaxStreams(AppConstants.MAX_SOUND_STREAMS)
                .build();
    }

    @Provides
    @Singleton
    ExpandedMediaPlayer provideMediaPlayer() {
        return new ExpandedMediaPlayer();
    }
}
