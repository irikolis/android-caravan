package ru.irikolis.caravan.di.module;

import dagger.Module;
import dagger.Provides;
import ru.irikolis.caravan.data.database.AppDatabase;
import ru.irikolis.caravan.data.network.ApiService;
import ru.irikolis.caravan.data.repositories.TargetRepository;
import ru.irikolis.caravan.di.ActivityScope;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
public class TargetModule {

    @Provides
    @ActivityScope
    TargetRepository provideTargetRepository(ApiService apiService, AppDatabase appDatabase) {
        return new TargetRepository(apiService, appDatabase);
    }
}
