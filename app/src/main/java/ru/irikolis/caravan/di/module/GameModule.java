package ru.irikolis.caravan.di.module;

import android.app.Application;

import ru.irikolis.caravan.data.database.AppDatabase;
import ru.irikolis.caravan.data.network.ApiService;
import ru.irikolis.caravan.data.repositories.ArtifactRepository;
import ru.irikolis.caravan.data.providers.BluetoothProviderRepository;
import ru.irikolis.caravan.data.repositories.TargetRepository;
import ru.irikolis.caravan.data.repositories.ServiceRepository;
import ru.irikolis.caravan.data.providers.LocationProviderRepository;
import ru.irikolis.caravan.data.repositories.MapRepository;
import ru.irikolis.caravan.data.service.RxBus;
import ru.irikolis.caravan.di.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
public class GameModule {

    @Provides
    @ActivityScope
    ServiceRepository provideServiceRepository(Application application) {
        return new ServiceRepository(application);
    }

    @Provides
    @ActivityScope
    LocationProviderRepository providerLocationRepository(Application application) {
        return new LocationProviderRepository(application);
    }

    @Provides
    @ActivityScope
    BluetoothProviderRepository provideBluetoothProviderRepository(Application application) {
        return new BluetoothProviderRepository(application);
    }
}
