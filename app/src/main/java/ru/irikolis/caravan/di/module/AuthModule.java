package ru.irikolis.caravan.di.module;

import ru.irikolis.caravan.data.network.ApiService;
import ru.irikolis.caravan.data.preference.AppPreference;
import ru.irikolis.caravan.data.repositories.AuthRepository;
import ru.irikolis.caravan.di.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
public class AuthModule {

    @Provides
    @ActivityScope
    AuthRepository provideAuthRepository(ApiService apiService, AppPreference appPreference) {
        return new AuthRepository(apiService, appPreference);
    }
}
