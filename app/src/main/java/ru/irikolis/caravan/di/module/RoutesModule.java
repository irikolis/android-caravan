package ru.irikolis.caravan.di.module;

import ru.irikolis.caravan.data.database.AppDatabase;
import ru.irikolis.caravan.data.network.ApiService;
import ru.irikolis.caravan.data.repositories.RoutesRepository;
import ru.irikolis.caravan.di.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
public class RoutesModule {

    @Provides
    @ActivityScope
    RoutesRepository provideRoutesRepository(ApiService apiService, AppDatabase appDatabase) {
        return new RoutesRepository(apiService, appDatabase);
    }
}
