package ru.irikolis.caravan.di.module;

import dagger.Module;
import ru.irikolis.caravan.data.network.ApiService;
import ru.irikolis.caravan.data.repositories.GamesRepository;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
public class GamesModule {

    GamesRepository provideGamesRepository(ApiService apiService) {
        return new GamesRepository(apiService);
    }
}
