package ru.irikolis.caravan.di.module;

import dagger.Module;
import dagger.Provides;
import ru.irikolis.caravan.data.database.AppDatabase;
import ru.irikolis.caravan.data.network.ApiService;
import ru.irikolis.caravan.data.repositories.ArtifactRepository;
import ru.irikolis.caravan.di.ActivityScope;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
public class ArtifactModule {

    @Provides
    @ActivityScope
    ArtifactRepository provideArtifactRepository(ApiService apiService, AppDatabase appDatabase) {
        return new ArtifactRepository(apiService, appDatabase);
    }
}
