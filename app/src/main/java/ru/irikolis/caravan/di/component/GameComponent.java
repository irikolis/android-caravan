package ru.irikolis.caravan.di.component;

import ru.irikolis.caravan.di.ActivityScope;
import ru.irikolis.caravan.di.module.ArtifactModule;
import ru.irikolis.caravan.di.module.GameModule;
import ru.irikolis.caravan.di.module.MapModule;
import ru.irikolis.caravan.di.module.MessageModule;
import ru.irikolis.caravan.di.module.NavigationModule;
import ru.irikolis.caravan.di.module.RulesModule;
import ru.irikolis.caravan.di.module.TargetModule;
import ru.irikolis.caravan.presentation.game.artifact.ArtifactFragment;
import ru.irikolis.caravan.presentation.game.congratulation.CongratulationFragment;
import ru.irikolis.caravan.presentation.game.information.InformationFragment;
import ru.irikolis.caravan.presentation.game.map.MapFragment;
import ru.irikolis.caravan.presentation.game.message.MessageFragment;
import ru.irikolis.caravan.presentation.game.GameActivity;
import ru.irikolis.caravan.presentation.game.message.adapter.MessageAdapter;
import ru.irikolis.caravan.presentation.game.message.viewholders.audio.AudioViewHolder;
import ru.irikolis.caravan.presentation.game.rules.RulesFragment;

import dagger.Subcomponent;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Subcomponent(
        modules = {
                GameModule.class,
                NavigationModule.class,
                TargetModule.class,
                ArtifactModule.class,
                MessageModule.class,
                MapModule.class,
                RulesModule.class
        }
)
@ActivityScope
public interface GameComponent {
    void inject(GameActivity gameActivity);

    void inject(InformationFragment informationFragment);
    void inject(CongratulationFragment congratulationFragment);
    void inject(MessageFragment messageFragment);
    void inject(ArtifactFragment artifactFragment);
    void inject(MapFragment mapFragment);
    void inject(RulesFragment rulesFragment);
    void inject(MessageAdapter messageAdapter);
    void inject(AudioViewHolder audioMessageViewHolder);
}
