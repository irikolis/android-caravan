package ru.irikolis.caravan.di.component;

import ru.irikolis.caravan.di.ServiceScope;
import ru.irikolis.caravan.di.module.ServiceModule;
import ru.irikolis.caravan.data.service.GameService;

import dagger.Subcomponent;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Subcomponent(
        modules = {
                ServiceModule.class
        }
)
@ServiceScope
public interface ServiceComponent {
    void inject(GameService gameService);
}
