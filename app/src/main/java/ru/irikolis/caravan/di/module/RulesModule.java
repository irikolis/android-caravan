package ru.irikolis.caravan.di.module;

import android.app.Application;

import dagger.Module;
import dagger.Provides;
import ru.irikolis.caravan.data.database.AppDatabase;
import ru.irikolis.caravan.data.network.ApiService;
import ru.irikolis.caravan.data.repositories.MapRepository;
import ru.irikolis.caravan.data.repositories.RulesRepository;
import ru.irikolis.caravan.di.ActivityScope;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
public class RulesModule {

    @Provides
    @ActivityScope
    RulesRepository provideGameRulesRepository(Application application) {
        return new RulesRepository(application);
    }
}
