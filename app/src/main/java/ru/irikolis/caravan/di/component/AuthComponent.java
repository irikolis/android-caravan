package ru.irikolis.caravan.di.component;

import ru.irikolis.caravan.di.ActivityScope;
import ru.irikolis.caravan.di.module.ArtifactModule;
import ru.irikolis.caravan.di.module.AuthModule;
import ru.irikolis.caravan.di.module.ConfigurationModule;
import ru.irikolis.caravan.di.module.MapModule;
import ru.irikolis.caravan.di.module.MessageModule;
import ru.irikolis.caravan.di.module.RoutesModule;
import ru.irikolis.caravan.di.module.RulesModule;
import ru.irikolis.caravan.di.module.TargetModule;
import ru.irikolis.caravan.presentation.authorization.AuthActivity;
import ru.irikolis.caravan.presentation.authorization.login.LoginFragment;
import ru.irikolis.caravan.presentation.authorization.pincode.PinCodeFragment;

import dagger.Subcomponent;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Subcomponent(
        modules = {
                AuthModule.class,
                ConfigurationModule.class,
                RoutesModule.class,
                TargetModule.class,
                ArtifactModule.class,
                MessageModule.class,
                MapModule.class,
                RulesModule.class
        }
)
@ActivityScope
public interface AuthComponent {
    void inject(AuthActivity authActivity);

    void inject(LoginFragment loginFragment);
    void inject(PinCodeFragment pinCodeFragment);
}
