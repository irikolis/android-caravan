package ru.irikolis.caravan.di.module;

import dagger.Module;
import dagger.Provides;
import ru.irikolis.caravan.di.ActivityScope;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
public class NavigationModule {

    @Provides
    @ActivityScope
    Cicerone<Router> getCicerone() {
        return Cicerone.create();
    }

    @Provides
    @ActivityScope
    Router getRouter(Cicerone<Router> cicerone) {
        return cicerone.getRouter();
    }

    @Provides
    @ActivityScope
    NavigatorHolder getNavigationHolder(Cicerone<Router> cicerone) {
        return cicerone.getNavigatorHolder();
    }
}
