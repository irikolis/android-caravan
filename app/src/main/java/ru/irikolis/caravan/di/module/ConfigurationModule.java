package ru.irikolis.caravan.di.module;

import ru.irikolis.caravan.data.database.AppDatabase;
import ru.irikolis.caravan.data.repositories.ModificationRepository;
import ru.irikolis.caravan.di.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
public class ConfigurationModule {

    @Provides
    @ActivityScope
    ModificationRepository provideModificationRepository(AppDatabase appDatabase) {
        return new ModificationRepository(appDatabase);
    }
}
