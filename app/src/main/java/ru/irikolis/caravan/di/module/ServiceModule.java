package ru.irikolis.caravan.di.module;

import android.app.Application;

import ru.irikolis.caravan.data.database.AppDatabase;
import ru.irikolis.caravan.data.network.ApiService;
import ru.irikolis.caravan.data.providers.LocationProviderRepository;
import ru.irikolis.caravan.data.repositories.MessageRepository;
import ru.irikolis.caravan.data.repositories.TargetRepository;
import ru.irikolis.caravan.di.ActivityScope;
import ru.irikolis.caravan.di.ServiceScope;

import dagger.Module;
import dagger.Provides;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
public class ServiceModule {

    @Provides
    @ServiceScope
    LocationProviderRepository providerLocationRepository(Application application) {
        return new LocationProviderRepository(application);
    }

    @Provides
    @ServiceScope
    TargetRepository provideTargetRepository(ApiService apiService, AppDatabase appDatabase) {
        return new TargetRepository(apiService, appDatabase);
    }

    @Provides
    @ServiceScope
    MessageRepository provideMessageRepository(ApiService apiService, AppDatabase appDatabase) {
        return new MessageRepository(apiService, appDatabase);
    }
}
