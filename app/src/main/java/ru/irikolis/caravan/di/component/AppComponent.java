package ru.irikolis.caravan.di.component;

import android.app.Application;

import ru.irikolis.caravan.application.CaravanApplication;
import ru.irikolis.caravan.di.builder.ActivityBuilder;
import ru.irikolis.caravan.di.module.AppModule;
import ru.irikolis.caravan.di.module.GameModule;
import ru.irikolis.caravan.di.module.AuthModule;
import ru.irikolis.caravan.di.module.ConfigurationModule;
import ru.irikolis.caravan.di.module.MainModule;
import ru.irikolis.caravan.di.module.ServiceModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Component(
        modules = {
                AppModule.class,
                ActivityBuilder.class
        }
)
@Singleton
public interface AppComponent {

    void inject(CaravanApplication app);

    @Component.Builder
    interface Builder{
        AppComponent build();

        @BindsInstance
        Builder application(Application application);

    }

    MainComponent plusMainComponent(MainModule mainModule);
    AuthComponent plusAuthComponent(AuthModule authModule);
    ConfigurationComponent plusConfigurationComponent(ConfigurationModule configurationModule);
    GameComponent plusGameComponent(GameModule gameModule);
    ServiceComponent plusGameServiceComponent(ServiceModule serviceModule);
}
