package ru.irikolis.caravan.domain.interactors;

import android.app.Application;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.SparseBooleanArray;

import ru.irikolis.caravan.application.AppConstants;
import ru.irikolis.caravan.data.preference.AppPreference;
import ru.irikolis.caravan.data.providers.LocationProviderRepository;
import ru.irikolis.caravan.data.repositories.MessageRepository;
import ru.irikolis.caravan.data.repositories.TargetRepository;
import ru.irikolis.caravan.domain.models.BaseMessage;
import ru.irikolis.caravan.domain.models.Coord;
import ru.irikolis.caravan.domain.models.CoordState;
import ru.irikolis.caravan.domain.models.TargetCheckState;
import ru.irikolis.caravan.domain.models.Result;
import ru.irikolis.caravan.domain.models.TargetCoord;
import ru.irikolis.caravan.domain.models.TargetMessage;
import ru.irikolis.caravan.domain.models.TargetState;
import ru.irikolis.caravan.domain.models.TargetType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import ru.irikolis.caravan.utils.SchedulersProvider;
import timber.log.Timber;

import static android.content.Context.AUDIO_SERVICE;
import static ru.irikolis.caravan.application.AppConstants.SOUND_LOOP;
import static ru.irikolis.caravan.application.AppConstants.SOUND_PRIORITY;
import static ru.irikolis.caravan.domain.models.TargetCheckState.COMPLETED_TARGET;
import static ru.irikolis.caravan.domain.models.TargetCheckState.LAST_COMPLETED_TARGET;
import static ru.irikolis.caravan.domain.models.TargetCheckState.MISSED_TARGET;
import static ru.irikolis.caravan.application.AppConstants.NETWORK_LOCATION_DISPATCH_INTERVAL_SEC;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ServiceInteractor {
    private Application application;
    private AppPreference appPreference;
    private SoundPool soundPool;
    private LocationProviderRepository locationProviderRepository;
    private TargetRepository targetRepository;
    private MessageRepository messageRepository;
    private SchedulersProvider schedulersProvider;

    private SparseBooleanArray loadedSounds = new SparseBooleanArray();

    @Inject
    public ServiceInteractor(Application application, AppPreference appPreference, SoundPool soundPool,
                             LocationProviderRepository locationProviderRepository, TargetRepository targetRepository,
                             MessageRepository messageRepository, SchedulersProvider schedulersProvider) {
        this.application = application;
        this.appPreference = appPreference;
        this.soundPool = soundPool;
        this.locationProviderRepository = locationProviderRepository;
        this.targetRepository = targetRepository;
        this.messageRepository = messageRepository;
        this.schedulersProvider = schedulersProvider;
    }

    public int loadSound(String fileName) {
        // When Sound Pool load complete
        soundPool.setOnLoadCompleteListener((soundPool, sampleId, status) -> loadedSounds.put(sampleId, status == 0));
        AssetFileDescriptor descriptor;
        try {
            descriptor = application.getAssets().openFd(fileName);
        } catch (IOException ex) {
            Timber.d(ex);
            return -1;
        }

        return soundPool.load(descriptor, SOUND_PRIORITY);
    }

    public void playSound(int soundId) {
        if (loadedSounds != null && loadedSounds.get(soundId, false)) {
            AudioManager audioManager = (AudioManager) application.getSystemService(AUDIO_SERVICE);

            // Current volume index of particular stream type
            float currentVolumeIndex = audioManager != null ? audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) : 0f;

            // Get the maximum volume index for a particular stream type
            float maxVolumeIndex = audioManager != null ? audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) : 1f;

            float volume = currentVolumeIndex / maxVolumeIndex;

            soundPool.play(soundId, volume, volume, SOUND_PRIORITY, SOUND_LOOP, 1f);
        }

    }

    public int getNumberOfNecessaryTargets() {
        return appPreference.getNumberOfNessesaryTargets();
    }

    public int getCompletedNumberOfNecessaryTargets() {
        return appPreference.getCompletedNumberOfNessesaryTargets();
    }

    public Observable<Result<CoordState, Coord>> startLocationListener() {
        return locationProviderRepository.startLocationListener()
                .observeOn(schedulersProvider.io());
    }

    public Observable<BaseMessage> startLocationDispatchAndMessagesCheck() {
        return Observable.interval(NETWORK_LOCATION_DISPATCH_INTERVAL_SEC, TimeUnit.SECONDS)
                .observeOn(schedulersProvider.io())
                .flatMap(interval -> locationProviderRepository.getLastLocation().toObservable())
                .filter(result -> result.getState() == CoordState.SUCCESS)
                .map(Result::getData)
                .flatMap(this::dispatchLocation)
                .flatMap(this::checkMessage);
    }

    private Observable<Integer> dispatchLocation(Coord coord) {
        return targetRepository.getAllTargetsFromDb(TargetState.COMPLETED.name())
                .flattenAsObservable(items -> items)
                .map(TargetCoord::getSeqId)
                .toList()
                .flatMap(points -> targetRepository
                        .dispatchLocationApi(appPreference.getGamerId(), coord.getLatitude(), coord.getLongitude(), points.toArray(new Integer[0]))
                )
                .toObservable();
    }

    private Observable<BaseMessage> checkMessage(int lastMessageId) {
        return messageRepository.getLastIdOfMasterMessagesFromDb().toObservable()
                .filter(lastReceivedId -> lastReceivedId < lastMessageId)
                .flatMap(lastReceivedId -> messageRepository.getMasterMessagesApi(lastReceivedId + 1, lastMessageId).toObservable())
                .flatMapIterable(items -> items)
                .flatMap(message -> messageRepository.putMessageToDb(message).map(id -> message).toObservable());
    }

    public Single<Result<TargetCheckState, List<TargetMessage>>> checkTargets(Coord location) {
        return targetRepository.getAllTargetsFromDb(TargetState.UNBLOCKED.name())
                .flattenAsObservable(items -> items)
                .filter(target -> LocationProviderRepository.isReachTarget(location, target.getCoord(), AppConstants.MIN_LOCATION_UPDATE_DISTANCE))
                .doOnNext(this::incrementCompletedNumberOfTarget)
                .flatMap(target -> Observable.zip(
                        updateTarget(target),
                        updateTargetMessagePostingTime(target.getSeqId(), new Date().getTime()),
                        Result::create)
                )
                .collectInto(Result.create(MISSED_TARGET, new ArrayList<>()), this::collectCheckedTargetResult);
    }

    private void incrementCompletedNumberOfTarget(TargetCoord target) {
        if (target.getType() == TargetType.NECESSARY) {
            int number = appPreference.getCompletedNumberOfNessesaryTargets();
            appPreference.putCompletedNumberOfNessesaryTargets(++number);
        }
        else if (target.getType() == TargetType.OPTIONAL) {
            int number = appPreference.getCompletedNumberOfOptionalTargets();
            appPreference.putCompletedNumberOfOptionalTargets(++number);
        }
    }

    private Observable<TargetCheckState> updateTarget(TargetCoord target) {
        return targetRepository.updateTargetStateToDb(target.getSeqId(), TargetState.COMPLETED.name()).toObservable()
                .map(completed -> COMPLETED_TARGET)
                .flatMap(state -> {
                    if (target.getType() == TargetType.NECESSARY) {
                        return targetRepository
                                .setNextTargetToDb(TargetType.NECESSARY.name(), TargetState.BLOCKED.name(), TargetState.UNBLOCKED.name()).toObservable()
                                .map(updated -> (updated) ? COMPLETED_TARGET : LAST_COMPLETED_TARGET);
                    }
                    else
                        return Observable.just(state);
                });
    }

    private Observable<TargetMessage> updateTargetMessagePostingTime(int id, long time) {
        return messageRepository.getTargetMessageFromDb(id)
                .flatMap(message -> {
                    message.setPostingTime(time);
                    return messageRepository
                            .updateMessageFromDb(message)
                            .map(updated -> message);
                })
                .toObservable();
    }

    private void collectCheckedTargetResult(Result<TargetCheckState, List<TargetMessage>> totalResult, Result<TargetCheckState, TargetMessage> result) {
        TargetCheckState totalState = (totalResult.getState() == LAST_COMPLETED_TARGET) ? LAST_COMPLETED_TARGET : result.getState();
        totalResult.setState(totalState);
        if (totalResult.getData() != null)
            totalResult.getData().add(result.getData());
    }
}
