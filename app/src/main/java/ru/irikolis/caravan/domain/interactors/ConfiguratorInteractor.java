package ru.irikolis.caravan.domain.interactors;

import java.util.Collections;
import java.util.List;

import ru.irikolis.caravan.data.preference.AppPreference;
import ru.irikolis.caravan.data.repositories.ArtifactRepository;
import ru.irikolis.caravan.data.repositories.MapRepository;
import ru.irikolis.caravan.data.repositories.MessageRepository;
import ru.irikolis.caravan.data.repositories.ModificationRepository;
import ru.irikolis.caravan.data.repositories.TargetRepository;
import ru.irikolis.caravan.data.repositories.RoutesRepository;
import ru.irikolis.caravan.data.repositories.RulesRepository;
import ru.irikolis.caravan.domain.models.GameData;
import ru.irikolis.caravan.domain.models.GameState;
import ru.irikolis.caravan.domain.models.Route;
import ru.irikolis.caravan.domain.models.Target;
import ru.irikolis.caravan.domain.models.TargetMessage;
import ru.irikolis.caravan.domain.models.TargetType;
import ru.irikolis.caravan.domain.models.GlobalGameDataType;

import javax.inject.Inject;

import io.reactivex.Single;
import ru.irikolis.caravan.utils.SchedulersProvider;

import static ru.irikolis.caravan.domain.models.GlobalGameDataType.DATA_RULES;
import static ru.irikolis.caravan.application.AppConstants.MAP_FILE_NAME;
import static ru.irikolis.caravan.application.AppConstants.RULES_FILE_NAME;
import static ru.irikolis.caravan.application.AppConstants.RULES_FILE_URL;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ConfiguratorInteractor {
    private RoutesRepository routesRepository;
    private ModificationRepository modificationRepository;
    private MapRepository mapRepository;
    private ArtifactRepository artifactRepository;
    private MessageRepository messageRepository;
    private TargetRepository targetRepository;
    private RulesRepository rulesRepository;
    private AppPreference appPreference;
    private SchedulersProvider schedulersProvider;

    @Inject
    public ConfiguratorInteractor(RoutesRepository routesRepository, ModificationRepository modificationRepository, MapRepository mapRepository,
                                  ArtifactRepository artifactRepository, MessageRepository messageRepository, RulesRepository rulesRepository,
                                  TargetRepository targetRepository, AppPreference appPreference, SchedulersProvider schedulersProvider) {
        this.routesRepository = routesRepository;
        this.modificationRepository = modificationRepository;
        this.mapRepository = mapRepository;
        this.artifactRepository = artifactRepository;
        this.messageRepository = messageRepository;
        this.rulesRepository = rulesRepository;
        this.targetRepository = targetRepository;
        this.appPreference = appPreference;
        this.schedulersProvider = schedulersProvider;
    }

    public Single<Route> getRoute(int routeId) {
        return routesRepository.getRouteFromDb(routeId)
                .subscribeOn(schedulersProvider.io());
    }

    public Single<Long> getLastModifiedTime(GlobalGameDataType type) {
        Single<Long> lastModify;
        if (type == DATA_RULES)
            lastModify = rulesRepository.getLastModifyOfFile(type.getSourceName());
        else
            lastModify = modificationRepository.getLastModifyFromDb(type.getSourceName());

        return lastModify
                .subscribeOn(schedulersProvider.io())
                .onErrorReturnItem(0L);
    }

    public Single<Boolean> loadGameMap() {
        return mapRepository.getMapFromApi()
                .subscribeOn(schedulersProvider.io())
                .flatMap(maps -> mapRepository.resetMapsDb(maps)
                        .map(rows -> maps.get(0))
                        .flatMap(map -> mapRepository.downloadMapImageFromApi(map.getImageUrl()))
                        .flatMap(stream -> mapRepository.putMapToFile(MAP_FILE_NAME, stream))
                        .map(bytes -> bytes > 0)
                );
    }

    public Single<Boolean> loadArtifacts() {
        return artifactRepository.getArtifactsFromApi()
                .subscribeOn(schedulersProvider.io())
                .flatMap(artifacts -> artifactRepository.resetArtifactsDb(artifacts))
                .map(rows -> !rows.isEmpty());
    }

    public Single<Boolean> loadGeoPoints() {
        return targetRepository.getGeoPointsFromApi()
                .subscribeOn(schedulersProvider.io())
                .flatMap(geoPoints -> targetRepository.resetGeoPointsDb(geoPoints))
                .map(rows -> !rows.isEmpty());
    }

    public Single<Boolean> loadGameRules() {
        return rulesRepository.getGameRulesFromUrl(RULES_FILE_URL)
                .subscribeOn(schedulersProvider.io())
                .flatMap(stream -> rulesRepository.putGameRulesToFile(RULES_FILE_NAME, stream))
                .map(bytes -> bytes > 0);
    }

    public Single<List<Route>> loadRoutes() {
        return routesRepository.getRoutesFromApi()
                .flatMap(routes -> routesRepository.resetRoutesDb(routes)
                        .map(rows -> routes)
                );
    }

    public Single<Boolean> performManualInitialization(String gamerName, int routeId) {
        return targetRepository.performQuestBuildApi(gamerName, routeId)
                .subscribeOn(schedulersProvider.io())
                .doOnSuccess(gameData -> {
                    appPreference.putRouteId(routeId);
                    appPreference.putGamerName(gamerName);
                })
                .flatMap(gameData -> routesRepository.getRouteFromDb(routeId)
                        .flatMap(route -> updateData(route, gameData))
                );
    }

    public Single<Boolean> performAutoInitialization() {
        return Single.merge(loadGameMap(), loadArtifacts(), loadGeoPoints()/*, loadGameRules()*/)
                .reduce((t, st) -> t & st)
                .toSingle()
                .map(routes -> appPreference.getRouteId())
                .flatMap(routeId -> loadRoutes()
                        .flattenAsObservable(items -> items)
                        .filter(route -> route.getRouteId() == routeId)
                        .singleOrError()
                        .flatMap(route -> targetRepository.performQuestBuildApi(appPreference.getGamerName(), routeId)
                                .flatMap(gameData -> updateData(route, gameData))
                        )
                );
    }

    private Single<Boolean> updateData(Route route, GameData gameData) {
        return updateCache(route, gameData)
                .mergeWith(resetDatabases(gameData.getTargets(), gameData.getMessages()))
                .reduce((t, st) -> t & st)
                .toSingle();
    }

    private Single<Boolean> updateCache(Route route, GameData gameData) {
        return Single.fromCallable(() -> {
            appPreference.putRouteParams(route.getName(), route.getDescription());
            appPreference.putGameParams(GameState.GAME_STATE_INITIALIZED, gameData.getGamerId());
            appPreference.putMapParams(gameData.isMapVisible(), gameData.isTargetsVisible(), gameData.isOrdered());
            int count = 0;
            for (Target target: gameData.getTargets()) {
                if (target.getType() == TargetType.NECESSARY)
                    ++count;
            }
            appPreference.putTargetParams(count, 0, 0);
            return true;
        });
    }

    private Single<Boolean> resetDatabases(List<Target> targets, List<TargetMessage> messages) {
        return targetRepository.resetTargetsDb(targets)
                .mergeWith(messageRepository.resetMessagesDb(messages))
                .mergeWith(artifactRepository.resetCollectedArtifactsDb(Collections.emptyList()))
                .map(rows -> !rows.isEmpty())
                .reduce((t, st) -> t | st)
                .toSingle();
    }
}
