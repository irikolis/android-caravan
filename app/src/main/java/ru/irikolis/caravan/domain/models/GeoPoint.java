package ru.irikolis.caravan.domain.models;

import androidx.annotation.Nullable;

import java.util.Objects;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GeoPoint {
    private int geoId;
    private String title;
    private double latitude;
    private double longitude;

    public GeoPoint(int geoId, String title, double latitude, double longitude) {
        this.geoId = geoId;
        this.title = title;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getGeoId() {
        return geoId;
    }

    public String getTitle() {
        return title;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        GeoPoint geoPoint = (GeoPoint) obj;
        return geoId == geoPoint.geoId &&
               Objects.equals(title, geoPoint.title) &&
               latitude == geoPoint.latitude &&
               longitude == geoPoint.longitude;
    }

    @Override
    public int hashCode() {
        return Objects.hash(geoId, title, latitude, longitude);
    }
}
