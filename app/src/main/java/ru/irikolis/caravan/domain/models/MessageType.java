package ru.irikolis.caravan.domain.models;

import androidx.annotation.LayoutRes;

import ru.irikolis.caravan.R;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public enum MessageType {
    TARGET_MESSAGE(R.layout.item_target_message),
    TEXT_MESSAGE(R.layout.item_text_message),
    AUDIO_MESSAGE(R.layout.item_audio_message);

    private int layoutId;

    MessageType(@LayoutRes int layoutId) {
        this.layoutId = layoutId;
    }

    public int getLayoutId() {
        return layoutId;
    }
}
