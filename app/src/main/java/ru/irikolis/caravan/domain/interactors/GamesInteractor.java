package ru.irikolis.caravan.domain.interactors;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import ru.irikolis.caravan.data.repositories.GamesRepository;
import ru.irikolis.caravan.domain.models.Game;
import ru.irikolis.caravan.utils.SchedulersProvider;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GamesInteractor {
    private GamesRepository gamesRepository;
    private SchedulersProvider schedulersProvider;

    @Inject
    public GamesInteractor(GamesRepository gamesRepository, SchedulersProvider schedulersProvider) {
        this.gamesRepository = gamesRepository;
        this.schedulersProvider = schedulersProvider;
    }

    public Single<List<Game>> getAllGames() {
        return gamesRepository.getGamesFromApi()
                .subscribeOn(schedulersProvider.io());
    }
}
