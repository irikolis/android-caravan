package ru.irikolis.caravan.domain.models;

import androidx.annotation.Nullable;

import java.util.List;
import java.util.Objects;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GameData {
    private int gamerId;
    private boolean mapVisible;
    private boolean targetsVisible;
    private boolean ordered;
    private List<Target> targets;
    private List<TargetMessage> messages;

    public GameData(int gamerId, boolean mapVisible, boolean targetsVisible, boolean ordered, List<Target> targets, List<TargetMessage> messages) {
        this.gamerId = gamerId;
        this.mapVisible = mapVisible;
        this.targetsVisible = targetsVisible;
        this.ordered = ordered;
        this.targets = targets;
        this.messages = messages;
    }

    public int getGamerId() {
        return gamerId;
    }

    public boolean isMapVisible() {
        return mapVisible;
    }

    public boolean isTargetsVisible() {
        return targetsVisible;
    }

    public boolean isOrdered() {
        return ordered;
    }

    public List<Target> getTargets() {
        return targets;
    }

    public List<TargetMessage> getMessages() {
        return messages;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        GameData data = (GameData) obj;
        return gamerId == data.gamerId &&
               mapVisible == data.mapVisible &&
               targetsVisible == data.targetsVisible &&
               ordered == data.ordered &&
               Objects.equals(targets, data.targets) &&
               Objects.equals(messages, data.messages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gamerId, mapVisible, targetsVisible, ordered, targets, messages);
    }
}
