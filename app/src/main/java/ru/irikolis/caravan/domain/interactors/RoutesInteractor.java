package ru.irikolis.caravan.domain.interactors;

import ru.irikolis.caravan.data.repositories.RoutesRepository;
import ru.irikolis.caravan.domain.models.Route;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import ru.irikolis.caravan.utils.SchedulersProvider;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class RoutesInteractor {
    private RoutesRepository routesRepository;
    private SchedulersProvider schedulersProvider;

    @Inject
    public RoutesInteractor(RoutesRepository routesRepository, SchedulersProvider schedulersProvider) {
        this.routesRepository = routesRepository;
        this.schedulersProvider = schedulersProvider;
    }

    public Single<List<Route>> getAllRoutes() {
        return routesRepository.getRoutesFromApi()
                .subscribeOn(schedulersProvider.io())
                .flatMap(routes -> routesRepository.resetRoutesDb(routes)
                        .map(rows -> routes)
                );
    }

    public Single<Boolean> performGameSelection(int gameId) {
        return routesRepository.performGameSelectionApi(gameId)
                .subscribeOn(schedulersProvider.io());
    }
}
