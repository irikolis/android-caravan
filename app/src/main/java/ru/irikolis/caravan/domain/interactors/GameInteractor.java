package ru.irikolis.caravan.domain.interactors;

import android.app.Application;
import android.media.AudioManager;
import android.media.SoundPool;

import ru.irikolis.caravan.data.preference.AppPreference;
import ru.irikolis.caravan.data.repositories.ServiceRepository;

import javax.inject.Inject;

import io.reactivex.Observable;
import ru.irikolis.caravan.utils.SchedulersProvider;

import static android.content.Context.AUDIO_SERVICE;
import static ru.irikolis.caravan.application.AppConstants.SOUND_LOOP;
import static ru.irikolis.caravan.application.AppConstants.SOUND_PRIORITY;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GameInteractor {
    private Application application;
    private SoundPool soundPool;
    private AppPreference appPreference;
    private ServiceRepository serviceRepository;
    private SchedulersProvider schedulersProvider;

    @Inject
    public GameInteractor(Application application, SoundPool soundPool, AppPreference appPreference,
                          ServiceRepository serviceRepository, SchedulersProvider schedulersProvider) {
        this.application = application;
        this.soundPool = soundPool;
        this.appPreference = appPreference;
        this.serviceRepository = serviceRepository;
        this.schedulersProvider = schedulersProvider;
    }

    public boolean isServiceRunning() {
        return serviceRepository.isServiceRunning();
    }

    public void playSound(int soundId) {
        AudioManager audioManager = (AudioManager) application.getSystemService(AUDIO_SERVICE);

        // Current volume index of particular stream type
        float currentVolumeIndex = audioManager != null ? audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) : 0f;

        // Get the maximum volume index for a particular stream type
        float maxVolumeIndex = audioManager != null ? audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) : 1f;

        float volume = currentVolumeIndex / maxVolumeIndex;

        soundPool.play(soundId, volume, volume, SOUND_PRIORITY, SOUND_LOOP, 1f);
    }

    public void setGameState(String state) {
        appPreference.putGameState(state);
    }

    public boolean isMapVisible() {
        return appPreference.isMapVisible();
    }

    public int getNumberOfNecessaryTargets() {
        return appPreference.getNumberOfNessesaryTargets();
    }

    public int getCompletedNumberOfNecessaryTargets() {
        return appPreference.getCompletedNumberOfNessesaryTargets();
    }

    public Observable<Boolean> startPermissionRequestListener() {
        return  serviceRepository.startRequestPermissionListener()
                .observeOn(schedulersProvider.io());
    }

    public Observable<Boolean> startGameOverListener() {
        return serviceRepository.startLastCompletedTargetListener()
                .observeOn(schedulersProvider.io())
                .map(message -> true);
    }

    public Observable<Boolean> startMessageListener() {
        return Observable.merge(
                serviceRepository.startCompletedTargetListener(),
                serviceRepository.startTextMessageListener(),
                serviceRepository.startAudioMessageListener()
        )
                .observeOn(schedulersProvider.io())
                .map(message -> true);
    }
}
