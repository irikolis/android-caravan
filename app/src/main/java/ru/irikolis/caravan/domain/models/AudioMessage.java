package ru.irikolis.caravan.domain.models;

import androidx.annotation.Nullable;

import java.util.Objects;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class AudioMessage implements BaseMessage {
    protected int sentId;
    protected Long sentTime;
    protected String text;
    protected String audioUrl;
    protected Integer startTime;
    protected Integer finalTime;
    protected Double audioSize;
    protected Long postingTime;

    public AudioMessage(int sentId, Long sentTime, String text, String audioUrl, Integer startTime, Integer finalTime, Double audioSize, Long postingTime) {
        this.sentId = sentId;
        this.sentTime = sentTime;
        this.text = text;
        this.audioUrl = audioUrl;
        this.startTime = startTime;
        this.finalTime = finalTime;
        this.audioSize = audioSize;
        this.postingTime = postingTime;
    }

    public AudioMessage(AudioMessage message) {
        this(
                message.sentId,
                message.sentTime,
                message.text,
                message.audioUrl,
                message.startTime,
                message.finalTime,
                message.audioSize,
                message.postingTime
        );
    }

    public int getSentId() {
        return sentId;
    }

    public Long getSentTime() {
        return sentTime;
    }

    public String getText() {
        return text;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setFinalTime(Integer finalTime) {
        this.finalTime = finalTime;
    }

    public Integer getFinalTime() {
        return finalTime;
    }

    public Double getAudioSize() {
        return audioSize;
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.AUDIO_MESSAGE;
    }

    @Override
    public Long getPostingTime() {
        return postingTime;
    }

    @Override
    public void setPostingTime(Long postingTime) {
        this.postingTime = postingTime;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentId, sentTime, text, audioUrl, startTime, finalTime, audioSize, postingTime);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        AudioMessage msg = (AudioMessage) obj;
        return sentId == msg.sentId &&
               Objects.equals(sentTime, msg.sentTime) &&
               Objects.equals(text, msg.text) &&
               Objects.equals(audioUrl, msg.audioUrl) &&
               Objects.equals(startTime, msg.startTime) &&
               Objects.equals(finalTime, msg.finalTime) &&
               Objects.equals(audioSize, msg.audioSize) &&
               Objects.equals(postingTime, msg.postingTime);
    }
}
