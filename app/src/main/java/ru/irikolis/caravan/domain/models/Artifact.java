package ru.irikolis.caravan.domain.models;

import java.util.Objects;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class Artifact {
    private long mac;
    private String name;
    private String description;
    private boolean autoInit;
    private String time;

    public Artifact(long mac, String name, String description, boolean autoInit, String time) {
        this.mac = mac;
        this.name = name;
        this.description = description;
        this.autoInit = autoInit;
        this.time = time;
    }

    public Artifact(Artifact artifact) {
        this.mac = artifact.mac;
        this.name = artifact.name;
        this.description = artifact.description;
        this.autoInit = artifact.autoInit;
        this.time = artifact.time;
    }

    public long getMac() {
        return mac;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isAutoInit() {
        return autoInit;
    }

    public String getTime() {
        return time;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Artifact artifact = (Artifact) obj;
        return mac == artifact.mac &&
               Objects.equals(name, artifact.name) &&
               Objects.equals(description, artifact.description) &&
               autoInit == artifact.autoInit &&
               Objects.equals(time, artifact.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mac, name, description, autoInit, time);
    }
}
