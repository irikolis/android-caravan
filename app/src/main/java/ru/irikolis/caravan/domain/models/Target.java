package ru.irikolis.caravan.domain.models;

import androidx.annotation.Nullable;

import java.util.Objects;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class Target {
    private int seqId;
    private int geoId;
    private TargetType type;
    private TargetState state;

    public Target(int seqId, int geoId, TargetType type, TargetState state) {
        this.seqId = seqId;
        this.geoId = geoId;
        this.type = type;
        this.state = state;
    }

    public int getSeqId() {
        return seqId;
    }

    public int getGeoId() {
        return geoId;
    }

    public TargetType getType() {
        return type;
    }

    public TargetState getState() {
        return state;
    }

    public void setState(TargetState state) {
        this.state = state;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Target target = (Target) obj;
        return seqId == target.seqId &&
               geoId == target.geoId &&
               type == target.type &&
               state == target.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(seqId, geoId, type, state);
    }
}
