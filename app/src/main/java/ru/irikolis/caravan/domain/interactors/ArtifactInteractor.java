package ru.irikolis.caravan.domain.interactors;

import ru.irikolis.caravan.data.repositories.ArtifactRepository;
import ru.irikolis.caravan.data.providers.BluetoothProviderRepository;
import ru.irikolis.caravan.domain.models.Artifact;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import ru.irikolis.caravan.utils.SchedulersProvider;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class ArtifactInteractor {
    private ArtifactRepository artifactRepository;
    private BluetoothProviderRepository bluetoothProviderRepository;
    private SchedulersProvider schedulersProvider;

    @Inject
    public ArtifactInteractor(ArtifactRepository artifactRepository, BluetoothProviderRepository bluetoothProviderRepository,
                              SchedulersProvider schedulersProvider) {
        this.artifactRepository = artifactRepository;
        this.bluetoothProviderRepository = bluetoothProviderRepository;
        this.schedulersProvider = schedulersProvider;
    }

    /**
     * Get collected artifacts by gamer
     */
    public Single<List<Artifact>> getCollectedArtifacts() {
        return artifactRepository.getCollectedArtifactsFromDb()
                .subscribeOn(schedulersProvider.io());
    }

    /**
     * Find new artifacts with bluetooth
     */
    public Observable<Artifact> searchNewArtifacts() {
        return bluetoothProviderRepository.startBluetoothListener()
                .subscribeOn(schedulersProvider.io())
                .flatMap(mac -> artifactRepository.getCollectedArtifactsFromDb(mac).toObservable()
                        .filter(List::isEmpty)
                        .flatMap(collectedArtifacts -> artifactRepository.getArtifactsFromDb(mac).toObservable())
                        .filter(artifacts -> !artifacts.isEmpty())
                        .flatMap(artifacts -> artifactRepository.putCollectedArtifactsToDb(artifacts).toObservable()
                                .map(rows -> artifacts)
                        )
                        .flatMapIterable(items -> items)
                        .take(1)
                );
    }
}
