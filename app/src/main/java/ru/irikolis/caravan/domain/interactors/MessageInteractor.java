package ru.irikolis.caravan.domain.interactors;

import java.util.List;

import io.reactivex.Single;
import ru.irikolis.caravan.data.repositories.ServiceRepository;
import ru.irikolis.caravan.data.repositories.MessageRepository;
import ru.irikolis.caravan.domain.models.BaseMessage;

import javax.inject.Inject;

import io.reactivex.Observable;
import ru.irikolis.caravan.utils.SchedulersProvider;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class MessageInteractor {
    private MessageRepository messageRepository;
    private ServiceRepository serviceRepository;
    private SchedulersProvider schedulersProvider;

    @Inject
    public MessageInteractor(MessageRepository messageRepository, ServiceRepository serviceRepository, SchedulersProvider schedulersProvider) {
        this.messageRepository = messageRepository;
        this.serviceRepository = serviceRepository;
        this.schedulersProvider = schedulersProvider;
    }

    public Single<List<BaseMessage>> getAllMessages() {
        return messageRepository.getAllMessagesFromDb()
                .subscribeOn(schedulersProvider.io());
    }

    public Observable<BaseMessage> startMessageListener() {
        return Observable.merge(
                serviceRepository.startCompletedTargetListener(),
                serviceRepository.startTextMessageListener(),
                serviceRepository.startAudioMessageListener()
        )
                .observeOn(schedulersProvider.io());
    }

    public Single<Integer> updateMessage(BaseMessage message) {
        return messageRepository.updateMessageFromDb(message)
                .subscribeOn(schedulersProvider.io());
    }
}
