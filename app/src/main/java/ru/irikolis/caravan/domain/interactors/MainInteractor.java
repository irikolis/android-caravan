package ru.irikolis.caravan.domain.interactors;

import ru.irikolis.caravan.data.preference.AppPreference;

import javax.inject.Inject;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class MainInteractor {
    AppPreference appPreference;

    @Inject
    public MainInteractor(AppPreference appPreference) {
        this.appPreference = appPreference;
    }

    public void setGameState(String state) {
        appPreference.putGameState(state);
    }

    public String getGameState() {
        return appPreference.getGameState();
    }
}
