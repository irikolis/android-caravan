package ru.irikolis.caravan.domain.models;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class Result<E extends Enum<E>, T> {
    @NonNull
    private E state;

    @Nullable
    private T data;

    public Result(@NonNull E state, @Nullable T data) {
        this.state = state;
        this.data = data;
    }

    @NonNull
    public E getState() {
        return state;
    }

    public void setState(@NonNull E state) {
        this.state = state;
    }

    @Nullable
    public T getData() {
        return data;
    }

    public void setData(@Nullable T data) {
        this.data = data;
    }

    public static <E extends Enum<E>, T> Result <E, T> create(E state, T data) {
        return new Result<>(state, data);
    }

}
