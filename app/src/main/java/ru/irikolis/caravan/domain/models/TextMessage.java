package ru.irikolis.caravan.domain.models;

import androidx.annotation.Nullable;

import java.util.Objects;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class TextMessage implements BaseMessage {
    private int sentId;
    private Long sentTime;
    private String text;
    private Long postingTime;

    public TextMessage(int sentId, Long sentTime, String text, Long postingTime) {
        this.sentId = sentId;
        this.sentTime = sentTime;
        this.text = text;
        this.postingTime = postingTime;
    }

    public TextMessage(TextMessage message) {
        this(
                message.sentId,
                message.sentTime,
                message.text,
                message.postingTime
        );
    }

    public int getSentId() {
        return sentId;
    }

    public Long getSentTime() {
        return sentTime;
    }

    public String getText() {
        return text;
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.TEXT_MESSAGE;
    }

    @Override
    public Long getPostingTime() {
        return postingTime;
    }

    @Override
    public void setPostingTime(Long postingTime) {
        this.postingTime = postingTime;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentId, sentTime, text, postingTime);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        TextMessage msg = (TextMessage) obj;
        return sentId == msg.sentId &&
               Objects.equals(sentTime, msg.sentTime) &&
               Objects.equals(text, msg.text) &&
               Objects.equals(postingTime, msg.postingTime);
    }
}
