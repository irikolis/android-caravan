package ru.irikolis.caravan.domain.models;

import androidx.annotation.Nullable;

import java.util.Objects;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class TargetCoord {
    private int seqId;
    private String title;
    private Coord coord;
    private TargetType type;
    private TargetState state;

    public TargetCoord(int seqId, String title, double latitude, double longitude, TargetType type, TargetState state) {
        this.seqId = seqId;
        this.title = title;
        this.type = type;
        this.state = state;
        this.coord = new Coord(latitude, longitude);
    }

    public int getSeqId() {
        return seqId;
    }

    public String getTitle() {
        return title;
    }

    public Coord getCoord() {
        return coord;
    }

    public double getLatitude() {
        return coord.getLatitude();
    }

    public double getLongitude() {
        return coord.getLongitude();
    }

    public TargetType getType() {
        return type;
    }

    public TargetState getState() {
        return state;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        TargetCoord targetCoord = (TargetCoord) obj;
        return seqId == targetCoord.seqId &&
               Objects.equals(title, targetCoord.title) &&
               Objects.equals(coord, targetCoord.coord) &&
               type == targetCoord.type &&
               state == targetCoord.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(seqId, title, coord, type, state);
    }
}
