package ru.irikolis.caravan.domain.interactors;

import ru.irikolis.caravan.data.preference.AppPreference;
import ru.irikolis.caravan.data.repositories.ServiceRepository;

import javax.inject.Inject;

import io.reactivex.Observable;
import ru.irikolis.caravan.utils.SchedulersProvider;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class InformationInteractor {
    private ServiceRepository serviceRepository;
    private AppPreference appPreference;
    private SchedulersProvider schedulersProvider;

    @Inject
    public InformationInteractor(ServiceRepository serviceRepository, AppPreference appPreference, SchedulersProvider schedulersProvider) {
        this.appPreference = appPreference;
        this.serviceRepository = serviceRepository;
        this.schedulersProvider = schedulersProvider;
    }

    public String getGamerName() {
        return appPreference.getGamerName();
    }

    public String getRouteName() {
        return appPreference.getRouteName();
    }

    public String getRouteDescription() {
        return appPreference.getRouteDescription();
    }

    public int getNumberOfNecessaryTargets() {
        return appPreference.getNumberOfNessesaryTargets();
    }

    public int getCompletedNumberOfNecessaryTargets() {
        return appPreference.getCompletedNumberOfNessesaryTargets();
    }

    public int getCompletedNumberOfOptionalTargets() {
        return appPreference.getCompletedNumberOfOptionalTargets();
    }

    public Observable<Boolean> startCompletedTargetListener() {
        return serviceRepository.startCompletedTargetListener()
                .observeOn(schedulersProvider.io())
                .map(message -> true);
    }
}
