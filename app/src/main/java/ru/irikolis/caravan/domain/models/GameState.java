package ru.irikolis.caravan.domain.models;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GameState {
    public static final String GAME_STATE_NOT_INITIALIZED = "not initialized";
    public static final String GAME_STATE_INITIALIZED = "initialized";
    public static final String GAME_STATE_IN_PROCESS = "in process";
    public static final String GAME_STATE_ABORTED = "aborted";
    public static final String GAME_STATE_FINISHED = "finished";
}
