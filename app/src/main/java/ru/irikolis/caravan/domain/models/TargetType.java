package ru.irikolis.caravan.domain.models;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public enum TargetType {
    NECESSARY,
    OPTIONAL,
    HIDDEN
}
