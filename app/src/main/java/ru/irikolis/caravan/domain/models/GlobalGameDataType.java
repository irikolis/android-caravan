package ru.irikolis.caravan.domain.models;

import static ru.irikolis.caravan.application.AppConstants.RULES_FILE_NAME;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public enum GlobalGameDataType {
    DATA_MAP("game_maps"),
    DATA_ARTIFACT("artifacts"),
    DATA_GEO_POINT("geo_points"),
    DATA_RULES(RULES_FILE_NAME);

    private String sourceName;

    GlobalGameDataType(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getSourceName() {
        return sourceName;
    }
}
