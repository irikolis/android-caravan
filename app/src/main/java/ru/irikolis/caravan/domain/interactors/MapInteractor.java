package ru.irikolis.caravan.domain.interactors;

import ru.irikolis.caravan.data.preference.AppPreference;
import ru.irikolis.caravan.data.repositories.MapRepository;
import ru.irikolis.caravan.data.repositories.TargetRepository;
import ru.irikolis.caravan.data.repositories.ServiceRepository;
import ru.irikolis.caravan.data.providers.LocationProviderRepository;
import ru.irikolis.caravan.domain.models.Coord;
import ru.irikolis.caravan.domain.models.CoordState;
import ru.irikolis.caravan.domain.models.GameMap;
import ru.irikolis.caravan.domain.models.Result;
import ru.irikolis.caravan.domain.models.TargetCoord;
import ru.irikolis.caravan.domain.models.TargetState;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import ru.irikolis.caravan.utils.SchedulersProvider;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class MapInteractor {
    private MapRepository mapRepository;
    private TargetRepository targetRepository;
    private LocationProviderRepository locationProviderRepository;
    private ServiceRepository serviceRepository;
    private AppPreference appPreference;
    private SchedulersProvider schedulersProvider;

    @Inject
    public MapInteractor(MapRepository mapRepository, TargetRepository targetRepository, LocationProviderRepository locationProviderRepository,
                         ServiceRepository serviceRepository, AppPreference appPreference, SchedulersProvider schedulersProvider) {
        this.mapRepository = mapRepository;
        this.targetRepository = targetRepository;
        this.locationProviderRepository = locationProviderRepository;
        this.serviceRepository = serviceRepository;
        this.appPreference = appPreference;
        this.schedulersProvider = schedulersProvider;
    }

    public boolean isTargetsVisible() {
        return appPreference.isTargetsVisible();
    }

    public Single<GameMap> getGameMap() {
        return mapRepository.getMapToDb()
                .subscribeOn(schedulersProvider.io());
    }

    public Observable<TargetCoord> getAllTargets() {
        return targetRepository.getAllTargetsFromDb()
                .subscribeOn(schedulersProvider.io())
                .flattenAsObservable(items -> items);
    }

    public Single<Result<CoordState, Coord>> getLastLocation() {
        return locationProviderRepository.getLastLocation()
                .observeOn(schedulersProvider.io());
    }

    public Observable<Coord> startLocationListener() {
        return  serviceRepository.startLocationListener()
                .observeOn(schedulersProvider.io());
    }

    public Observable<TargetCoord> startCompletedTargetListener() {
        return serviceRepository.startCompletedTargetListener()
                .observeOn(schedulersProvider.io())
                .flatMap(message -> targetRepository
                        .getAllTargetsFromDb(TargetState.COMPLETED.name())
                        .flattenAsObservable(items -> items)
                );
    }
}
