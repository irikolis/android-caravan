package ru.irikolis.caravan.domain.models;

import androidx.annotation.Nullable;

import java.util.Objects;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class Route {
    private int     id;
    private String  name;
    private int     level;
    private String  description;
    private String  instruction;
    private boolean mapVisible;
    private boolean routeVisible;
    private boolean ordered;
    private String  lastModifiedTime;

    public Route(int id, String name, int level, String description, String instruction,
                 boolean mapVisible, boolean routeVisible, boolean ordered, String lastModifiedTime) {
        this.id = id;
        this.name = name;
        this.level = level;
        this.description = description;
        this.instruction = instruction;
        this.mapVisible = mapVisible;
        this.routeVisible = routeVisible;
        this.ordered = ordered;
        this.lastModifiedTime = lastModifiedTime;
    }

    public int getRouteId() {
        return id;
    }

    public void setRouteId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public boolean isMapVisible() {
        return mapVisible;
    }

    public void setMapVisible(boolean mapVisible) {
        this.mapVisible = mapVisible;
    }

    public boolean isRouteVisible() {
        return routeVisible;
    }

    public void setRouteVisible(boolean routeVisible) {
        this.routeVisible = routeVisible;
    }

    public boolean isOrdered() {
        return ordered;
    }

    public void setOrdered(boolean ordered) {
        this.ordered = ordered;
    }

    public String getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setLastModifiedTime(String lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Route route = (Route) obj;
        return id == route.id &&
               Objects.equals(name, route.name) &&
               level == route.level &&
               Objects.equals(description, route.description) &&
               Objects.equals(instruction, route.instruction) &&
               mapVisible == route.mapVisible &&
               routeVisible == route.routeVisible &&
               ordered == route.ordered &&
               Objects.equals(lastModifiedTime, route.lastModifiedTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, level, description, instruction, mapVisible, routeVisible, ordered, lastModifiedTime);
    }
}
