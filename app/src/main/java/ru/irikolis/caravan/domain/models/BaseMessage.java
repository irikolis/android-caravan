package ru.irikolis.caravan.domain.models;

import androidx.annotation.Nullable;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public interface BaseMessage {
    MessageType getMessageType();
    Long getPostingTime();
    void setPostingTime(Long postingTime);

    @Override
    boolean equals(@Nullable Object obj);
}
