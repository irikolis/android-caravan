package ru.irikolis.caravan.domain.models;

import androidx.annotation.Nullable;

import java.util.Objects;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class TargetMessage implements BaseMessage {
    private int id;
    private String text;
    private Long postingTime;

    public TargetMessage(int id, String text, Long postingTime) {
        this.id = id;
        this.text = text;
        this.postingTime = postingTime;
    }

    public TargetMessage(TargetMessage message) {
        this(
                message.id,
                message.text,
                message.postingTime
        );
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.TARGET_MESSAGE;
    }

    @Override
    public Long getPostingTime() {
        return postingTime;
    }

    @Override
    public void setPostingTime(Long postingTime) {
        this.postingTime = postingTime;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, postingTime);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        TargetMessage msg = (TargetMessage) obj;
        return id == msg.id &&
               Objects.equals(text, msg.text) &&
               Objects.equals(postingTime, msg.postingTime);
    }
}
