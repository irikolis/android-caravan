package ru.irikolis.caravan.domain.models;

import androidx.annotation.Nullable;

import ru.irikolis.mapview.GeoCoord;

import java.util.Objects;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GameMap {
    private String imageUrl;
    private GeoCoord northWestCoord;
    private GeoCoord southEastCoord;
    private int accuracy;

    public GameMap(String imageUrl, GeoCoord northWestCoord, GeoCoord southEastCoord, int accuracy) {
        this.imageUrl = imageUrl;
        this.northWestCoord = northWestCoord;
        this.southEastCoord = southEastCoord;
        this.accuracy = accuracy;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public GeoCoord getNorthWestCoord() {
        return northWestCoord;
    }

    public GeoCoord getSouthEastCoord() {
        return southEastCoord;
    }

    public int getAccuracy() {
        return accuracy;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        GameMap gameMap = (GameMap) obj;
        return Objects.equals(imageUrl, gameMap.imageUrl) &&
               Objects.equals(northWestCoord, gameMap.northWestCoord) &&
               Objects.equals(southEastCoord, gameMap.southEastCoord) &&
               accuracy == gameMap.accuracy;
    }

    @Override
    public int hashCode() {
        return Objects.hash(imageUrl, northWestCoord, southEastCoord, accuracy);
    }
}
