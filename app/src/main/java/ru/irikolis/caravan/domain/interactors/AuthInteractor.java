package ru.irikolis.caravan.domain.interactors;

import ru.irikolis.caravan.data.preference.AppPreference;
import ru.irikolis.caravan.data.repositories.AuthRepository;

import javax.inject.Inject;

import io.reactivex.Single;
import ru.irikolis.caravan.utils.SchedulersProvider;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class AuthInteractor {
    private AuthRepository authRepository;
    private AppPreference appPreference;
    private SchedulersProvider schedulersProvider;

    @Inject
    public AuthInteractor(AuthRepository authRepository, AppPreference appPreference, SchedulersProvider schedulersProvider) {
        this.authRepository = authRepository;
        this.appPreference = appPreference;
        this.schedulersProvider = schedulersProvider;
    }

    public Single<Boolean> performAuthLogin(String username, String password) {
        return authRepository.performAuthLoginFromApi(username, password)
            .subscribeOn(schedulersProvider.io());
    }

    public Single<Boolean> performAuthPinCode(String pinCode) {
        return authRepository.performAuthPinCodeFromApi(pinCode)
                .subscribeOn(schedulersProvider.io());
    }

    public void setServerAddress(String address) {
        appPreference.putBaseUrl(address);
    }

    public String getServerAddress() {
        return appPreference.getBaseUrl();
    }
}
