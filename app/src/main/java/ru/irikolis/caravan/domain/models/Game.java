package ru.irikolis.caravan.domain.models;

import androidx.annotation.Nullable;

import java.util.Objects;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class Game {
    private int     id;
    private String  name;
    private String  description;
    private String  iconUrl;

    public Game(int id, String name, String description, String iconUrl) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.iconUrl = iconUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Game game = (Game) obj;
        return id == game.id &&
               Objects.equals(name, game.name) &&
               Objects.equals(description, game.description) &&
               Objects.equals(iconUrl, game.iconUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, iconUrl);
    }
}
