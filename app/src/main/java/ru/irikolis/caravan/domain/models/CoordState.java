package ru.irikolis.caravan.domain.models;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public enum CoordState {
    SUCCESS,
    PERMISSION_DENIED,
    NO_DATA
}
