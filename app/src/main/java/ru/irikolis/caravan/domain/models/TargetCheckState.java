package ru.irikolis.caravan.domain.models;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public enum TargetCheckState {
    MISSED_TARGET,
    COMPLETED_TARGET,
    LAST_COMPLETED_TARGET,
    NO_DATA
}
