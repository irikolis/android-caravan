package ru.irikolis.caravan.domain.models;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public enum TargetState {
    BLOCKED,
    UNBLOCKED,
    COMPLETED
}
