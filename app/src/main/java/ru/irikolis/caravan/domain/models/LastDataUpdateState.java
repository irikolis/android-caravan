package ru.irikolis.caravan.domain.models;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

import ru.irikolis.caravan.R;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public enum LastDataUpdateState {
    NO_DATA(R.drawable.ic_data_error_24dp, R.string.no_data),
    OLD_DATA(R.drawable.ic_data_warring_24dp, R.string.last_update),
    SUCCESS(R.drawable.ic_data_ok_24dp, R.string.last_update),
    LOADING_ERROR(R.drawable.ic_data_error_24dp, R.string.loading_error);

    private int iconId;
    private int infoId;

    LastDataUpdateState(@DrawableRes int iconId, @StringRes int infoId) {
        this.iconId = iconId;
        this.infoId = infoId;
    }

    public int getIconId() {
        return iconId;
    }

    public int getInfoId() {
        return infoId;
    }
}
