package ru.irikolis.caravan.utils;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class FileUtils {
    public static String getFileFromAsset(Context context, String fileName) {
        try {
            InputStream inputStream = context.getAssets().open(fileName);
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            inputStream.close();
            return new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static <T> List<T> getJsonFromAsset(Context context, Type classType, String jsonFileName) {
        GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
        Gson gson = builder.create();
        Type type = com.google.gson.internal.$Gson$Types.newParameterizedTypeWithOwner(null, List.class, classType);
        return gson.fromJson(getFileFromAsset(context, jsonFileName), type);
    }

    public static long copyInputStreamToFile(Context context, InputStream inputStream, String fileName) {
        int writtenBytes = 0;
        try (OutputStream outputStream = context.openFileOutput(fileName, MODE_PRIVATE)) {
            int read;
            byte[] bytes = new byte[4*1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
                writtenBytes += read;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return writtenBytes;
    }

    public static long getLastModified(String fileName) {
        File file = new File(fileName);
        if (file.exists())
            return file.lastModified();
        else
            return 0L;
    }
}
