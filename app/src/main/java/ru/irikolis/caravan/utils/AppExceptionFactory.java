package ru.irikolis.caravan.utils;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class AppExceptionFactory {
    public static final String EX_NETWORK = "network_exception";
    public static final String EX_TOKEN = "login_exception";

    public static AppException createNetworkException(String message) {
        return new AppException(EX_NETWORK, message);
    }

    public static AppException createTokenException() {
        return new AppException(EX_TOKEN, "The received token is invalid");
    }
}
