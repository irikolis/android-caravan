package ru.irikolis.caravan.utils;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class OnTextChanged implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}
