package ru.irikolis.caravan.utils;

import android.location.Location;

import static java.lang.Math.atan;
import static java.lang.Math.exp;
import static java.lang.Math.log;
import static java.lang.Math.tan;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */

/*
 * WGS 84/Pseudo-Mercator (Spherical Mercator) - EPSG:3857(3785, 900913)
 * It is used by virtually all major online map providers, including Google Maps, Mapbox, Bing Maps, OpenStreetMap, Mapquest, Esri, and many others.
 */
public class GeoTransformer {
    private Raster raster;

    public GeoTransformer() {
    }

    public GeoTransformer(Raster raster) {
        this.raster = raster;
    }

    public void setRaster(Raster raster) {
        this.raster = raster;
    }

    public Raster getRaster() {
        return raster;
    }

    public class Raster {
        // Number of columns/rows in image
        private int cols;
        private int rows;

        // Minimum/maximum longitude of image (in decimal degrees)
        private double minLongitude;
        private double maxLongitude;

        // Minimum/maximum latitude of image (in decimal degrees)
        // Note that these are the coordinates of the CENTRE of the corner pixels
        private double minLatitude;
        private double maxLatitude;

        public Raster(int cols, int rows, double minLongitude, double minLatitude, double maxLongitude, double maxLatitude) {
            this.cols = cols;
            this.rows = rows;
            this.minLongitude = minLongitude;
            this.maxLongitude = maxLongitude;
            this.minLatitude = minLatitude;
            this.maxLatitude = maxLatitude;
        }

        public int getX(double longitude) {
            double fract = (longitude - minLongitude) / (maxLongitude - minLongitude);
            return (int) ((cols - 1) * fract);
        }


        public int getY(double latitude) {
            double ymin = log(tan(toRadians(45.0 + (minLatitude / 2.0))));
            double ymax = log(tan(toRadians(45.0 + (maxLatitude / 2.0))));
            double yint = log(tan(toRadians(45.0 + (latitude / 2.0))));
            double fract = (yint - ymin) / (ymax - ymin);
            return (int) ((rows - 1) * (1.0 - fract));
        }

        public double getLongitude(int x) {
            double fract = x / (cols - 1);
            return minLongitude + (fract * (maxLongitude - minLongitude));
        }

        public double getLatitude(int y) {
            double fract = 1.0 - ((double) y / (rows - 1));
            double ymin = log(tan(toRadians(45.0 + (minLatitude / 2.0))));
            double ymax = log(tan(toRadians(45.0 + (maxLatitude / 2.0))));
            double yint = ymin + (fract * (ymax - ymin));
            return 2.0 * (toDegrees(atan(exp(yint))) - 45.0);
        }
    }

    public static class Mercator {
        private static final double R_MAJOR = 6378137.0;

        public static double getX(double longitude) {
            return R_MAJOR * toRadians(longitude);
        }

        public static double getY(double latitude) {
            return R_MAJOR * log(tan(toRadians(45.0 + (latitude) / 2.0)));
        }

        public static double getDistance(double startLatitude, double startLongitude, double endLatitude, double endLongitude) {
            float[] result = new float[3];
            Location.distanceBetween(startLatitude, startLongitude, endLatitude, endLongitude, result);
            return result[0];
        }
    }
}
