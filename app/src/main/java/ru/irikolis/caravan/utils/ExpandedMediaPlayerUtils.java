package ru.irikolis.caravan.utils;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.net.Uri;

import ru.irikolis.caravan.presentation.core.mediaplayer.ExpandedMediaPlayer;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@SuppressWarnings({"unused", "RedundantSuppression", "UnusedReturnValue"})
public class ExpandedMediaPlayerUtils {

    public static boolean playRawMedia(Context context, ExpandedMediaPlayer mediaPlayer, String resName)  {
        try {
            int resID = context.getResources().getIdentifier(resName, "raw", context.getPackageName());
            Uri uri = Uri.parse("android.resource://" + context.getPackageName() + "/" + resID);
            mediaPlayer.setDataSource(context, uri);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.prepareAsync();
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean playLocalMedia(Context context, ExpandedMediaPlayer mediaPlayer, String localPath)  {
        try {
            Uri uri = Uri.parse("android.resource://" + localPath);
            mediaPlayer.setDataSource(context, uri);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.prepareAsync();
        } catch(Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean playUrlMedia(Context context, ExpandedMediaPlayer mediaPlayer, Uri uri)  {
        try {
            mediaPlayer.setDataSource(context, uri);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.prepareAsync();
        } catch(Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean playAssetMedia(Context context, ExpandedMediaPlayer mediaPlayer, String fileName)  {
        try {
            AssetFileDescriptor descriptor = context.getAssets().openFd(fileName);
            mediaPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.prepareAsync();
        } catch(Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean playMedia(Context context, ExpandedMediaPlayer mediaPlayer, String mediaUri)  {
        Uri uri = Uri.parse(mediaUri);
        String scheme = uri.getScheme();
        String authority = uri.getAuthority();
        String path = uri.getPath();
        if (scheme != null && scheme.equals("file") && authority != null && authority.equals("android_asset") && path != null)
            return ExpandedMediaPlayerUtils.playAssetMedia(context, mediaPlayer, uri.getPath().substring(1));
        else
            return ExpandedMediaPlayerUtils.playUrlMedia(context, mediaPlayer, uri);
    }
}
