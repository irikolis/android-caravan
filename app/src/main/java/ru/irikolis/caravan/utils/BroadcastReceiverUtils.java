package ru.irikolis.caravan.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.annotation.NonNull;

import io.reactivex.Observable;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class BroadcastReceiverUtils {

    public static Observable<Intent> create(@NonNull Context context, @NonNull IntentFilter intentFilter) {
        final Context appContext = context.getApplicationContext();
        return Observable.create(emitter -> {
            final BroadcastReceiver receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (intentFilter.hasAction(action)) {
                        emitter.onNext(intent);
                    }
                }
            };

            appContext.registerReceiver(receiver, intentFilter);
            emitter.setCancellable(() -> appContext.unregisterReceiver(receiver));
        });
    }
}