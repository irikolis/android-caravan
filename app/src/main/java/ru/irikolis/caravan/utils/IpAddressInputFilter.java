package ru.irikolis.caravan.utils;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class IpAddressInputFilter implements InputFilter {
    private static final String pattern = "^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(:(\\d{1,5})?)?)?)?)?)?)?)?";

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        if (end > start) {
            String destTxt = dest.toString();
            String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
            if (resultingTxt.matches (pattern)) {
                int ipEndIndex = resultingTxt.indexOf(':');

                // IP
                String ipTxt = (ipEndIndex < 0) ? resultingTxt : resultingTxt.substring(0, ipEndIndex);
                String[] splits = ipTxt.split("\\.");
                for (String split : splits) {
                    if (Integer.parseInt(split) > 255)
                        return "";
                }

                // Port
                if (ipEndIndex > 0) {
                    String portTxt = resultingTxt.substring(ipEndIndex + 1);
                    if (!portTxt.isEmpty() && Integer.parseInt(portTxt) > 65535)
                        return "";
                }
            } else {
                return "";
            }
        }

        return null;
    }
}
