package ru.irikolis.caravan.utils;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class AppException extends RuntimeException {
    private String exceptionType;

    public AppException(String exceptionType, String message) {
        super(message);
        this.exceptionType = exceptionType;
    }

    public String getType() {
        return exceptionType;
    }
}
