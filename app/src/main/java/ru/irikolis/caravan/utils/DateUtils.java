package ru.irikolis.caravan.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class DateUtils {
    public static Date parse(String pattern, String string) {
        try {
            DateFormat format = new SimpleDateFormat(pattern, Locale.getDefault());
            return format.parse(string);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return new Date();
    }
}
