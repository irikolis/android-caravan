package ru.irikolis.caravan.application;

/**
 * @author Irina Kolovorotnaya
 */
public class AppConstants {
    public static final String APP_PREFERENCES = "app_prefs";
    public static final String APP_DATABASE = "app_database";

    public static final String BASE_URL = "84.201.145.244:8000";
    public static final String EXT_URL = "84.201.145.244:8000";

    public static final String RULES_FILE_URL = "https://losigra.ru/wiki/page/%D0%9F%D1%80%D0%B0%D0%B2%D0%B8%D0%BB%D0%B0_%D0%BF%D0%BE_%D1%8D%D0%BA%D0%BE%D0%BD%D0%BE%D0%BC%D0%B8%D0%BA%D0%B5#.D0.9A.D0.B0.D1.80.D0.B0.D0.B2.D0.B0.D0.BD.D1.8B";
    public static final String RULES_FILE_NAME = "game_rules.html";
    public static final String MAP_FILE_NAME = "game_map.jpg";

    // The maximum number of days per game
    public static final int MAX_GAME_DAYS = 7;

    // The minimum location distance to change updates in meters
    public static final int MIN_LOCATION_UPDATE_DISTANCE = 20;

    // The minimum location time between updates in milliseconds
    public static final int MIN_LOCATION_UPDATE_TIME = 5000;

    public static final long NETWORK_TIMEOUT_SEC = 5;
    public static final long NETWORK_LOCATION_DISPATCH_INTERVAL_SEC = 30;

    public static final int MAX_SOUND_STREAMS = 5;
    public static final int SOUND_PRIORITY = 1;
    public static final int SOUND_LOOP = 0;
}
