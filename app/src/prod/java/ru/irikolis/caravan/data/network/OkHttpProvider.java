package ru.irikolis.caravan.data.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import ru.irikolis.caravan.data.preference.AppPreference;

import static ru.irikolis.caravan.application.AppConstants.NETWORK_TIMEOUT_SEC;

/**
 * @author Irina Kolovorotnaya
 */
public class OkHttpProvider {
    public static OkHttpClient buildClient(AppPreference preference) {
        return new OkHttpClient.Builder()
                .connectTimeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
                .readTimeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
                .writeTimeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
                .addInterceptor(ApiKeyInterceptor.create(preference))
                .addInterceptor(LoggingInterceptor.create())
                .build();
    }
}
