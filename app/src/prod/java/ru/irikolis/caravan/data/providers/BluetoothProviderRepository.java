package ru.irikolis.caravan.data.providers;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import javax.inject.Inject;

import io.reactivex.Observable;
import ru.irikolis.caravan.utils.BroadcastReceiverUtils;

/**
 * @author Irina Kolovorotnaya
 */
public class BluetoothProviderRepository {
    private Context context;

    @Inject
    public BluetoothProviderRepository(Context context) {
        this.context = context;
    }

    public boolean isBluetoothEnabled() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null)
            return false;
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            return false;
        }
        else
            return true;
    }

    public Observable<Long> startBluetoothListener() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

        return BroadcastReceiverUtils.create(context, intentFilter)
                .flatMap(intent -> {
                    String action = intent.getAction();
                    if (action != null && action.equals(BluetoothDevice.ACTION_FOUND)) {
                        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                        if (device != null && device.getBondState() == BluetoothDevice.BOND_BONDED) {
                            return Observable.just(device.getAddress());
                        }
                    }

                    return Observable.empty();
                })
                .map(macString -> Long.parseLong(macString.replaceAll(":", ""), 16));
    }
}
