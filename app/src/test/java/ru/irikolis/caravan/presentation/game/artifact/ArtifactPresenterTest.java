package ru.irikolis.caravan.presentation.game.artifact;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import ru.irikolis.caravan.domain.interactors.ArtifactInteractor;
import ru.irikolis.caravan.domain.models.Artifact;
import ru.irikolis.caravan.utils.SchedulersProvider;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@RunWith(MockitoJUnitRunner.class)
public class ArtifactPresenterTest {

    @Mock SchedulersProvider schedulersProvider;
    @Mock ArtifactInteractor interactor;
    @Mock ArtifactView view;
    @InjectMocks ArtifactPresenter presenter;

    private long mac;
    private List<Artifact> artifacts;

    @Before
    public void setUp() {
        mac = 0x1122334455L;
        artifacts = new ArrayList<>();
        artifacts.add(new Artifact(mac, "name", "description", true, "00:00:00"));
        when(schedulersProvider.ui()).thenReturn(Schedulers.trampoline());
    }

    @Test
    public void testLoadingCollectedArtifacts() {
        when(interactor.getCollectedArtifacts()).thenReturn(Single.just(artifacts));
        presenter.attachView(view);
//        verify(view).showGameInitLoading(true);
//        verify(view).showGameInitLoading(false);
//        verify(view).setArtifacts(artifacts);
    }
}
