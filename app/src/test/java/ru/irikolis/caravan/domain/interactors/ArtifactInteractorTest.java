package ru.irikolis.caravan.domain.interactors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;
import ru.irikolis.caravan.data.providers.BluetoothProviderRepository;
import ru.irikolis.caravan.data.repositories.ArtifactRepository;
import ru.irikolis.caravan.domain.models.Artifact;
import ru.irikolis.caravan.utils.SchedulersProvider;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@RunWith(MockitoJUnitRunner.class)
public class ArtifactInteractorTest {

    @Mock SchedulersProvider schedulersProvider;
    @Mock ArtifactRepository artifactRepository;
    @Mock BluetoothProviderRepository bluetoothProviderRepository;

    @InjectMocks ArtifactInteractor interactor;

    private long mac;
    private List<Artifact> artifacts;

    @Before
    public void setUp() throws Exception {
        mac = 0x1122334455L;
        artifacts = new ArrayList<>();
        artifacts.add(new Artifact(mac, "name", "description", true, "00:00:00"));
        when(schedulersProvider.io()).thenReturn(Schedulers.trampoline());
    }

    @Test
    public void testSearchingNewArtifactsIfHasNew() throws Exception {
        when(bluetoothProviderRepository.startBluetoothListener())
                .thenReturn(Observable.just(mac));
        when(artifactRepository.getCollectedArtifactsFromDb(mac))
                .thenReturn(Single.just(Collections.emptyList()));
        when(artifactRepository.getArtifactsFromDb(mac))
                .thenReturn(Single.just(artifacts));
        when(artifactRepository.putCollectedArtifactsToDb(artifacts))
                .thenReturn(Single.just(Collections.emptyList()));

        TestObserver<List<Artifact>> testObserver = interactor.searchNewArtifacts().test();
        testObserver.awaitTerminalEvent();

        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
        testObserver.assertValue(list -> list.size() == 1);
        testObserver.assertValue(list -> list.get(0).getMac() == mac);

        verify(bluetoothProviderRepository).startBluetoothListener();
        verify(artifactRepository).getCollectedArtifactsFromDb(mac);
        verify(artifactRepository).getArtifactsFromDb(mac);
        verify(artifactRepository).putCollectedArtifactsToDb(artifacts);
    }

    @Test
    public void testSearchingNewArtifactsIfNoNew() throws Exception {
        when(bluetoothProviderRepository.startBluetoothListener())
                .thenReturn(Observable.just(mac));
        when(artifactRepository.getCollectedArtifactsFromDb(mac))
                .thenReturn(Single.just(artifacts));

        TestObserver<List<Artifact>> testObserver = interactor.searchNewArtifacts().test();
        testObserver.awaitTerminalEvent();

        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
        testObserver.assertValue(List::isEmpty);

        verify(bluetoothProviderRepository).startBluetoothListener();
        verify(artifactRepository).getCollectedArtifactsFromDb(mac);
        verify(artifactRepository, never()).getArtifactsFromDb(anyLong());
        verify(artifactRepository, never()).putCollectedArtifactsToDb(anyList());
    }
}
