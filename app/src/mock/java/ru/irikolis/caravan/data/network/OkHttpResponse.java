package ru.irikolis.caravan.data.network;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class OkHttpResponse {

    private static final MediaType APPLICATION_JSON = MediaType.parse("application/json");
    private static final byte[] EMPTY_BODY = new byte[0];

    private OkHttpResponse() {
    }

    @NotNull
    public static Response success(@NotNull Request request, @NotNull InputStream stream) throws IOException {
        Buffer buffer = new Buffer().readFrom(stream);
        return new Response.Builder()
                .request(request)
                .protocol(Protocol.HTTP_1_1)
                .code(200)
                .message("OK")
                .body(ResponseBody.create(APPLICATION_JSON, buffer.size(), buffer))
                .build();
    }

    @NotNull
    public static Response error(@NotNull Request request, int code, @NotNull String message) {
        return new Response.Builder()
                .request(request)
                .protocol(Protocol.HTTP_1_1)
                .code(code)
                .message(message)
                .body(ResponseBody.create(APPLICATION_JSON, EMPTY_BODY))
                .build();
    }
}
