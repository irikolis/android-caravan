package ru.irikolis.caravan.data.network;

import android.content.Context;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import okhttp3.Request;
import okhttp3.Response;
import ru.irikolis.caravan.application.CaravanApplication;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class RequestsHandler {

    private final Map<String, String> responses = new HashMap<>();

    RequestsHandler() {
        responses.put(ApiService.REQUEST_AUTH_TOKEN, "json-files/auth_token.json");
        responses.put(ApiService.REQUEST_GAMES, "json-files/games.json");
        responses.put(ApiService.REQUEST_GAME_SELECT, "json-files/game_select.json");
        responses.put(ApiService.REQUEST_ROUTES, "json-files/routes.json");
        responses.put(ApiService.REQUEST_GAME_MAP, "json-files/game_map.json");
        responses.put(ApiService.REQUEST_GEO_POINTS, "json-files/geo_points.json");
        responses.put(ApiService.REQUEST_ARTIFACTS, "json-files/artifacts.json");
        responses.put(ApiService.REQUEST_QUEST_BUILD, "json-files/quest_build.json");
        responses.put(ApiService.REQUEST_GAME_MAP_IMAGE, "json-files/img_map_21.jpg");
        responses.put(ApiService.REQUEST_PLAY, "json-files/play.json");
        responses.put(ApiService.REQUEST_MESSAGES, "json-files/messages.json");
        responses.put(ApiService.REQUEST_AUDIO, "audio/audio_message.mp3");
    }

    public boolean shouldIntercept(String path) {
        return responses.containsKey(path);
    }

    @NotNull
    public Response proceed(@NotNull Request request, @NotNull String path) {
        if (responses.containsKey(path)) {
            String assetPath = responses.get(path);
            if (assetPath != null)
                return createResponseFromAssets(request, assetPath);
        }

        return OkHttpResponse.error(request, 500, "Incorrectly intercepted request");
    }

    @NotNull
    private Response createResponseFromAssets(@NotNull Request request, @NotNull String assetPath) {
        Context context = CaravanApplication.getInstance().getApplicationContext();
        try (InputStream stream = context.getAssets().open(assetPath)) {
            return OkHttpResponse.success(request, stream);
        } catch (IOException e) {
            return OkHttpResponse.error(request, 500, Objects.requireNonNull(e.getMessage()));
        }
    }
}
