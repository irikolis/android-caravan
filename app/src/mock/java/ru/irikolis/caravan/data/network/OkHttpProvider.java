package ru.irikolis.caravan.data.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import ru.irikolis.caravan.application.AppConstants;
import ru.irikolis.caravan.data.preference.AppPreference;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class OkHttpProvider {
    public static OkHttpClient buildClient(AppPreference preference) {
        return new OkHttpClient.Builder()
                .connectTimeout(AppConstants.NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
                .readTimeout(AppConstants.NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
                .writeTimeout(AppConstants.NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
                .addInterceptor(ApiKeyInterceptor.create(preference))
                .addInterceptor(LoggingInterceptor.create())
                .addInterceptor(MockingInterceptor.create())
                .build();
    }
}
