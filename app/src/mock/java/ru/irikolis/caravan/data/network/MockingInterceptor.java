package ru.irikolis.caravan.data.network;

import android.os.SystemClock;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.Random;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class MockingInterceptor implements Interceptor {

    private final RequestsHandler handlers;
    private final Random random;

    private MockingInterceptor() {
        handlers = new RequestsHandler();
        random = new SecureRandom();
    }

    @Override
    @NonNull
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();
        String path = request.url().encodedPath();
        if (handlers.shouldIntercept(path)) {
            Response response = handlers.proceed(request, path);
            int stubDelay = 3000;//50 + random.nextInt(2500);
            SystemClock.sleep(stubDelay);
            return response;
        }

        return chain.proceed(request);
    }

    @NonNull
    public static MockingInterceptor create() {
        return new MockingInterceptor();
    }
}
