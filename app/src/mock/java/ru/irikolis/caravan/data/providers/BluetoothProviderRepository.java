package ru.irikolis.caravan.data.providers;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class BluetoothProviderRepository {
    private final String[] macs = {
            "01:11:22:AA:BB:CC",
            "02:11:22:AA:BB:CC",
            "03:11:22:AA:BB:CC",
            "04:11:22:AA:BB:CC",
            "05:11:22:AA:BB:CC",
            "06:11:22:AA:BB:CC",
            "07:11:22:AA:BB:CC",
            "08:11:22:AA:BB:CC",
            "09:11:22:AA:BB:CC",
            "11:11:22:AA:BB:CC",
            "12:11:22:AA:BB:CC",
            "13:11:22:AA:BB:CC",
            "14:11:22:AA:BB:CC",
            "15:11:22:AA:BB:CC",
            "16:11:22:AA:BB:CC",
            "f1:11:22:AA:BB:CC",
            "f2:11:22:AA:BB:CC"
    };
    private Context context;

    @Inject
    public BluetoothProviderRepository(Context context) {
        this.context = context;
    }

    public boolean isBluetoothEnabled() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null)
            return false;
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            return false;
        }
        else
            return true;
    }

    public Observable<Long> startBluetoothListener() {
        return Observable.interval(1, TimeUnit.SECONDS)
                .take(macs.length)
                .map(index -> macs[index.intValue()])
                .map(macString -> Long.parseLong(macString.replaceAll(":", ""), 16));
    }
}
