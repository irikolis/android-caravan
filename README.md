## About

Caravan is a project designed to organize live-action role-playing games.
A team of players is given a caravan-a chest, inside of which is a phone or tablet with installed software.
The goal of the players is to visit certain places along the given route. Players receive notifications with a description of the task.

## Features ([screenshots](misc/image))

1. Authorization
    Authorization is performed by the PIN code issued to the team, or by the login password for the manual configuration of the game by the masters. With successful authorization, access to the game interface is granted.

2. Game interface
	* Messages
		Notifications with tasks for players.
	* Artifacts
        Search for artifacts (Bluetooth beacons) within the radius of availability.
        Artifacts give additional points to players (the logic of the calculation on the server).
	* Map (optional)
		A map is displayed, stylized as the game world, which shows the movements of the players, completed points and quests.
	* Rules of the game

3. The configuration of the game (for masters)



## Technology

This project takes advantage of many popular libraries and tools of the Android ecosystem.

* Tech-stack
	* [Dagger 2](https://github.com/google/dagger) is arguably the most used Dependency Injection framework for Android.
    * [Retrofit 2](https://github.com/square/retrofit) is a type-safe HTTP client for Android and Java.
    * [Room](https://github.com/square/retrofit) provides an abstraction layer over SQLite.
    * [RxJava 2](https://github.com/ReactiveX/RxJava) provides a simple way of asynchronous programming and handling multiple events, errors and termination of the event stream.
    * [Moxy](https://github.com/Arello-Mobile/Moxy) allows for hassle-free implementation of the MVP pattern.
    * [Picasso](https://github.com/square/picasso) allows for hassle-free image loading.
    * [Cicerone](https://github.com/terrakok/Cicerone) makes the navigation in an Android app easy.
    * [Timber](https://github.com/JakeWharton/timber) simplifies the process of Logging statements and automatically handle debug/release issue by itself.
    * [ButterKnife](https://github.com/JakeWharton/butterknife) tries to reduce the boilerplate needed to inflate views. (This tool is now deprecated).
    * [TouchView](https://github.com/MikeOrtiz/TouchImageView) is widget extends ImageView.
    * [Firebase Crashlytics](https://firebase.google.com/docs/crashlytics) is a realtime crash reporter.
    * [Mockito](https://github.com/mockito/mockito) is used to mock interfaces.

* Architecture
    * Clean Architecture
    * MVP (presentation layer)
