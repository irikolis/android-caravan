package ru.irikolis.mapview;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Objects;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class Marker {
    private String title;
    private boolean visible;
    private Bitmap bitmap;
    private GeoCoord geoCoord;

    public Marker() {
    }

    public final Marker title(@NonNull String title) {
        if (!Objects.equals(this.title, title))
            this.title = title;
        return this;
    }

    public final Marker visible(boolean visible) {
        if (this.visible != visible)
            this.visible = visible;
        return this;
    }

    public final Marker icon(@NonNull Bitmap bitmap) {
        if (!Objects.equals(this.bitmap, bitmap))
            this.bitmap = bitmap;
        return this;
    }

    public final Marker position(@NonNull GeoCoord geoCoord) {
        if (!Objects.equals(this.geoCoord, geoCoord))
            this.geoCoord = geoCoord;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public boolean isVisible() {
        return visible;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public double getLatitude() {
        return geoCoord.getLatitude();
    }

    public double getLongitude() {
        return geoCoord.getLongitude();
    }

    public GeoCoord getPosition() {
        return geoCoord;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Marker marker = (Marker) obj;
        return title.equals(marker.title) &&
               visible == marker.visible &&
               bitmap.equals(marker.bitmap) &&
               geoCoord.equals(marker.geoCoord);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, visible, bitmap, geoCoord);
    }
}
