package ru.irikolis.mapview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.util.AttributeSet;

import androidx.annotation.NonNull;

import com.ortiz.touchview.TouchImageView;

import java.util.HashMap;

import static java.lang.Math.log;
import static java.lang.Math.tan;
import static java.lang.Math.toRadians;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class MapImageView extends TouchImageView {
    // Minimum/maximum Mercator X(longitude) of image (in decimal degrees)
    private float minMercatorX;
    private float maxMercatorX;

    // Minimum/maximum Mercator Y(latitude) of image (in decimal degrees)
    private float minMercatorY;
    private float maxMercatorY;

    // List of markers on the map
    private HashMap<String, Marker> markers;

    public MapImageView(Context context) {
        this(context, null);
    }

    public MapImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MapImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        markers = new HashMap<>();
    }

    public void bindGeoCoords(GeoCoord leftTopCoord, GeoCoord rightBottomCoord) {
        minMercatorX = getMercatorX(leftTopCoord.getLongitude());
        maxMercatorX = getMercatorX(rightBottomCoord.getLongitude());
        minMercatorY = getMercatorY(leftTopCoord.getLatitude());
        maxMercatorY = getMercatorY(rightBottomCoord.getLatitude());
    }

    public void addMarker(@NonNull Marker marker) {
        markers.put(marker.getTitle(), marker);
        if (marker.getPosition() != null)
            invalidate();
    }

    public Marker getMarker(String title) {
        return markers.get(title);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for(Marker marker: markers.values()) {
            if (marker.getBitmap() != null && marker.isVisible()) {
                float x = getBitmapX(marker.getLongitude());
                float y = getBitmapY(marker.getLatitude());
                PointF curr = transformCoordBitmapToTouch(x, y);
                curr.x = (int) (curr.x - marker.getBitmap().getWidth() / 2.0);
                curr.y = (int) (curr.y - marker.getBitmap().getHeight() / 2.0);
                canvas.drawBitmap(marker.getBitmap(), curr.x, curr.y, null);
            }
        }
    }

    /*
     * WGS 84/Pseudo-Mercator (Spherical Mercator) - EPSG:3857(3785, 900913)
     * It is used by virtually all major online map providers, including Google Maps, Mapbox, Bing Maps, OpenStreetMap, Mapquest, Esri, and many others.
     */
    private static final double R_MAJOR = 6378137.0;

    public float getMercatorX(double longitude) {
        return (float) (R_MAJOR * toRadians(longitude));
    }

    public float getMercatorY(double latitude) {
        return (float) (R_MAJOR * log(tan(toRadians(45.0 + (latitude) / 2.0))));
    }

    public float getBitmapX(double longitude) {
        float x = getMercatorX(longitude);
        float w = getDrawable().getIntrinsicWidth();
        return (x - minMercatorX) / (maxMercatorX - minMercatorX) * w;
    }

    public float getBitmapY(double latitude) {
        float y = getMercatorY(latitude);
        float h = getDrawable().getIntrinsicHeight();
        return (y - minMercatorY) / (maxMercatorY - minMercatorY) * h;
    }
}
