package ru.irikolis.mapview;

import androidx.annotation.Nullable;

import java.util.Objects;

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
public class GeoCoord {
    private double latitude;
    private double longitude;

    public GeoCoord(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        GeoCoord geoCoord = (GeoCoord) obj;
        return latitude == geoCoord.latitude &&
               longitude == geoCoord.longitude;
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }
}
